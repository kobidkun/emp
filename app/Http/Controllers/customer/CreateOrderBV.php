<?php

namespace App\Http\Controllers\customer;

use App\Customer;
use App\Model\BVinvoice\Payment\PaymentDetails;
use App\Model\Initial\OrderBV\InitialCreateOrderBV;
use App\Model\Initial\OrderBV\InitialCreateOrderToProductBV;
use App\Model\Order\BVOrder\CreateBVOrder;
use App\ProductVatient;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Razorpay\Api\Api;
use Softon\Indipay\Facades\Indipay;


class CreateOrderBV extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:customer-api');
    }


    public function CreateOrderSave(Request $request)
    {

        $getcustomer = $request->user();


        $createorder = new CreateBVOrder();
        $createsecureorderid = ('OD' . $this->random_str('alphanum', 10));
        //invoice request

        $createorder->customer_id = $getcustomer->id;
        $createorder->customer_gstin = $request->customer_gstin;
        $createorder->order_secure_id = $createsecureorderid;
        $createorder->order_total_taxable_amount = $request->order_total_taxable_amount;
        $createorder->order_total_taxable_discount_amount = $request->order_total_taxable_discount_amount;
        $createorder->order_total_tax_cgst = $request->order_total_tax_cgst;
        $createorder->order_total_tax_sgst = $request->order_total_tax_sgst;
        $createorder->order_total_tax_igst = $request->order_total_tax_igst;

        $createorder->order_total_tax = $request->order_total_tax;
        $createorder->order_total = $request->order_total;
        $createorder->payment_type = $request->payment_type;
        $createorder->bookedfrom = $request->bookedfrom;
        $createorder->order_total_bv = $request->order_total_bv;
        $createorder->lead_purchase = $request->lead_purchase;
        $createorder->save();


        $products = $request->input('product');

        foreach ($products as $key => $value) {
            $createorder->order_to_products()->create([
                'order_id' => $createorder->id,
                'product_id' => $value['product_id'],
                'parent_product_id' => $value['parent_product_id'],
                'customer_id' => $getcustomer->id,
                'color' => $value['color'],
                'size' => $value['size'],
                'product_name' => $value['product_name'],
                'quantity' => $value['quantity'],
                'hsn' => $value['hsn'],
                'type' => $value['type'],
                'taxable_rate' => $value['taxable_rate'],
                'taxable_bv_rate' => $value['taxable_bv_rate'],
                'taxable_value' => $value['taxable_value'],
                'taxable_bv_value' => $value['taxable_bv_value'],
                'taxable_discount_amount' => $value['taxable_discount_amount'],
                'taxable_tax_cgst' => $value['taxable_tax_cgst'],
                'taxable_tax_cgst_percentage' => $value['taxable_tax_cgst_percentage'],
                'taxable_tax_sgst' => $value['taxable_tax_sgst'],
                'taxable_tax_sgst_percentage' => $value['taxable_tax_sgst_percentage'],
                'taxable_tax_igst' => $value['taxable_tax_igst'],
                'taxable_tax_igst_percentage' => $value['taxable_tax_igst_percentage'],
                'taxable_tax_cess' => $value['taxable_tax_cess'],
                'taxable_tax_cess_percentage' => $value['taxable_tax_cess_percentage'],
                'taxable_tax_total' => $value['taxable_tax_total'],
                'total' => $value['total'],


            ]);
        }


        /*  $invoicepdf = PDF::loadView('service.email.neworder',[
              'products' => $products,
              'getcustomer' => $getcustomer,
              'createorder' => $createorder,

          ])->setPaper('a4');

          $invoicepdf->save('public/invoice/'.$createsecureorderid.'.pdf');


          $order = $createorder;
          $customer_name = $getcustomer->fname;
          Mail::to($getcustomer->email)->send(new EmailToCreteOrder($order,$createsecureorderid,$customer_name));*/

        /*sms*/

        /*Send SMS using PHP*/


        //endsms


        return response()->json([
            'customer_id' => $getcustomer->id,
            'order' => $createorder->id,
            'status' => 'success',
            'order_id' => $createsecureorderid,
            'products' => $products
        ]);


    }

    function random_str($type = 'alphanum', $length = 10)
    {
        switch ($type) {
            case 'basic'    :
                return mt_rand();
                break;
            case 'alpha'    :
            case 'alphanum' :
            case 'num'      :
            case 'nozero'   :
                $seedings = array();
                $seedings['alpha'] = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                $seedings['alphanum'] = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                $seedings['num'] = '0123456789';
                $seedings['nozero'] = '123456789';

                $pool = $seedings[$type];

                $str = '';
                for ($i = 0; $i < $length; $i++) {
                    $str .= substr($pool, mt_rand(0, strlen($pool) - 1), 1);
                }
                return $str;
                break;
            case 'unique'   :
            case 'md5'      :
                return md5(uniqid(mt_rand()));
                break;
        }
    }

    public function CreateOrderSave2(Request $request)
    {

        $getcustomer = $request->user();

        // return response()->json($request);


        $createinitialorder = new InitialCreateOrderBV();

        $createinitialorder->payment_type = $request->payment_type;
        $createinitialorder->bookedfrom = $request->bookedfrom;
        $createinitialorder->customer_id = $getcustomer->id;
        $createinitialorder->lead_purchase = $request->lead_purchase;

        $createinitialorder->save();


        $products = $request->product;

        // return ($products);

        foreach ($products as $key => $value) {


            $createinitialorder->initial_create_order_to_products()->create([

                'product_id' => $value['product_id'],
                'quantity' => $value['quantity'],
                'parent_product_id' => $value['parent_product_id'],
                'initial_create_orders_id' => $createinitialorder->id,
                'customer_id' => $getcustomer->id,


            ]);
        }


        $a = $this->CreateOrderSave3($createinitialorder);


        return response()->json([
            'products' => $createinitialorder,
            'order' => $a,
        ]);


    }


    public function CreateOrderSave3($createinitialorder)
    {

        $getcustomer = Customer::findorfail($createinitialorder->customer_id);


        $createorder = new \App\Model\Order\BVOrder\CreateBVOrder();
        $createsecureorderid = ('OD' . $this->random_str('alphanum', 10));


        $productssums = InitialCreateOrderBV::with('initial_create_order_to_products')
            ->where('id', $createinitialorder->id)->first();


        $products = InitialCreateOrderToProductBV::where('initial_create_orders_id', $productssums->id)->get();


        $order_total_taxable_amount = $products->sum(function ($row) {

            $productid = $row->product_id;

            $quantity = $row->quantity;

            $getProduct = ProductVatient::findorfail($productid);

            return round($getProduct->taxable_rate * $quantity, 2);

        });


        $order_total_tax_cgst = $products->sum(function ($product) {

            $productid = $product->product_id;

            $quantity = $product->quantity;

            $getProduct = ProductVatient::findorfail($productid);

            return round(($getProduct->cgst_percentage / 100) * ($getProduct->taxable_rate * $quantity), 2);
        });

        $order_total_tax_sgst = $products->sum(function ($product) {

            $productid = $product->product_id;

            $quantity = $product->quantity;

            $getProduct = ProductVatient::findorfail($productid);

            return round(($getProduct->sgst_percentage / 100) * ($getProduct->taxable_rate * $quantity), 2);
        });


        $order_total_tax = $products->sum(function ($product) {

            $productid = $product->product_id;

            $quantity = $product->quantity;

            $getProduct = ProductVatient::findorfail($productid);

            return round(($getProduct->sgst_percentage / 100) * 2 * ($getProduct->taxable_rate * $quantity), 2);
        });


        $order_total = $products->sum(function ($product) {

            $productid = $product->product_id;

            $quantity = $product->quantity;

            $getProduct = ProductVatient::findorfail($productid);

            return round(($getProduct->listing_price) * $quantity, 2);
        });

        $order_total_bv = $products->sum(function ($product) {

            $productid = $product->product_id;

            $quantity = $product->quantity;

            $getProduct = ProductVatient::findorfail($productid);

            return round(($getProduct->business_vol) * $quantity, 2);
        });


        $createorder->customer_id = $getcustomer->id;
        $createorder->customer_gstin = $getcustomer->gstin;
        $createorder->order_secure_id = $createsecureorderid;
        $createorder->order_total_taxable_amount = round(($order_total_taxable_amount), 2);
        $createorder->order_total_taxable_discount_amount = '0';
        $createorder->order_total_tax_cgst = round(($order_total_tax_cgst), 2);
        $createorder->order_total_tax_sgst = round(($order_total_tax_sgst), 2);
        $createorder->order_total_tax_igst = '0';

        $createorder->order_total_tax = round(($order_total_tax), 2);
        $createorder->order_total = round(($order_total), 2);
        $createorder->payment_type = $productssums->payment_type;
        $createorder->bookedfrom = $productssums->bookedfrom;

        // bv change
        $createorder->order_total_bv = $order_total_bv;
        $createorder->lead_purchase = $productssums->lead_purchase;

        $createorder->save();


        foreach ($products as $key => $value) {

            $productid = $value->product_id;

            $quantity = $value['quantity'];

            $getProduct = ProductVatient::findorfail($productid);

            $taxablecgst = round(($getProduct->cgst_percentage / 100) * ($getProduct->taxable_rate * $quantity), 2);
            $taxablesgst = round(($getProduct->sgst_percentage / 100) * ($getProduct->taxable_rate * $quantity), 2);
            $taxableigst = round($taxablecgst + $taxablesgst, 2);

            $createorder->order_to_products()->create([
                'order_id' => $createorder->id,
                'product_id' => $productid,
                'parent_product_id' => $getProduct->product_id,
                'customer_id' => $getcustomer->id,
                'color' => $getProduct->color,
                'size' => $getProduct->size,
                'product_name' => $getProduct->name,
                'quantity' => $quantity,
                'hsn' => $getProduct->hsn,
                'type' => $getProduct->type,
                'taxable_rate' => $getProduct->taxable_rate,
                'taxable_value' => $getProduct->taxable_rate * $quantity,
                'taxable_bv_rate' => $getProduct->business_vol,
                'taxable_bv_value' => $getProduct->business_vol * $quantity,
                'taxable_discount_amount' => '0',
                'taxable_tax_cgst' => $taxablecgst,
                'taxable_tax_cgst_percentage' => $getProduct->cgst_percentage,
                'taxable_tax_sgst' => $taxablesgst,
                'taxable_tax_sgst_percentage' => $getProduct->sgst_percentage,
                'taxable_tax_igst' => $taxableigst,
                'taxable_tax_igst_percentage' => $getProduct->igst_percentage,
                'taxable_tax_cess' => '0',
                'taxable_tax_cess_percentage' => '0',
                'taxable_tax_total' => round($taxablecgst + $taxablesgst, 2),
                'total' => round(($getProduct->taxable_rate * $quantity) + ($taxablecgst + $taxablesgst), 2),


            ]);
        }


        //  DB::table('customer_cart_items')->where('customer_id', '=', $getcustomer->id)->delete();


        return ([

            'orders' => $createorder,
            // 'products' => $prod
        ]);


    }

    public function RequestPayment($order, Request $request)
    {
        $getcustomer = $request->user();


        $slug = $this->random_str();
        $createorder = CreateBVOrder::where('order_secure_id', $order)->first();
        //  dd($createorder);


        $appid = env('TestAppid');
        $appsecret = env('TestSecretKey');
        $orderid = $slug;
        $orderamt = $createorder->order_total;
        $orderdesc = 'buy for order ' . $createorder->id;
        $customername = $getcustomer->fname . ' ' . $getcustomer->lname;
        $customerphone = $getcustomer->phone;
        $custoomeremail = $getcustomer->email;
        $returnurl = env('WEB_URL') . '/api/openurl/payorder/status/complete';
        $notifurl = env('WEB_URL') . '/api/openurl/payorder/status/complete';

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, 'https://test.cashfree.com/api/v1/order/create');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "appId=" . $appid . "&secretKey=" . $appsecret . "&orderId="
            . $orderid . "&orderAmount=" . $orderamt . "&orderNote=" . $orderdesc . "&customerName=" . $customername .
            "&customerPhone=" . $customerphone . "&customerEmail=" . $custoomeremail .
            "&sellerPhone=&returnUrl=" . $returnurl . "&notifyUrl=" . $notifurl . "&paymentModes=&pc=");

        $headers = array();
        $headers[] = 'Cache-Control: no-cache';
        $headers[] = 'Content-Type: application/x-www-form-urlencoded';
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        curl_close($ch);

        $gatestatus = json_decode($result);
        /*  return response()->json([
              $gatestatus
          ],200);*/

        //if ($gatestatus->)

        if ($gatestatus->status === "OK") {
            $createpay = new PaymentDetails();
            $createpay->create_order_id = $createorder->id;
            $createpay->customer_id = $getcustomer->id;
            $createpay->gateway_id = $slug;
            $createpay->slug = $slug;
            $createpay->url = env('WEB_URL') . '/openurl/payorder/' . $slug;
            $createpay->amount = $createorder->order_total;
            $createpay->email = $getcustomer->email;
            $createpay->purpose = 'buy for order ' . $createorder->id;
            $createpay->inidump = $result;
            $createpay->phone = $getcustomer->phone;
            $createpay->buyer_name = $getcustomer->fname . ' ' . $getcustomer->lname;
            $createpay->status = 'pending';
            $createpay->save();


            return response()->json([
                'string' => $createpay->url,
                'status' => 'ok'
            ]);
        } else {
            return response()->json($gatestatus->status);
        }


        //    return $order;


    }

    public function CheckPaymentstatus($slug)
    {
        $a = \App\Model\Order\BVOrder\CreateBVOrder::with('create_b_v_order_to_payments')
            ->where('order_secure_id',$slug)->first();

        return response()->json($a,200);

    }





}
