<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCreateBVOrderToProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('create_b_v_order_to_products', function (Blueprint $table) {
            $table->increments('id');
            $table->text('order_id')->nullable();
            $table->text('create_orders_id')->nullable();
            $table->text('size')->nullable();
            $table->text('color')->nullable();
            $table->text('product_id')->nullable();
            $table->text('parent_product_id')->nullable();
            $table->text('product_name')->nullable();
            $table->text('customer_id')->nullable();
            $table->string('hsn')->nullable();
            $table->string('type')->nullable();
            $table->text('quantity')->nullable();
            $table->text('hsn_sac')->nullable();
            $table->text('taxable_rate')->nullable();
            $table->text('taxable_bv_rate')->nullable();
            $table->text('taxable_value')->nullable();
            $table->text('taxable_bv_value')->nullable();
            $table->text('taxable_discount_amount')->nullable();
            $table->text('taxable_tax_cgst')->nullable();
            $table->text('taxable_tax_sgst')->nullable();
            $table->text('taxable_tax_igst')->nullable();
            $table->text('taxable_tax_cess')->nullable();
            $table->text('taxable_tax_total')->nullable();
            $table->text('taxable_tax_cgst_percentage')->nullable();
            $table->text('taxable_tax_sgst_percentage')->nullable();
            $table->text('taxable_tax_igst_percentage')->nullable();
            $table->text('taxable_tax_cess_percentage')->nullable();
            $table->text('total')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('create_b_v_order_to_products');
    }
}
