<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


/*public Routes*/


Route::get('/search/global', 'admin\globalSearch\GlobalSearch@Search');

//Products
Route::get('/products/all', 'publiccontroller\ProductListApi@ProductsApiList')->name('api.products.public');



//ProductsMobileSlider

Route::get('/products/all', 'publiccontroller\ProductListApi@ProductsApiList')->name('api.products.public');
Route::get('/products/getimg/{slug}', 'publiccontroller\ProductListApi@GetProductImage');
Route::get('/products/by-attribute', 'publiccontroller\ProductListApi@ProductbyCategory')->name('api.productsattri.public');
Route::get('/products/by-category/{calslud}', 'publiccontroller\ProductListApi@GetProductByCategory')->name('api.products.bycategory.public');
Route::get('/products/by-count/{count}', 'publiccontroller\ProductListApi@GetProductByCount')->name('api.products.bycount.public');
Route::get('/products/details/{slug}', 'publiccontroller\ProductListApi@GetProductById')->name('api.products.details.public');
Route::get('/products/variant/details/{slug}', 'publiccontroller\ProductListApi@GetProductVariantById')
    ->name('api.product.variant.details.public');

//other components
Route::get('/gallery/{slug}', 'publiccontroller\ProductListApi@Gallery')->name('api.gallery.mobile');
Route::get('/slider/mobile', 'publiccontroller\ProductListApi@MobileSlider')->name('api.slider.mobile');
Route::get('/slider/website', 'publiccontroller\ProductListApi@WebsiteSlider')->name('api.slider.website');
Route::get('/config', 'publiccontroller\ProductListApi@getConfig');
/*public Routes end*/



//json-api


Route::any('/openurl/payorder/status/complete', 'publiccontroller\PublicWebConreoller@PaymentProcessComplete');

//json-api-end




Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::group(['middleware' => 'auth:customer-api'], function () {
    Route::get('/customer', function (Request $request) {
        return $request->user(); // Return admin
    });
});



/*customer*/
Route::post('/customer/register', 'publiccontroller\CustomerAuth@CreateCustomerSave')->name('api.customer.register.save');


Route::post('/customer/private/addtocart',
    'customer\CartController@StoreCart')
    ->name('api.customer.add.to.cart.post');




Route::get('/customer/private/getaddtocart',
    'customer\CartController@GetCartItem');


Route::get('/customer/private/cart/delete/{id}',
    'customer\CartController@DeleteCartItem');


/*order take api*/
Route::post('/customer/private/takeorder',
    'customer\CreateOrder@CreateOrderSave');


Route::post('/customer/private/v2/takeorder',
    'customer\CreateOrder@CreateOrderSave2');


/*order take api bv*/
Route::post('/customer/private/bv/takeorder',
    'customer\CreateOrderBV@CreateOrderSave');


Route::post('/customer/private/bv/v2/takeorder',
    'customer\CreateOrderBV@CreateOrderSave2');

//wallet

Route::get('/customer/wallet/balance',
    'customer\ShopController@GetWallet');


//wallet bv

Route::get('/customer/bv-wallet/balance',
    'customer\ShopController@GetWalletBV');


// treeview

Route::get('/customer/tree/{id}',
    'customer\TreeController@TreeApi');


/// forgot pwd

Route::post('/auth/forgot-password/request-otp',
    'customer\AuthController@ResetPasswordRequesttt');

Route::post('/auth/forgot-password/change-password',
    'customer\AuthController@ResetPasswordRequestchgpwd');

Route::post('/auth/forgot-password/verify-otp',
    'customer\AuthController@ResetPasswordRequestttVerify');

// profile



Route::get('/customer/getmoredata/create',
    'customer\CustomerData@getMoredata');


Route::get('/customer/notifications/get',
    'customer\CustomerData@Notificationss');

Route::get('/customer/notifications/count/get',
    'customer\CustomerData@NotificationssCount');

Route::post('/customer/getmoredata/adddetails',
    'customer\CustomerData@getMoredatasave');









//customer getForApiforBvPurchaseTypes


Route::get('/master/bv/types/get',
    'Customer\CustomerData@getForApiforBvPurchaseTypes');


///// customer profile


Route::get('/customer/profile/get',
    'Customer\CustomerData@Profile');

Route::post('/customer/send/notifytoken',
    'Customer\CustomerData@updateNotifytoken');

//customer order


Route::get('/customer/order/get',
    'Customer\CustomerData@GetOrder');


Route::get('/customer/orderbv/get',
    'Customer\CustomerData@GetBVOrder');

Route::get('/customer/orderbv/details/{slug}',
    'Customer\CustomerData@Getorderbvdetails');


//customer order


Route::get('/customer/invoice/get',
    'Customer\CustomerData@GetInvoice');


Route::get('/customer/invoicebv/get',
    'Customer\CustomerData@GetBVInvoice');



//customer order


Route::get('/customer/returninvoice/get',
    'Customer\CustomerData@GetReturnInvoice');


Route::get('/customer/returninvoicebv/get',
    'Customer\CustomerData@GetReturnBVInvoice');


//tree view

Route::get('/customer/treeview/{id}',
    'customer\ManageTreeView@GetAllDownloneByid')
    ->name('api.customer.treeview');


// payment

// ordertopayment

Route::get('/customer/order/createpaymentrequest/{order}',
    'customer\CreateOrderBV@RequestPayment');

Route::get('/openurl/payorder/{slug}',
    'customer\CreateOrderBV@RequestPayment');

Route::get('/openurl/payorder/status/{slug}',
    'customer\CreateOrderBV@CheckPaymentstatus');


Route::post('secure/verify/document',
    'customer\DoccumentVerification@DooucumentVerifyFace');

Route::post('secure/verify/face',
    'customer\DoccumentVerification@FaceVerify');


// pickup request

Route::post('order/puckup/request',
    'customer\ManagePickup@RequestPickup');

Route::get('order/puckup/status/{id}',
    'customer\ManagePickup@RequestPickupstatus');


Route::get('order/puckup/customer/latest',
    'customer\ManagePickup@GetCustomerPickup');
