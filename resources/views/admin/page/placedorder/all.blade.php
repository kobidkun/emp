@extends('admin.index');

@section('content')

    <div class="m-content">
        <div class="row">
            <div class="col-md-12">


                            <div class="m-portlet m-portlet--mobile">

                                <div class="m-portlet__body">
                                    <!--begin: Search Form -->


                                    <style>

                                        .table > tbody > tr > td {
                                            vertical-align: middle;
                                        }


                                        .dataTables_wrapper .dataTables_paginate .paginate_button:hover {
                                            /* color: white !important; */
                                            /* border: 1px solid #111; */
                                            background: #ffffff !important;
                                            border-color: #5867dd !important;

                                        }


                                        .table .thead-dark th {
                                            color: #fff;
                                            background-color: #5867dd!important;
                                            border-color: #32383e;
                                        }





                                    </style>




                                    <table class="table table-striped table-bordered" id="product-table">
                                        <thead class="thead-dark">
                                        <tr>
                                            <th style="width: 100px">Order ID</th>

                                            <th style="width: 80px">Customer</th>
                                            <th >Tax Amt</th>

                                            <th>TTl TAX</th>
                                            <th>Total</th>
                                            <th>Payment Type</th>
                                            <th>Booked Platform</th>

                                            <th>Action</th>
                                            <th>Date</th>



                                        </tr>
                                        </thead>
                                    </table>







                        </div>
                        <!--end::Section-->

                    </div>


            </div>
        </div>
    </div>

 @endsection


@section('admin_footer_script')





    <link rel="stylesheet" type="text/css" href="{{asset('/datatable/datatables.css')}}"/>

    <script type="text/javascript" src="{{asset('/datatable/datatables.min.js')}}"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js"></script>
    <script>
        $(function() {
            $('#product-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{{route('dashboard.placed.create.datatable')}}',
                order: [ [0, 'desc'] ],
                columns: [
                    { data: 'order_secure_id', name: 'order_secure_id' },
                  //  { data: 'customer_gstin', name: 'customer_gstin' },
                    { data: 'customer_id', name: 'customer_id' },
                    { data: 'order_total_taxable_amount', name: 'order_total_taxable_amount' },
                  //  { data: 'order_total_tax_cgst', name: 'order_total_tax_cgst' },
                   // { data: 'order_total_tax_sgst', name: 'order_total_tax_sgst' },
                    { data: 'order_total_tax', name: 'order_total_tax' },
                    { data: 'order_total', name: 'order_total' },
                    { data: 'payment_type', name: 'payment_type' },

                    { data: 'bookedfrom', name: 'bookedfrom' },
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                    {data: 'created_at', name: 'created_at', orderable: false, searchable: false},

                ]
            });

        });
    </script>



    @endsection