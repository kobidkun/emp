<?php

namespace App\Http\Controllers\Calculation;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ManageCalculation extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth:admin,storeuser,cashier']);


    }


    public function CalculateView(){
        return view('admin.page.calculations.calculation');
    }
}
