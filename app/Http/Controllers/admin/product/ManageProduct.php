<?php

namespace App\Http\Controllers\admin\product;

use App\Catagory;
use App\Category2;
use App\Category3;
use App\Http\Requests\AddProductRequest;
use App\model\Invoice\InvoiceToProduct;
use App\Product;
use App\ProductImage;
use App\ProductPrimaryImage;
use App\ProductsToCategory;
use App\ProductToCategory2;
use App\ProductToCategory3;
use App\ProductVatienttoImages;
use App\ProductVatienttoStock;
use App\VariantType;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Image;
use Input;

use Yajra\DataTables\DataTables;
use DB;

class ManageProduct extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth:admin,storeuser,cashier']);


    }


    public function CreateProduct(){
        $c = Catagory::all();
        $a = Product\ProductAtribute::all();
        return view('admin.page.product.create')->with(['cats' => $c,'atts' => $a]);
    }


    public function ImagesToAttribute(Request $request){

        $id = $request->id;

        $c = Catagory::all();
        $a = Product\ProductAtribute::all();
        return view('admin.page.product.createattributes')->with(['id' => $id,'cats' => $c,'atts' => $a]);
    }

    public function CreateProductAttribute(){
        $c = Catagory::all();
        $a = Product\ProductAtribute::all();
        return view('admin.page.product.createattributes')->with(['cats' => $c,'atts' => $a]);
    }

    public function CreateProductAttributeStore(Request $request){



        $images = $request->attribute_name;
        $images2 = $request->attribute_value;
        $images3 = $request->product_id;

        for ($i = 0; $i < count($images); $i++) {
            Product\ProductToAtribute::create([
                'name' => $images[$i],
                'value' => $images2[$i],
                'product_id' => $images3[$i]
            ]);
        }





       return redirect(route('admin.product.edit.show',$images3));
    }
    public function ProductSave(AddProductRequest $request){
      //  return \GuzzleHttp\json_encode($request);
        //dd($request);
        $s = new Product();
        $s->name = $request->name;
        $s->hsn = $request->hsn;
        $s->type = $request->type;
        $s->code_name = $request->code_name;
        $s->unit = $request->unit;
        $s->s_desc = $request->s_desc;
        $s->l_desc = $request->l_desc;
        $s->h_light = $request->h_light;



        $s->mrp_price = $request->mrp_price;


        $s->listing_price = $request->listing_price;

        $s->igst_percentage = $request->igst_percentage;
        $s->igst = $request->igst;
        $s->cgst_percentage = $request->cgst_percentage;
        $s->sgst_percentage = $request->cgst_percentage;
        $s->cgst = $request->cgst;
        $s->sgst = $request->cgst;
        $s->cess_percentage = $request->cess_percentage;
        $s->taxable_value = $request->taxable_value;
        $s->taxable_rate = $request->taxable_rate;
        $s->total_tax = $request->total_tax;
        $s->unit = $request->unit;
        $s->stock = $request->stock;



        //
        $s->catagory_id = $request->catagory_id;
        $s->catagory_id = $request->catagory_id;
        $s->catagory2_id = $request->catagory2_id;
        $s->catagory3_id = $request->catagory3_id;



        $s->slug = str_replace(' ', '_', $request->name.'-'.time());;




        //


        $s->save();


        $gpc = $request->catagory_id;
         if ($gpc > 0) {


             $pc = Catagory::findorfail($gpc);

             $a = new ProductsToCategory();
             $a->name = $pc->name;
             $a->slug = $pc->slug;
             $a->product_id = $s->id;
             $a->catagory_id = $pc->id;
             $a->save();


         }



        $gsc = $request->catagory2_id;
         if ($gsc > 0){


        $sc = Category2::findorfail($gsc);

        $b = new ProductToCategory2();
        $b->name = $sc->name;
        $b->slug = $sc->slug;
        $b->product_id = $s->id;
        $b->catagory_id = $sc->id;
        $b->save();


         }






        $gtc = $request->catagory3_id;
         if ($gtc > 0) {

             $tc = Category3::findorfail($gtc);

             $c = new ProductToCategory3();
             $c->name = $tc->name;
             $c->slug = $tc->slug;
             $c->product_id = $s->id;
             $c->catagory_id = $tc->id;
             $c->save();

         }


        return view('admin.page.product.createimage')->with(['product' => $s]);
    }

    public function UploadProductImagesPage(Request $request){

        return view('admin.page.product.createimage');


    }

    public function UploadProductImages(Request $request){




        $id = $request->id;
        $image      = $request->file('image');
        $fileName   = time() . '.' . $image->getClientOriginalExtension();

        $image_normal = Image::make($image);
        $image_thumb = Image::make($image);
        $image_normal = $image_normal->stream();
        $image_thumb = $image_thumb->stream();

        $finalImage = $image_thumb->__toString();

        // Storage::disk('s3')->put($path.$fileName, $image_normal->__toString());
        Storage::disk('s3')->put('product_image/secondary/'.$fileName, $finalImage);

        $s = new ProductImage();
        $s->images_url = $fileName;
        $s->extinsion = $fileName;
        $s->path = 'product_image/secondary/';
        $s->local_path = 'null';
        $s->cdn_url = env('cdnurl').'/product_image/secondary/'.$fileName;
        $s->product_id = $id;
        $s->name = $fileName;
        $s->save();


    }


    public function UploadProductPrimaryImages(Request $request){



        $id = $request->id;
        $image      = $request->file('image');
        $fileName   = time() . '.' . $image->getClientOriginalExtension();

        $image_normal = Image::make($image);
        $image_thumb = Image::make($image);
        $image_normal = $image_normal->stream();
        $image_thumb = $image_thumb->stream();

        $finalImage = $image_thumb->__toString();

       // Storage::disk('s3')->put($path.$fileName, $image_normal->__toString());
        Storage::disk('s3')->put('product_image/primary/'.$fileName, $finalImage);





        $s = new ProductPrimaryImage();
        $s->images_url = $fileName;
        $s->extinsion = $fileName;
        $s->path = 'product_image/primary/';
        $s->local_path = 'null';
        $s->cdn_url = env('cdnurl').'/product_image/primary/'.$fileName;
        $s->product_id = $id;
        $s->name = $fileName;
        $s->save();

        return $s->cdn_url;





    }


    public function ViewProductDetails(Request $request, $id){



        $fromDate = Carbon::now()->subMonth()->startOfMonth()->toDateString();
        $tillDate = Carbon::now()->subMonth()->endOfMonth()->toDateString();




        $p = Product::findorfail($id);
        $c = Catagory::all();
        $atts = Product\ProductAtribute::all();
        $color = VariantType::where('name', 'color' )->get();
        $size = VariantType::where('name', 'size' )->get();


        $productSale = $p->product_to_sale()
            ->whereBetween(DB::raw('date(created_at)'), [$fromDate, $tillDate])
            //->whereBetween('created_at',[$fromDate,$tillDate])
            ->get();

        $sumsale = $productSale
            ->count();
            //->sum('quantity');

       return view('admin.page.product.details')->with([
           'product' => $p,
           'cats' => $c,
           'atts' =>$atts,
           'colors' => $color,
           'sizes' => $size,
           'productSale' => $sumsale,

           ]);
     }

    public function UpdateProductdata(Request $request, $id){
      $s =  Product::findorfail($id);
        //  return 'lll';
        $s->name = $request->name;
        $s->hsn = $request->hsn;
        $s->type = $request->type;
        $s->code_name = $request->code_name;
        $s->unit = $request->unit;
        $s->mrp_price = $request->mrp_price;
        $s->listing_price = $request->listing_price;
        $s->igst_percentage = $request->igst_percentage;
        $s->igst = $request->igst;
        $s->cgst_percentage = $request->cgst_percentage;
        $s->sgst_percentage = $request->cgst_percentage;
        $s->cgst = $request->cgst;
        $s->sgst = $request->cgst;
        $s->cess_percentage = $request->cess_percentage;
        $s->taxable_value = $request->taxable_value;
        $s->taxable_rate = $request->taxable_rate;
        $s->total_tax = $request->total_tax;






        //$s->catagory_id = implode(',', $a);


        $s->save();

        return back();

    }

    public function UpdateProductPrimaryImage(Request $request, $id){


        $image      = $request->file('image');
        $fileName   = time() . '.' . $image->getClientOriginalExtension();

        $image_normal = Image::make($image);
        $image_thumb = Image::make($image);
        $image_normal = $image_normal->stream();
        $image_thumb = $image_thumb->stream();

        $finalImage = $image_thumb->__toString();

        // Storage::disk('s3')->put($path.$fileName, $image_normal->__toString());
        Storage::disk('s3')->put('product_image/primary/'.$fileName, $finalImage);



        $s =  ProductPrimaryImage::findorfail($id);
        Storage::disk('s3')->delete('product_image/primary/'.$s->extinsion);
        $s->images_url = $fileName;
        $s->extinsion = $fileName;
        $s->path = 'product_image/primary/';
        $s->local_path = 'null';
        $s->cdn_url = env('cdnurl').'/product_image/primary/'.$fileName;
        $s->product_id = $request->id;
        $s->name = $fileName;
        $s->save();

        return $s->cdn_url;







    }


    public function DeleteProductSecondaryImages(Request $request, $id){
       $img = ProductImage::findorfail($id);

        Storage::disk('s3')->delete('product_image/secondary/'.$img->extinsion);
        $img->delete();
        return redirect()->back();

        //  return 'lll';


    }


     public function ActivateDeactivateProduct(Request $request, $id){
        Product::findorfail($id);
          //  return 'lll';

     }


     public function DeleteProduct(Request $request, $id){
      $d =  Product::with(
          'products_to_categories',
          'products_to_categories',
          'products_to_categories2',
          'products_to_categories3',
          'product_images'

      )->findorfail($id);
          //  return 'lll';

      //   dd($d);


         if ($d->products_to_categories !== null) {
             $d->products_to_categories->delete();

         }


         if ($d->products_to_categories2 !== null) {
             $d->products_to_categories2->delete();
         }


         if ($d->products_to_categories3 !== null) {
             $d->products_to_categories3->delete();
         }

         if ($d->product_primary_images !== null) {


           $piname =  $d->product_primary_images;
             Storage::disk('s3')->delete('product_image/primary/'.$piname->extinsion);



             $d->product_primary_images->delete();
         }

       //  if ($d->product_images !== null) {
         //    $d->product_images->delete();
        // }

         if ($d->product_images !== null) {
               foreach ($d->product_images as $img){


                   Storage::disk('s3')->delete('product_image/secondary/'.$img->extinsion);
                  $img->delete();
                 }

         }


         if ($d->product_to_varients !== null) {


             $var_images = ProductVatienttoImages::where('product_id',$id )->get();



               foreach ($var_images as $var_image){

                   Storage::disk('s3')->delete('product_variant_image/secondary/'.$var_image->extinsion);

                   $var_image->delete();

                   }



                   $var_stocks = ProductVatienttoStock::where('prim_product',$id )->get();



               foreach ($var_stocks as $var_stock){

                   $var_stock->delete();

                   }


             foreach ($d->product_to_varients as $var){


                 $var->delete();

             }






         }


       $d->delete();

         return redirect(route('admin.product.all'));



     }


    public function AllProduct(){
        return view('admin.page.product.all');
    }

    public function getProductDatatables()
    {
        $products = Product::select(
            ['id','name','code_name',
                'stock',
                'mrp_price',
                'igst_percentage',

            'listing_price','slug','created_at']);

      //  return DataTables::of($products)
        //    ->make(true);



        return Datatables::of($products)
            ->editColumn('created_at', function($product) {
                return Carbon::createFromFormat('Y-m-d H:i:s', $product->created_at)->toDayDateTimeString();
            })
            ->addColumn('action', function ($product) {
                return '<a href="/admin/product/edit/'.$product->id.'" class=" btn btn-xs btn-primary" title="View details"><i class="la la-edit"></i>View Details</a>';
            })

            ->editColumn('stock', function ($product) {


             return   $product->product_to_varients->sum('stock');


                /*return $product->product_to_varients->map(function($product_to_varients) {
                    return $product_to_varients->;
                });*/


            })

            ->addColumn('product_to_varients', function ($product) {

                $a = \App\ProductVatient::where('product_id', $product->id)
                    ->select([
                        'name',
                        'id',
                        'color',
                        'size',
                    ])
                    ->get();

               // return $a;  $product->id


                return $product->product_to_varients->map(function($product_to_varients) {
                    return ('<a style="margin-top:5;" href="'.route('admin.product.varient.details',$product_to_varients->id).'" class="btn m-btn--pill m-btn--air         btn-outline-success m-btn m-btn--custom m-btn--outline-2x">'.$product_to_varients->color.' '.$product_to_varients->size .'</a>');
                })->implode('<br>');



            })

            ->addColumn('delete', function ($product) {
                return '<a href="'.route('admin.product.whole.delete',$product->id).'" class="btn btn-xs btn-danger" title="Delete"><i class="la la-trash"></i>Delete</a>';
            })
            ->rawColumns(['action','delete','product_to_varients'])
            ->orderColumn('id', 'name $1')
            ->make();





    }



    public function GetAllProductiaSearch(Request $request){


        $query = $request->get('keyword','');

        $products = \App\ProductVatient::where('name', 'LIKE', '%' . $query . '%')
            ->orwhere('code_name', 'LIKE', '%' . $query . '%')
            ->take(25)->get();

        $data=array();
        foreach ($products as $product) {
            $data[]=array(
                'value'=>$product->name,
                'id'=>$product->id,
                'code_name'=>$product->code_name,
                'parent_product_id'=>$product->product_id,
                'hsn'=>$product->hsn,
                'type'=>$product->type,
                'color'=>$product->color,
                'size'=>$product->size,
                'base_price'=>$product->base_price,
                'disc_price'=>$product->disc_price,
                'offer_price'=>$product->offer_price,
                'mrp_price'=>$product->mrp_price,
                'disc_amount'=>$product->disc_amount,
                'discount_price'=>$product->discount_price,
                'discount_per' => $product->discount_per,
                'listing_price' => $product->listing_price,
                'dis_per' => $product->dis_per,
                'igst_percentage' => $product->igst_percentage,
                'igst' => $product->igst,
                'cgst_percentage' => $product->cgst_percentage,
                'sgst_percentage' => $product->sgst_percentage,
                'cgst' => $product->cgst,
                'sgst' => $product->sgst,
                'cess_percentage' => $product->cess_percentage,
                'taxable_value' => $product->taxable_value,
                'total_tax' => $product->total_tax,
                'unit' => $product->unit,
                'stock' => $product->stock,
                'business_vol' => $product->business_vol,

            );
        }
        if(count($data))
            return $data;
        else
            return ['value'=>'No Result Found','id'=>''];
    }



}
