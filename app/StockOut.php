<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StockOut extends Model
{
    protected $fillable = [

        'customer_id',
        'invoice_id',
        'product_id',
        'create_invoices_id',
        'parent_product_id',
        'product_variant_id',
        'stock_out_quantity',

    ];
}
