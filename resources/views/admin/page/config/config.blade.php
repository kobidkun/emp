@extends('admin.index');

@section('content')



    <div class="m-content">
        <div class="row">
            <div class="col-md-12">
                <!--begin::Portlet-->
                <div class="m-portlet m-portlet--tab">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
												<span class="m-portlet__head-icon m--hide">
													<i class="la la-gear"></i>
												</span>
                                <h3 class="m-portlet__head-text">
                                    View App Config
                                </h3>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <!--begin::Section-->
                        <div class="m-section">

                            <link rel="stylesheet" href="https://codemirror.net/lib/codemirror.css">
                            <link rel="stylesheet" href="https://codemirror.net/theme/3024-day.css">
                            <link rel="stylesheet" href="https://codemirror.net/theme/3024-night.css">
                            <link rel="stylesheet" href="https://codemirror.net/theme/ambiance.css">
                            <link rel="stylesheet" href="https://codemirror.net/theme/base16-dark.css">
                            <link rel="stylesheet" href="https://codemirror.net/theme/base16-light.css">
                            <link rel="stylesheet" href="https://codemirror.net/theme/blackboard.css">
                            <link rel="stylesheet" href="https://codemirror.net/theme/cobalt.css">
                            <link rel="stylesheet" href="https://codemirror.net/theme/eclipse.css">
                            <link rel="stylesheet" href="https://codemirror.net/theme/elegant.css">
                            <link rel="stylesheet" href="https://codemirror.net/theme/erlang-dark.css">
                            <link rel="stylesheet" href="https://codemirror.net/theme/lesser-dark.css">
                            <link rel="stylesheet" href="https://codemirror.net/theme/midnight.css">
                            <link rel="stylesheet" href="https://codemirror.net/theme/monokai.css">
                            <link rel="stylesheet" href="https://codemirror.net/theme/neat.css">

                            <form

                                    enctype="multipart/form-data"
                                  action="{{route('admin.config.save',$code->id)}}" method="post">
                                @csrf
                                <textarea id="queryText"
                                                                         style="background-color: #0a0a0a"
                                                                         name="config" cols="120" rows="30">
          {{  $code->config}}

    </textarea>

                                <button type="submit" class="btn  btn-primary">Update</button>
                            </form >
                            <script src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.52.2/codemirror.min.js" integrity="sha256-id5Qk/MwQJxgNlDFDpVymUuReXfTUZiaQKb8arrddQM=" crossorigin="anonymous"></script>

                            <script type="text/javascript">

                                CodeMirror.fromTextArea(document.getElementById('queryText'), {
                                    mode: "application/ld+json",
                                    height: "150px",
                                    stylesheet: "https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.52.2/codemirror.min.css",
                                    textWrapping: true,
                                    theme: "blackboard"
                                });

                            </script>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



    @endsection

@section('admin_footer_script')


    @endsection