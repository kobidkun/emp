@extends('admin.index');

@section('content')
    <style>
        .select2-options {


        }
    </style>
    <div class="m-content">
        <div class="row">
            <div class="col-md-12">



                <div class="m-portlet">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
												<span class="m-portlet__head-icon m--hide">
													<i class="la la-gear"></i>
												</span>
                                <h3 class="m-portlet__head-text">
                                    Order to Invoice
                                </h3>
                            </div>
                        </div>
                    </div>


                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                @endif



                <!--begin::Form-->
                    <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed"
                          role="form" method="post"
                          action="{{ route('convert.placedorder.to.bvinvoice',$o->id) }}"
                    >
                        {{ csrf_field() }}
                        <div class="m-portlet__body">



                            <div class="form-group m-form__group row">
                                <div class="col-lg-3">

                                    <label for="exampleInputEmail1">
                                        Remarks
                                    </label>
                                    <div class="input-group m-input-group">
                                        <div class="input-group-prepend">
                                                         <span class="input-group-text">
                                                           #
                                                         </span>
                                        </div>
                                        <input type="text"
                                               class="form-control m-input--air invoice_number"
                                               placeholder="Remark"
                                               name="remark"
                                               value="{{$o->lead_purchase}}"
                                               autocomplete="off"
                                               aria-describedby="basic-addon1">


                                    </div>
                                </div>
                                <div class="col-lg-3">

                                    <label for="exampleInputEmail1">
                                        Invoice Date
                                    </label>
                                    <div class="input-group m-input-group">
                                        <div class="input-group-prepend">
                                                         <span class="input-group-text">
                                                          <i class="la la-calendar"></i>
                                                         </span>
                                        </div>
                                        <input type="text"
                                               class="form-control m-input--air invoice_date"
                                               placeholder="Invoice Date"
                                               name="invoice_date"
                                               value="{{ date('d-m-Y') }}"
                                               id="m_datepicker_1" readonly
                                               autocomplete="off"
                                               aria-describedby="basic-addon1">


                                    </div>
                                </div>
                                <div class="col-lg-3">

                                    <label for="exampleInputEmail1">
                                        Return Month
                                    </label>
                                    <div class="input-group m-input-group">
                                        <div class="input-group-prepend">
                                                         <span class="input-group-text">
                                                          <i class="la la-calendar"></i>
                                                         </span>
                                        </div>
                                        <input type="text"
                                               id="m_datepicker_return_month"
                                               class="form-control m-input--air invoice_return_month"
                                               placeholder="Return Month"
                                               value="{{ date('m-Y') }}"
                                               name="invoice_return_month"
                                               autocomplete="off"
                                               aria-describedby="basic-addon1">


                                    </div>
                                </div>
                                <div class="col-lg-3">

                                    <label for="exampleInputEmail1">
                                        Return Quater
                                    </label>
                                    <div class="input-group m-input-group">

                                        <select name="invoice_return_quater" class="form-control invoice_return_quater" id="invoice_return_quater">
                                            <option value="OCT-DEC 2019">JAN-MAR 2020</option>
                                            <option value="APR-JUN 2019">APR-JUN 2020</option>
                                            <option value="JUL-SEPT 2019">JUL-SEPT 2020</option>
                                            <option value="OCT-DEC 2019">OCT-DEC 2020</option>
                                            <option value="OCT-DEC 2019">JAN-MAR 2021</option>
                                            <option value="APR-JUN 2019">APR-JUN 2021</option>
                                            <option value="JUL-SEPT 2019">JUL-SEPT 2021</option>
                                            <option value="OCT-DEC 2019">OCT-DEC 2021</option>


                                        </select>


                                    </div>
                                </div>







                            </div>



                            <h3 style="text-align: center">Shipping Address</h3>

                            <div class="form-group m-form__group row">

                                <div class="col-lg-3">

                                    <label for="exampleInputEmail1">
                                        Street
                                    </label>
                                    <div class="input-group m-input-group">

                                        <input type="text"
                                               value="{{$c->street}}"
                                               class="form-control m-input--air invoice_number"
                                               placeholder="Street"
                                               name="invoice_shipping_address_street"
                                               autocomplete="off"
                                               aria-describedby="basic-addon1">


                                    </div>
                                </div>
                                <div class="col-lg-3">

                                    <label for="exampleInputEmail1">
                                        Locality
                                    </label>
                                    <div class="input-group m-input-group">

                                        <input type="text"
                                               class="form-control m-input--air invoice_date"
                                               placeholder="Locality"
                                               name="invoice_shipping_address_locality"
                                               value="{{$c->locality}}"

                                               autocomplete="off"
                                               aria-describedby="basic-addon1">


                                    </div>
                                </div>




                                <div class="col-lg-3">

                                    <label for="exampleInputEmail1">
                                        City
                                    </label>
                                    <div class="input-group m-input-group">

                                        <input type="text"
                                               id="m_datepicker_return_month"
                                               class="form-control m-input--air invoice_return_month"
                                               placeholder="City"
                                               value="{{$c->city}}"
                                               name="invoice_shipping_address_city"
                                               autocomplete="off"
                                               aria-describedby="basic-addon1">


                                    </div>
                                </div>


                                <div class="col-lg-3">

                                    <label for="exampleInputEmail1">
                                        State
                                    </label>
                                    <div class="input-group m-input-group">

                                        <input type="text"
                                               id="m_datepicker_return_month"
                                               class="form-control m-input--air invoice_return_month"
                                               placeholder="State"
                                               value="{{$c->state}}"
                                               name="invoice_shipping_address_state"
                                               autocomplete="off"
                                               aria-describedby="basic-addon1">


                                    </div>
                                </div>


                                <div class="col-lg-3">

                                    <label for="exampleInputEmail1">
                                        Country
                                    </label>
                                    <div class="input-group m-input-group">

                                        <input type="text"

                                               class="form-control m-input--air invoice_return_month"
                                               placeholder="Country"
                                               value="{{$c->country}}"
                                               name="invoice_shipping_address_country"
                                               autocomplete="off"
                                               aria-describedby="basic-addon1">


                                    </div>
                                </div>



                                <div class="col-lg-3">

                                    <label for="exampleInputEmail1">
                                        Pincode
                                    </label>
                                    <div class="input-group m-input-group">

                                        <input type="text"

                                               class="form-control m-input--air invoice_return_month"
                                               placeholder="Pincode"
                                               name="invoice_shipping_address_pin"
                                               autocomplete="off"
                                               value="{{$c->pin}}"
                                               aria-describedby="basic-addon1">


                                    </div>
                                </div>








                            </div>



                            <h3 style="text-align: center">Payment Medium</h3>

                            <div class="form-group m-form__group row">

                                @if ($p === null)
                                    <div class="col-lg-3">

                                        <label for="exampleInputEmail1">
                                            Payment Details
                                        </label>
                                        <div class="input-group m-input-group">

                                            <select class="form-control" name="payment" id="paymentselector">
                                                <option value="0">Cash</option>
                                                <option value="1">Online</option>
                                                <option value="2">In Store Debit Credit Card</option>
                                                <option value="3">Cheque</option>
                                            </select>


                                        </div>
                                    </div>

                                @elseif ($p !== null && $p->status === 'pending')

                                    <div class="col-lg-3">

                                        <label for="exampleInputEmail1">
                                            Payment Details
                                        </label>
                                        <div class="input-group m-input-group">

                                            <select  class="form-control" name="payment" id="paymentselector">
                                                <option value="0">Cash</option>
                                                <option value="1">Online</option>
                                                <option value="2">In Store Debit Credit Card</option>
                                                <option value="3">Cheque</option>
                                            </select>


                                        </div>
                                    </div>



                                    @elseif ($p !== null && $p->medium === "1" && $p->status === "SUCCESS")
                                        
                                        <div class="col-lg-3">

                                            <label for="exampleInputEmail1">
                                                Payment Details
                                            </label>
                                            <div class="input-group m-input-group">

                                                <select class="form-control" name="payment" id="paymentselector">
                                                    <option value="1">Online</option>
                                                </select>


                                            </div>
                                        </div>

                                    <div class="col-lg-3">

                                        <p>User has made payment with following details</p>

                                        <br>

                                        <ul>
                                            <li>Amt : {{$p->amount}}</li>
                                            <li>Status : {{$p->status}}</li>
                                            <li>TXT : {{$p->slug}}</li>
                                            <li>Time : {{$p->updated_at}}</li>
                                        </ul>
                                        </div>

                                    @else

                                    <div class="col-lg-3">

                                        <label for="exampleInputEmail1">
                                            Payment Details
                                        </label>
                                        <div class="input-group m-input-group">

                                            <select  class="form-control" name="payment" id="paymentselector">
                                                <option value="0">Cash</option>
                                                <option value="1">Online</option>
                                                <option value="2">In Store Debit Credit Card</option>
                                                <option value="3">Cheque</option>
                                            </select>


                                        </div>
                                    </div>


                                @endif


                                    <div class="col-lg-3 group" id="1">
                                        <br>

                                        @if ($p !== null && $p->status === "SUCCESS")



                                        @elseif ($p !== null && $p->status === 'pending')

                                            <a href="#" class="btn btn-primary"  id="sendpaybtn">Send Payment Link</a>


                                            @elseif($p === null)


                                            <a href="#" class="btn btn-primary"  id="sendpaybtn">Send Payment Link</a>



                                        @endif



                                    </div>

                                    <div class="col-lg-3 group" id="2">



                                        <label for="exampleInputEmail1">
                                            Tranaction ID
                                        </label>
                                        <div class="input-group m-input-group">

                                            <input type="text"

                                                   class="form-control m-input--air invoice_return_month"
                                                   placeholder="Tranaction ID"
                                                   value=""
                                                   name="txtid"
                                                   autocomplete="off"
                                                   aria-describedby="basic-addon1">


                                        </div>



                                    </div>


                                    <div class="col-lg-3 group" id="3">



                                        <label for="exampleInputEmail1">
                                            Tranaction ID
                                        </label>
                                        <div class="input-group m-input-group">

                                            <input type="text"

                                                   class="form-control m-input--air invoice_return_month"
                                                   placeholder="Tranaction ID"
                                                   value=""
                                                   name="txtid"
                                                   autocomplete="off"
                                                   aria-describedby="basic-addon1">


                                        </div>



                                    </div>







                            </div>











                        </div>





                        <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                            <div class="m-form__actions m-form__actions--solid">
                                <div class="row">

                                    <div class="col-lg-9">
                                        <button type="submit" class=" btn btn-primary">Convert Invoice</button>
                                    </div>

                                    <div class="col-lg-3">

                                        <button id="inv_reset" type="reset" onClick="window.location.reload();"class="btn btn-secondary inv_reset">
                                            Reset
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!--end::Form-->
                </div>
                <!--end::Portlet-->





            </div>
        </div>
    </div>

@endsection

@section('admin_footer_script')


    {{--jq ui--}}
    <style>
        input.ui-autocomplete-loading {
            background: white url("{{asset('loader/ajaxloader.gif')}}") right center no-repeat;
            background-size: 32px 32px;

        }

        .item_name{
            max-width: 200px;!important;
            width: 200px;!important;
        }


    </style>




    {{--auto serch--}}




    {{--copy products end--}}

    {{--jq uiend--}}


    <script>
        //== Class definition

        var BootstrapDatepicker = function () {

            //== Private functions
            var demos = function () {
                // minimum setup
                $('#m_datepicker_1, #m_datepicker_1_validate').datepicker({
                    format: "dd-mm-yyyy",
                    todayHighlight: true,
                    orientation: "bottom left",
                    templates: {
                        leftArrow: '<i class="la la-angle-left"></i>',
                        rightArrow: '<i class="la la-angle-right"></i>'
                    }
                });

                $('#m_datepicker_return_month').datepicker({
                    format: "mm-yyyy",
                    viewMode: "months",
                    minViewMode: "months",
                    orientation: "bottom left",
                    templates: {
                        leftArrow: '<i class="la la-angle-left"></i>',
                        rightArrow: '<i class="la la-angle-right"></i>'
                    }
                });

                // minimum setup for modal demo
            }

            return {
                // public functions
                init: function() {
                    demos();
                }
            };
        }();

        jQuery(document).ready(function() {
            BootstrapDatepicker.init();
        });




    </script>

    <script>
        $(document).ready(function () {
            $('.group').hide();
            $('#paymentselector').change(function () {
                $('.group').hide();
                $('#'+$(this).val()).show();
            })
        });
    </script>


    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>


    <script>
        $( "#sendpaybtn" ).click(function() {
            Swal.fire({
                html:
                    '<p>Confirm Send Payment link to customer via SMS - Mail - Telegram </p><br>' +
                    '<a class="btn btn-primary" href="{{route('dashboard.placed.bv.payment.admin.send',
['order' => $o->order_secure_id, 'getcustomerid' => $c->id])}}">Send Payment Link</a> ',
            })
        });

    </script>





@endsection