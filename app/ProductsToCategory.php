<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductsToCategory extends Model
{
    protected $hidden = array('id',
        'description',
        'product_id',
        'updated_at',
        'created_at',
        'catagory_id',
    );

    public function products()
    {
        return $this->hasOne('App\Product','id','product_id');
    }

    public function product_primary_images()
    {
        return $this->hasone('App\ProductPrimaryImage','product_id','product_id');
    }
    public function product_to_atributes()
    {
        return $this->hasMany('App\Product\ProductToAtribute','product_id','product_id');
    }




}
