<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderToPickupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_to_pickups', function (Blueprint $table) {
            $table->increments('id');
            $table->string('customer_id');
            $table->string('create_b_v_order_id');
            $table->string('slug')->nullable();
            $table->string('status')->nullable();
            $table->string('eta')->nullable();
            $table->string('que')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_to_pickups');
    }
}
