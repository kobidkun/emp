<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomerToMoreDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_to_more_details', function (Blueprint $table) {
            $table->increments('id');
            $table->text('customer_id')->nullable();
            $table->text('aadharno')->nullable();
            $table->text('addhardata')->nullable();
            $table->text('addharimage')->nullable();
            $table->text('panrno')->nullable();
            $table->text('pandata')->nullable();
            $table->text('panimage')->nullable();
            $table->text('selfile')->nullable();
            $table->text('selfiledump')->nullable();
            $table->text('ke1')->nullable();
            $table->text('ke2')->nullable();
            $table->text('ke3')->nullable();
            $table->text('ke4')->nullable();
            $table->text('ke5')->nullable();
            $table->text('ke6')->nullable();
            $table->text('ke7')->nullable();
            $table->text('ke8')->nullable();
            $table->text('ke9')->nullable();
            $table->text('ke10')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer_to_more_details');
    }
}
