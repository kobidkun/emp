<?php

namespace App\Http\Controllers\customer;

use App\Customer;
use App\Model\BVInvoice\CreateInvoiceBV;
use App\Model\BVinvoice\Payment\PaymentDetails;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Softon\Indipay\Facades\Indipay;

class ManagePayment extends Controller
{
   public function Test($slug){

       $a = PaymentDetails::where('slug',$slug)->first();



       return view('external.base')->with(['a' => $a]);


   }

   public function CreatepaymentRequest($id){

       $inv = CreateInvoiceBV::find($id);

       $cus = Customer::find($inv->customer_id);



       $a = new PaymentDetails();

       $a->create_invoice_b_vs_id = $inv->id;
       $a->slug = time();
       $a->amount = $inv->invoice_total;
       $a->email = $cus->email;
       $a->phone = $cus->mobile;
       $a->buyer_name = $inv->invoice_customer_name;
       $a->purpose = 'Shopping';

       $a->save();

       return Back();

   }

   public function MakePayment(Request $request) {
       $a = PaymentDetails::find($request->id);




       $parameters = [




           'tid' => $a->id,

           'amount' => $a->amount,
           'email' => $a->email ,
           'phone' =>$a->phone,
           'buyer_name' => $a->buyer_name ,
           'purpose' =>  $a->purpose,

       ];

       $order = Indipay::prepare($parameters);
       return Indipay::process($order);
   }
}
