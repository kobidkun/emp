<?php

namespace App\Mail\customer;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class CreateOrder extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    public $order,$createsecureorderid,$customer_name;



    public function __construct($order,$createsecureorderid,$customer_name)
    {
        $this->order = $order;
        $this->createsecureorderid = $createsecureorderid;
        $this->customer_name = $customer_name;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        //return $this->view('view.name');


        //$pdf = PDF::loadView('service.email.neworder')->setPaper('a4');
       // return $pdf->download('invoice.pdf');


        return $this->view('service.email.orderconformation');
    }
}
