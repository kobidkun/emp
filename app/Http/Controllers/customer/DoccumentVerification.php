<?php

namespace App\Http\Controllers\customer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use File;
use Illuminate\Support\Facades\Storage;
use Aws\Rekognition\RekognitionClient;

class DoccumentVerification extends Controller
{
    public function __construct()
    {
        //$this->middleware('auth:customer-api');
    }


     public function DooucumentVerifyFace(Request $request){

         $image = $request->image;  // your base64 encoded
         $image = str_replace('data:image/jpeg;base64,', '', $image);
         $image = str_replace(' ', '+', $image);
         $imageName = str_random(10).'.'.'jpeg';

      //   \File::put(public_path(). '/secure/document/verify/' . $imageName, base64_decode($image));
        Storage::disk('local')->put('secure/doccument/verify/'.$imageName, base64_decode($image));


         $contents = Storage::disk('local')->path('secure/doccument/verify/'.$imageName);

        // return ($contents);
        // $contents = Storage::get('file.jpg');


       //  $image = fopen($contents, 'r');



         $options = [
             'region'            => 'ap-south-1',
             'version'           => 'latest'
         ];

         $rekognition = new RekognitionClient($options);



         // Get local image
         $photo = $contents;
         $fp_image = fopen($photo, 'r');
         $image = fread($fp_image, filesize($photo));
         fclose($fp_image);



         $results = $rekognition->detectText([
             'Image'         => ['Bytes' => $image],
         ])['TextDetections'];
         //return response()->json($results);


         $string = '';
         foreach($results as $item)
         {
             if($item['Type'] === 'WORD')
             {
                 $string .= $item['DetectedText'] . ' ';
             }
         }

         if(empty($string))
         {
             return response()->json('This photo does not have any words',401);
         }
         else
         {
             return response()->json($string);
         }




      //   dd($result);
         // Display info for each detected person
      //   print 'People: Image position and estimated age' . PHP_EOL;



     }


     public function FaceVerify(Request $request){

         $image = $request->image;  // your base64 encoded
         $image = str_replace('data:image/jpeg;base64,', '', $image);
         $image = str_replace(' ', '+', $image);
         $imageName = str_random(10).'.'.'jpeg';

      //   \File::put(public_path(). '/secure/document/verify/' . $imageName, base64_decode($image));
        Storage::disk('local')->put('secure/doccument/verify/'.$imageName, base64_decode($image));


         $contents = Storage::disk('local')->path('secure/doccument/verify/'.$imageName);

        // return ($contents);
        // $contents = Storage::get('file.jpg');


       //  $image = fopen($contents, 'r');



         $options = [
             'region'            => 'ap-south-1',
             'version'           => 'latest'
         ];

         $rekognition = new RekognitionClient($options);



         // Get local image
         $photo = $contents;
         $fp_image = fopen($photo, 'r');
         $image = fread($fp_image, filesize($photo));
         fclose($fp_image);


         $result = $rekognition->DetectFaces(array(
                 'Image' => array(
                     'Bytes' => $image,
                 ),
                 'Attributes' => array('ALL')
             )
         );


        return $result['FaceDetails'];




      //   dd($result);
         // Display info for each detected person
      //   print 'People: Image position and estimated age' . PHP_EOL;



     }
}
