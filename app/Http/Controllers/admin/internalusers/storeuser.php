<?php

namespace App\Http\Controllers\admin\internalusers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class storeuser extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth:admin']);


    }


    public function View(){


        return view('admin.page.internaluser.storeuser.create');

    }

    public function create(){

        $a = \App\StoreUser::paginate(20);

        return view('admin.page.internaluser.storeuser.create')->with(['storeuser' => $a]);
    }

    public function SaveUsers(Request $request){

       $a = new \App\StoreUser();


       $password = '111111';


       $a->name = $request->name;
       $a->email = $request->email;
       $a->mobile = $request->mobile;
       $a->password = bcrypt($request->password);
       $a->user_type = 'storeuser';
       $a->save();

       return back();




    }

    public function Delete($id){

        $a = \App\StoreUser::findorfail($id);
        $a->delete();
        return back();
    }

    public function Edit(){

        return view('admin.page.internaluser.storeuser.create');
    }
}
