@extends('admin.index');

@section('content')

    <div class="m-content">
        <div class="row">
            <div class="col-md-6">
                <!--begin::Portlet-->
                <div class="m-portlet m-portlet--tab">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
												<span class="m-portlet__head-icon m--hide">
													<i class="la la-gear"></i>
												</span>
                                <h3 class="m-portlet__head-text">
                                    Create Secondary Category
                                </h3>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <!--begin::Section-->
                        <div class="m-section">


                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif




                            <form class="m-form m-form--fit m-form--label-align-right"
                                  role="form" method="post"
                                  action="{{ route('admin.category.sec.create') }}"
                            >
                                {{ csrf_field() }}
                                <div class="m-portlet__body">



                                        <div class="col-lg-12 col-md-12 col-sm-12">

                                            <label for="exampleInputEmail1">
                                                Select Primary Category
                                            </label>
                                            <select class="form-control m-select2" style="padding-bottom: 20px"

                                                    id="m_select2_1" name="catagory_id">

                                                @foreach($primarycategories as $primarycategory)
                                                    <option value="{{$primarycategory->id}}">
                                                        {{$primarycategory->name}}
                                                    </option>

                                                    @endforeach
                                            </select>
                                        </div>


                                    <br>
                                    <br>


                                    <div class="form-group m-input-group--air">
                                        <label for="exampleInputEmail1">
                                            Category
                                        </label>
                                        <div class="input-group m-input-group">
                                            <div class="input-group-prepend">
														<span class="input-group-text">
															<i class="flaticon-list"></i>
														</span>
                                            </div>
                                            <input type="text"
                                                   class="form-control m-input--air category-name"
                                                   placeholder="Category Name"
                                                   name="name"
                                                   autocomplete="off"
                                                   aria-describedby="basic-addon1">
                                        </div>
                                        <span class="m-form__help">
													Do not Create Same Category Twice
												</span>
                                    </div>
                                    <div class="form-group m-input-group--air">
                                        <label for="exampleInputEmail1">
                                            Slug
                                        </label>
                                        <div class="input-group m-input-group">
                                            <div class="input-group-prepend">
														<span class="input-group-text">
															<i class="flaticon-more-v6"></i>
														</span>
                                            </div>
                                            <input type="text"
                                                   readonly
                                                   class="form-control m-input--air category-slug"
                                                   placeholder="Slug"
                                                   name="slug"
                                                   autocomplete="off"
                                                   aria-describedby="basic-addon1">
                                        </div>
                                        <span class="m-form__help">
													This is auto generated.You cannot edit it.This is required to generate url
												</span>
                                    </div>
                                    <div class="form-group m-input-group--air">
                                        <label for="exampleInputEmail1">
                                            Description
                                        </label>
                                        <div class="input-group m-input-group">

                                            <textarea name="description" id="mytextarea">Category Description</textarea>
                                        </div>
                                        <span class="m-form__help">
													Description
												</span>
                                    </div>

                                </div>






                                <div class="m-portlet__foot m-portlet__foot--fit">
                                    <div class="m-form__actions">
                                        <button type="submit" class="btn btn-primary">
                                            Submit
                                        </button>
                                        <button type="reset" class="btn btn-secondary">
                                            Cancel
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <!--end::Section-->

                    </div>
                </div>
                <!--end::Portlet-->
                <!--begin::Portlet-->

            </div>
        </div>
    </div>

 @endsection



@section('admin_footer_script')
    <script>
        //== Class definition
        var Select2 = function() {
            //== Private functions
            var demos = function() {
                // basic
                $('#m_select2_1, #m_select2_1_validate').select2({
                    placeholder: "Please Select a Primary Category"
                });
            }



            //== Public functions
            return {
                init: function() {
                    demos();
                }
            };
        }();

        //== Initialization
        jQuery(document).ready(function() {
            Select2.init();
        });
    </script>
    <script src='{{asset('/production/js/admin/tinymce/tinymce.min.js')}}'></script>
    <script>
        tinymce.init({
            selector: '#mytextarea'
        });
    </script>
    <script>



        $(document).ready(function(){
            $(".category-name").keyup(function(){

                var cat_name_val = $( this ).val();
                var actualSlug = cat_name_val.replace(/ /g,'-').toLowerCase()

              $(".category-slug").inputVal(actualSlug);



            });
        });


    </script>

    @endsection