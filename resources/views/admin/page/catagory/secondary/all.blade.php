@extends('admin.index');

@section('content')

    <div class="m-content">
        <div class="row">
            <div class="col-md-12">


                            <div class="m-portlet m-portlet--mobile">
                                <div class="m-portlet__head">
                                    <div class="m-portlet__head-caption">
                                        <div class="m-portlet__head-title">
                                            <h3 class="m-portlet__head-text">
                                                All Customers
                                            </h3>
                                        </div>
                                    </div>
                                    <div class="m-portlet__head-tools">
                                        <ul class="m-portlet__nav">
                                            <li class="m-portlet__nav-item">
                                                <div class="m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" data-dropdown-toggle="hover" aria-expanded="true">
                                                    <a href="#"
                                                       class="m-portlet__nav-link btn btn-lg btn-secondary
                                                        m-btn m-btn--icon m-btn--icon-only m-btn--pill  m-dropdown__toggle">
                                                        <i class="la la-ellipsis-h m--font-brand"></i>
                                                    </a>

                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="m-portlet__body">
                                    <!--begin: Search Form -->
                                    <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
                                        <div class="row align-items-center">
                                            <div class="col-xl-8 order-2 order-xl-1">
                                                <div class="form-group m-form__group row align-items-center">
                                                    <div class="col-md-6">
                                                        <div class="m-input-icon m-input-icon--left">
                                                            <input type="text" class="form-control m-input"
                                                                   placeholder="Search by Name, Email, Mobile" id="generalSearch">
                                                            <span class="m-input-icon__icon m-input-icon__icon--left">
															<span>
																<i class="la la-search"></i>
															</span>
														</span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2">

                                                        <div class="d-md-none m--margin-bottom-10"></div>
                                                    </div>
                                                    <div class="col-md-4">

                                                        <div class="d-md-none m--margin-bottom-10"></div>
                                                    </div>

                                                </div>
                                            </div>
                                            <div class="col-xl-4 order-1 order-xl-2 m--align-right">
                                                <a href="{{route('admin.category.sec.create')}}"
                                                   class="btn btn-primary m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill">
												<span>
													<i class="la la-user"></i>
													<span>
														Add Secondary Category
													</span>
												</span>
                                                </a>
                                                <div class="m-separator m-separator--dashed d-xl-none"></div>
                                            </div>
                                        </div>
                                    </div>







                                    <div class="m_datatable" id="ajax_data"></div>







                        </div>
                        <!--end::Section-->

                    </div>


            </div>
        </div>
    </div>

 @endsection


@section('admin_footer_script')

    <script>

        //== Class definition

        var DatatableRemoteAjaxDemo = function() {
            //== Private functions

            // basic demo
            var demo = function() {

                var datatable = $('.m_datatable').mDatatable({
                    // datasource definition
                    data: {
                        type: 'remote',
                        source: {
                            read: {
                                // sample GET method
                                method: 'GET',
                                url: '{{route('admin.category.sec.alldatatable')}}',
                                map: function(raw) {
                                    // sample data mapping
                                    var dataSet = raw;
                                    if (typeof raw.data !== 'undefined') {
                                        dataSet = raw.data;
                                    }
                                    return dataSet;
                                },
                            },
                        },
                        pageSize: 10,
                        serverPaging: true,
                        serverFiltering: true,
                        serverSorting: true,
                    },

                    // layout definition
                    layout: {
                        scroll: false,
                        footer: false
                    },

                    // column sorting
                    sortable: true,

                    pagination: true,

                    toolbar: {
                        // toolbar items
                        items: {
                            // pagination
                            pagination: {
                                // page size select
                                pageSizeSelect: [10, 20, 30, 50, 100],
                            },
                        },
                    },

                    search: {
                        input: $('#generalSearch'),
                    },

                    // columns definition
                    columns: [
                        {
                            field: 'id',
                            title: '#',
                            sortable: false, // disable sort for this column
                            width: 40,
                            selector: false,
                            textAlign: 'center',
                        }, {
                            field: 'name',
                            title: ' Name',
                        }, {
                            field: 'slug',
                            title: 'Slug',
                        },
                        {
                            field: 'slug',
                            title: 'Slug',
                            width: 180
                        },
                        {
                            field: 'description',
                            title: 'Description',
                        },
                        {
                            field: 'Actions',
                            title: 'Actions',
                            sortable: false,
                            overflow: 'visible',
                            template: function (row, index, datatable) {
                                return '<a href="/admin/user/details/'+row.id+'" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Edit details">' +
                                    '<i class="la la-edit"></i>' +
                                    '</a>';  }},
                        {
                            field: 'Delete',
                            title: 'Delete',
                            sortable: false,
                            overflow: 'visible',
                            template: function (row, index, datatable) {
                                return '<a href="/admin/category/sec/delete/'+row.id+'" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Edit details">' +
                                    '<i class="la la-trash-o"></i>' +
                                    '</a>';  }}


                        ],
                });

                $('#m_form_status').on('change', function() {
                    datatable.search($(this).val().toLowerCase(), 'Status');
                });

                $('#m_form_type').on('change', function() {
                    datatable.search($(this).val().toLowerCase(), 'Type');
                });

                $('#m_form_status, #m_form_type').selectpicker();

            };

            return {
                // public functions
                init: function() {
                    demo();
                },
            };
        }();

        jQuery(document).ready(function() {
            DatatableRemoteAjaxDemo.init();
        });





    </script>



    @endsection