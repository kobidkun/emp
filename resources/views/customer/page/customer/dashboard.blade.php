@extends('customer.index');

@section('content')

    <div class="m-content">
        <div class="row">
            <div class="col-md-6">
                <!--begin::Portlet-->
                <div class="m-portlet m-portlet--tab">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
												<span class="m-portlet__head-icon m--hide">
													<i class="la la-gear"></i>
												</span>
                                <h3 class="m-portlet__head-text">
                                    Heading
                                </h3>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <!--begin::Section-->
                        <div class="m-section">
                            <h3 class="m-section__heading">
                                Default headings
                            </h3>
                            <span class="m-section__sub">
												All HTML headings,
												<code>
												&lt;h1&gt;
											</code>
											through
											<code>
											&lt;h6&gt;
										</code>
										, are available.
									</span>
                            <div class="m-section__content">
                                <div class="m-demo" data-code-preview="true" data-code-html="true" data-code-js="false">
                                    <div class="m-demo__preview">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <h1>
                                                    h1. Heading 1
                                                </h1>
                                                <div class="m--space-10"></div>
                                                <h2>
                                                    h2. Heading 2
                                                </h2>
                                                <div class="m--space-10"></div>
                                                <h3>
                                                    h3. Heading 3
                                                </h3>
                                                <div class="m--space-10"></div>
                                                <h4>
                                                    h4. Heading 4
                                                </h4>
                                                <div class="m--space-10"></div>
                                                <h5>
                                                    h5. Heading 5
                                                </h5>
                                                <div class="m--space-10"></div>
                                                <h6>
                                                    h6. Heading 6
                                                </h6>
                                            </div>
                                            <div class="col-md-6">
                                                <h1 class="m--font-success">
                                                    h1. Heading 1
                                                </h1>
                                                <div class="m--space-10"></div>
                                                <h2 class="m--font-info">
                                                    h2. Heading 2
                                                </h2>
                                                <div class="m--space-10"></div>
                                                <h3 class="m--font-warning">
                                                    h3. Heading 3
                                                </h3>
                                                <div class="m--space-10"></div>
                                                <h4 class="m--font-danger">
                                                    h4. Heading 4
                                                </h4>
                                                <div class="m--space-10"></div>
                                                <h5 class="m--font-primary">
                                                    h5. Heading 5
                                                </h5>
                                                <div class="m--space-10"></div>
                                                <h6 class="m--font-brand">
                                                    h6. Heading 6
                                                </h6>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--end::Section-->
                        <!--begin::Section-->
                        <div class="m-section">
                            <h3 class="m-section__heading">
                                Customizing headings
                            </h3>
                            <span class="m-section__sub">
										Use the included utility classes to recreate the small secondary heading text.
									</span>
                            <div class="m-section__content">
                                <div class="m-demo" data-code-preview="true" data-code-html="true" data-code-js="false">
                                    <div class="m-demo__preview">
                                        <h3>
                                            Fancy display heading
                                            <small class="text-muted">
                                                With faded secondary text
                                            </small>
                                        </h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--end::Section-->
                        <!--begin::Section-->
                        <div class="m-section">
                            <h3 class="m-section__heading">
                                Display headings
                            </h3>
                            <span class="m-section__sub">
										Larger, slightly more opinionated heading styles.
									</span>
                            <div class="m-section__content">
                                <div class="m-demo" data-code-preview="true" data-code-html="true" data-code-js="false">
                                    <div class="m-demo__preview">
                                        <h3 class="display-1">
                                            Display 1
                                        </h3>
                                        <h3 class="display-2">
                                            Display 2
                                        </h3>
                                        <h3 class="display-3">
                                            Display 3
                                        </h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--end::Section-->
                        <!--begin::Section-->
                        <div class="m-section">
                            <h3 class="m-section__heading">
                                Lead
                            </h3>
                            <span class="m-section__sub">
										Make a paragraph stand out by adding
										<code>
										.lead
									</code>
									.
								</span>
                            <div class="m-section__content">
                                <div class="m-demo" data-code-preview="true" data-code-html="true" data-code-js="false">
                                    <div class="m-demo__preview">
                                        <p class="lead">
                                            Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Duis mollis, est non commodo luctus.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--end::Section-->
                    </div>
                </div>
                <!--end::Portlet-->
                <!--begin::Portlet-->

            </div>
        </div>
    </div>



@endsection