<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('create_invoices', function (Blueprint $table) {
            $table->increments('id');
            $table->string('customer_id');
            $table->text('invoice_secure_id')->nullable();
            $table->text('create_orders_id')->nullable();
            $table->text('invoice_number')->nullable();
            $table->text('invoice_date')->nullable();
            $table->text('invoice_due_date')->nullable();
            $table->text('invoice_return_month')->nullable();
            $table->text('invoice_customer_name')->nullable();
            $table->text('invoice_customer_gstin')->nullable();
            $table->text('invoice_place_of_supply')->nullable();
            $table->text('invoice_return_quater')->nullable();
            $table->text('invoice_billing_address_street')->nullable();
            $table->text('invoice_billing_address_locality')->nullable();
            $table->text('invoice_billing_address_city')->nullable();
            $table->text('invoice_billing_address_state')->nullable();
            $table->text('invoice_billing_address_country')->nullable();
            $table->text('invoice_billing_address_pin')->nullable();
            $table->text('invoice_shipping_address_street')->nullable();
            $table->text('invoice_shipping_address_locality')->nullable();
            $table->text('invoice_shipping_address_city')->nullable();
            $table->text('invoice_shipping_address_state')->nullable();
            $table->text('invoice_shipping_address_country')->nullable();
            $table->text('invoice_shipping_address_pin')->nullable();
            $table->text('invoice_total_taxable_amount')->nullable();
            $table->text('invoice_total_taxable_discount_amount')->nullable();
            $table->text('invoice_total_tax_cgst')->nullable();
            $table->text('invoice_total_tax_sgst')->nullable();
            $table->text('invoice_total_tax_igst')->nullable();
            $table->text('invoice_total_tax_cess')->nullable();
            $table->text('invoice_total_tax')->nullable();
            $table->text('invoice_total_adjustment')->nullable();
            $table->text('invoice_total_tax_value')->nullable();
            $table->text('invoice_total_taxable_amt')->nullable();
            $table->text('invoice_total_discount_amt')->nullable();
            $table->text('invoice_total')->nullable();
            $table->text('invoice_number_generated')->nullable();
            $table->text('remark')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('create_invoices');
    }
}
