<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomerBVWalletsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_b_v_wallets', function (Blueprint $table) {
            $table->increments('id');
            $table->text('fname');
            $table->text('lname');
            $table->text('txt_id');
            $table->text('mobile');
            $table->text('email');
            $table->text('customer_id');
            $table->text('amount');
            $table->text('bv_value');
            $table->text('methods')->nullable();
            $table->text('gateway_charges')->nullable();
            $table->text('charges')->nullable();
            $table->text('taxes')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer_b_v_wallets');
    }
}
