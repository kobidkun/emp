let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

/*mix.js('resources/assets/js/app.js', 'public/js')
   .sass('resources/assets/sass/app.scss', 'public/css');


mix.styles([
    'resources/assets/dashboard/css/style.bundle.css',
    'resources/assets/dashboard/css/vendors.bundle.css'
], 'public/production/css/production.min.css');


mix.scripts([
    'resources/assets/dashboard/js/vendors.bundle.js',
    'resources/assets/dashboard/js/scripts.bundle.js'
], 'public/production/js/production.min.js');*/

mix.scripts([
    'resources/assets/highcharts/highcharts.js',
    'resources/assets/highcharts/data.js',
    'resources/assets/highcharts/exporting.js',
    'resources/assets/highcharts/moment.min.js',
    'resources/assets/highcharts/moment-timezone-with-data-2012-2022.min.js',
], 'public/production/js/highcharts-with-moment.min.js');