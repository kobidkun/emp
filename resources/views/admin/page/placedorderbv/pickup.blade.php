@extends('admin.index');

@section('content')

    <div class="m-content">
        <div class="row">
            <div class="col-md-12">


                            <div class="m-portlet m-portlet--mobile">

                                <div class="m-portlet__body">
                                    <!--begin: Search Form -->


                                    <style>

                                        .table > tbody > tr > td {
                                            vertical-align: middle;
                                        }


                                        .dataTables_wrapper .dataTables_paginate .paginate_button:hover {
                                            /* color: white !important; */
                                            /* border: 1px solid #111; */
                                            background: #ffffff !important;
                                            border-color: #5867dd !important;

                                        }


                                        .table .thead-dark th {
                                            color: #fff;
                                            background-color: #5867dd!important;
                                            border-color: #32383e;
                                        }





                                    </style>




                                    <table class="table table-striped table-bordered" id="product-table">
                                        <thead class="thead-dark">
                                        <tr>
                                            <th style="width: 80px">Customer</th>
                                            <th style="width: 100px">Order ID</th>
                                            <th >Payment</th>
                                            <th >AMT</th>
                                            <th>Action</th>
                                            <th>Delete</th>
                                            <th>Date</th>



                                        </tr>
                                        </thead>
                                    </table>







                        </div>
                        <!--end::Section-->

                    </div>


            </div>
        </div>
    </div>

 @endsection


@section('admin_footer_script')





    <link rel="stylesheet" type="text/css" href="{{asset('/datatable/datatables.css')}}"/>

    <script type="text/javascript" src="{{asset('/datatable/datatables.min.js')}}"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js"></script>
    <script>
        $(function() {
            $('#product-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{{route('dashboard.pickup.bv.create.datatable')}}',
                order: [ [0, 'desc'] ],
                columns: [
                    { data: 'customer_id', name: 'customer_id' },
                    { data: 'create_b_v_order_id', name: 'create_b_v_order_id' },
                    { data: 'total', name: 'total' },
                    { data: 'status', name: 'status' },
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                    {data: 'delete', name: 'delete', orderable: false, searchable: false},
                    {data: 'created_at', name: 'created_at', orderable: false, searchable: false},

                ]
            });

        });
    </script>



    @endsection