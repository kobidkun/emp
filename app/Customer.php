<?php

namespace App;

use Laravel\Passport\HasApiTokens;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Customer extends Authenticatable
{
    use HasApiTokens;

    protected $guard = 'customer';
    protected $fillable = [
        'fname',
     //   'lname',
      //  'mobile',
     //   'ref',
     //   'email',
     //   'password',
    ];

   // protected $hidden = [
 //       'password', 'remember_token',
  //  ];

    public function findForPassport($identifier) {
        return $this->orWhere('email', $identifier)->orWhere('ic_number', $identifier)->first();
    }

    public function customer_cart_items()
    {
        return $this->hasMany('App\Customer\CustomerCartItems',
            'customer_id','id');
    }

    public function wallet()
    {
        return $this->hasMany('App\Customer\CustomerWallet',
            'customer_id','id');
    }

    public function customer_to_customers()
    {
        return $this->hasOne('App\CustomerToCustomer',
            'customer_id','id');
    }

    public function customer()
    {
        return $this->hasOne('App\Customer',
            'sponser_id','ic_number');
    }

    public function customer_to_more_details()
    {
        return $this->hasOne('App\CustomerToMoreDetails',
            'customer_id','id');
    }

    public function customer_to_notifications()
    {
        return $this->hasMany('App\CustomerToNotification',
            'customer_id','id');
    }

    public function custnotifications()
    {
        return $this->hasMany('App\Model\customer\Misc\DBNotification',
            'customer_id','id');
    }



    protected $hidden = ['password','remember_token','created_at','updated_at'];
}
