<?php

namespace App\Model\Initial\Order;

use Illuminate\Database\Eloquent\Model;

class InitialCreateOrder extends Model
{
    public function initial_create_order_to_products()
    {
        return $this->hasMany('App\Model\Initial\Order\InitialCreateOrderToProduct', 'initial_create_orders_id', 'id');
    }

    public function customer()
    {
        return $this->hasOne('App\Customer', 'id', 'customer_id');
    }
}
