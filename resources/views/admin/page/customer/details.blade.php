@extends('admin.index');

@section('content')

    <div class="m-content">
        <div class="row" >




            <div class="col-xl-3 col-lg-4">
                <div class="m-portlet m-portlet--full-height">
                    <div class="m-portlet__body">
                        <div class="m-card-profile">
                            <div class="m-card-profile__title m--hide">
                                Your Profile
                            </div>
                            <div class="m-card-profile__pic">
                                <div class="m-card-profile__pic-wrapper">
                                    <img src="{{asset('/assets/app/media/img/users/user4.jpg')}}" alt=""/>
                                </div>
                            </div>



                            <div class="m-card-profile__details">
												<span class="m-card-profile__name">
													{{$customer->fname}} {{$customer->lname}}
												</span>
                                <a href="mailto: {{$customer->email}}" target="_blank" class="m-card-profile__email m-link">
                                    {{$customer->email}}
                                </a>

                                <a href="tel: {{$customer->mobile}}" target="_blank" class="m-card-profile__email m-link">
                                    {{$customer->mobile}}
                                </a>
                                <br>

                                <a href="{{route('admin.customer.delete',$customer->id)}}" class="btn btn-danger">
                                   Delete
                                </a>

                                <div>

                                </div>

                            </div>
                        </div>

                        <div class="m-portlet__body-separator"></div>


                        <div class="m-card-profile__details">



                              Wallet Balance:  {{$customer->waller_balance}}

                        </div>

                        <div class="m-card-profile__details">



                             BV  Wallet Balance:  {{$customer->bvwaller_balance}}

                        </div>

                    </div>
                </div>
            </div>



            <div class="col-xl-9 col-lg-8">



                <div class="m-portlet m-portlet--tabs">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-tools">
                            <ul class="nav nav-tabs m-tabs-line m-tabs-line--danger m-tabs-line--2x m-tabs-line--right" role="tablist">

                                <li class="nav-item m-tabs__item">
                                    <a class="nav-link m-tabs__link" data-toggle="tab" href="#profileedit" role="tab">
                                        <i class="la la-user-md" aria-hidden="true"></i>
                                        Edit
                                    </a>
                                </li>

                                <li class="nav-item m-tabs__item">
                                    <a class="nav-link m-tabs__link" data-toggle="tab" href="#treeView" role="tab">
                                        <i class="la la-user-md" aria-hidden="true"></i>
                                        Tree
                                    </a>
                                </li>


                                <li class="nav-item m-tabs__item">
                                    <a class="nav-link m-tabs__link active" data-toggle="tab" href="#orderdetails" role="tab">
                                        <i class="la la-cart-plus" aria-hidden="true"></i>
                                        Order
                                    </a>
                                </li>
                                <li class="nav-item m-tabs__item">
                                    <a class="nav-link m-tabs__link" data-toggle="tab" href="#customerfiles" role="tab">
                                        <i class="la la-cog" aria-hidden="true"></i>
                                        Files
                                    </a>
                                </li>
                                <li class="nav-item m-tabs__item">
                                    <a class="nav-link m-tabs__link" data-toggle="tab" href="#changepassword" role="tab">
                                        <i class="fa fa-tags" aria-hidden="true"></i>
                                        PWD
                                    </a>
                                </li>

                                <li class="nav-item m-tabs__item">
                                    <a class="nav-link m-tabs__link" data-toggle="tab" href="#rank" role="tab">
                                        <i class="fa fa-inr" aria-hidden="true"></i>
                                        Rank
                                    </a>
                                </li>

                                <li class="nav-item m-tabs__item">
                                    <a class="nav-link m-tabs__link" data-toggle="tab" href="#waller" role="tab">
                                        <i class="la la-google-wallet" aria-hidden="true"></i>
                                        Wallet
                                    </a>
                                </li>

                                <li class="nav-item m-tabs__item">
                                    <a class="nav-link m-tabs__link" data-toggle="tab" href="#wallerbv" role="tab">
                                        <i class="la la-google-wallet" aria-hidden="true"></i>
                                        BV Wallet
                                    </a>
                                </li>


                            </ul>
                        </div>
                    </div>


                    <div class="m-portlet__body">
                        <div class="tab-content">
                            <div class="tab-pane active" id="orderdetails" role="tabpanel">


                                <table class="table table-bordered" id="report-table">
                                    <thead>
                                    <tr>
                                        <th>Inv #</th>
                                        <th>Date</th>
                                        <th>Tax</th>
                                        <th>Tax Val</th>
                                        <th>Total</th>
                                        <th>Return Quarter </th>
                                        <th>Return Month</th>
                                        <th>Last update</th>
                                        <th>Action</th>


                                    </tr>
                                    </thead>
                                </table>


                            </div>




                            <div class="tab-pane" id="profileedit" role="tabpanel">

                                <form   role="form" method="post"
                                      action="{{ route('admin.customer.update.user',$customer->id) }}"
                                >
                                    {{ csrf_field() }}
                                    <div class="m-portlet__body">
                                        <div class="form-group m-form__group row">
                                            <div class="col-lg-4">
                                                <label for="exampleInputEmail1">
                                                    First Name
                                                </label>
                                                <div class="input-group m-input-group">
                                                    <div class="input-group-prepend">
														<span class="input-group-text">
															<i class="la la-user"></i>
														</span>
                                                    </div>
                                                    <input type="text"
                                                           class="form-control m-input--air"
                                                           placeholder="First Name"
                                                           name="fname"
                                                           value="{{$customer->fname}}"
                                                           autocomplete="off"
                                                           aria-describedby="basic-addon1">
                                                </div>
                                                <span class="m-form__help">
													First Name of customer
												</span>
                                            </div>
                                            <div class="col-lg-4">
                                                <label for="exampleInputEmail1">
                                                    Last Name
                                                </label>
                                                <div class="input-group m-input-group">
                                                    <div class="input-group-prepend">
														<span class="input-group-text">
															<i class="la la-users"></i>
														</span>
                                                    </div>
                                                    <input type="text"
                                                           class="form-control m-input--air"
                                                           placeholder="Last Name"
                                                           name="lname"  value="{{$customer->lname}}"
                                                           autocomplete="off"
                                                           aria-describedby="basic-addon1">
                                                </div>
                                                <span class="m-form__help">
													Last Name of customer
												</span>
                                            </div>
                                            <div class="col-lg-4">
                                                <label for="exampleInputEmail1">
                                                    Gurdian's Name
                                                </label>
                                                <div class="input-group m-input-group">
                                                    <div class="input-group-prepend">
														<span class="input-group-text">
															<i class="la la-child"></i>
														</span>
                                                    </div>
                                                    <input type="text"
                                                           class="form-control m-input--air"
                                                           placeholder="Gurdian's Name"
                                                           name="g_name"   value="{{$customer->g_name}}"
                                                           autocomplete="off"
                                                           aria-describedby="basic-addon1">
                                                </div>
                                                <span class="m-form__help">
													Father, Gurdian, Husband's Name
												</span>
                                            </div>
                                            <div class="col-lg-4">
                                                <label for="exampleInputEmail1">
                                                    Date of Birth
                                                </label>
                                                <div class="input-group m-input-group">
                                                    <div class="input-group-prepend">
														<span class="input-group-text">
															<i class="la la-calendar"></i>
														</span>
                                                    </div>
                                                    <input type="text"
                                                           class="form-control m-input--air"
                                                           placeholder="Date of Birth"
                                                           id="m_datepicker_1" readonly
                                                           name="dob"  value="{{$customer->dob}}"
                                                           autocomplete="off"
                                                           aria-describedby="basic-addon1">
                                                </div>
                                                <span class="m-form__help">
													Applicant's Date of Birth
												</span>
                                            </div>
                                            <div class="col-lg-4">
                                                <label for="exampleInputEmail1">
                                                    Pan Number for Indian
                                                </label>
                                                <div class="input-group m-input-group">
                                                    <div class="input-group-prepend">
														<span class="input-group-text">
															<i class="la la-square-o"></i>
														</span>
                                                    </div>
                                                    <input type="text"
                                                           class="form-control m-input--air"
                                                           placeholder="Pan Number"
                                                           name="pan"  value="{{$customer->pan}}"
                                                           autocomplete="off"
                                                           aria-describedby="basic-addon1">
                                                </div>
                                                <span class="m-form__help">
													Applicant's Pan Number if Indian
												</span>
                                            </div>
                                            <div class="col-lg-4">
                                                <label for="exampleInputEmail1">
                                                    Aadhar for Indian
                                                </label>
                                                <div class="input-group m-input-group">
                                                    <div class="input-group-prepend">
														<span class="input-group-text">
															<i class="la la-tablet"></i>
														</span>
                                                    </div>
                                                    <input type="text"
                                                           class="form-control m-input--air"
                                                           placeholder="Aadhar Number"
                                                           name="aadhar"  value="{{$customer->aadhar}}"
                                                           autocomplete="off"
                                                           aria-describedby="basic-addon1">
                                                </div>
                                                <span class="m-form__help">
													Applicant's Aadhar Number if Indian
												</span>
                                            </div>
                                            <div class="col-lg-4">
                                                <label for="exampleInputEmail1">
                                                    Billing Name
                                                </label>
                                                <div class="input-group m-input-group">
                                                    <div class="input-group-prepend">
														<span class="input-group-text">
															<i class="la la-building-o"></i>
														</span>
                                                    </div>
                                                    <input type="text"
                                                           class="form-control m-input--air"
                                                           placeholder="Billing Name"
                                                           name="billing_name"  value="{{$customer->billing_name}}"
                                                           autocomplete="off"
                                                           aria-describedby="basic-addon1">
                                                </div>
                                                <span class="m-form__help">
													Billing Name
												</span>
                                            </div>
                                            <div class="col-lg-4">
                                                <label for="exampleInputEmail1">
                                                    GSTIN for Indian
                                                </label>
                                                <div class="input-group m-input-group">
                                                    <div class="input-group-prepend">
														<span class="input-group-text">
															<i class="la la-life-ring"></i>
														</span>
                                                    </div>
                                                    <input type="text"
                                                           class="form-control m-input--air"
                                                           placeholder="GSTIN Number"
                                                           name="gstin"  value="{{$customer->gstin}}"
                                                           autocomplete="off"
                                                           aria-describedby="basic-addon1">
                                                </div>
                                                <span class="m-form__help">
													Applicant's GSTIN Number if Indian
												</span>
                                            </div>
                                            <div class="col-lg-4">
                                                <label for="exampleInputEmail1">
                                                    GSTIN State
                                                </label>
                                                <select  name="gstin_state" class="form-control m-input">
                                                    <option selected>  {{$customer->gstin_state}}</option>
                                                    <option value="35">Andaman and Nicobar</option>
                                                    <option value="37">Andhra Pradesh</option>
                                                    <option value="12">Arunachal Pradesh</option>
                                                    <option value="18">Assam</option>
                                                    <option value="10">Bihar</option>
                                                    <!-- <option value="Chandigarh">Chandigarh</option> -->
                                                    <option value="04">Chhattisgarh</option>
                                                    <option value="26">Dadra and Nagar Haveli</option>
                                                    <option value="25">Daman and Diu</option>
                                                    <option value="07">Delhi</option>
                                                    <!-- <option value="Foreign">Foreign</option> -->
                                                    <option value="30">Goa</option>
                                                    <option value="24">Gujarat</option>
                                                    <option value="06">Haryana</option>
                                                    <option value="02">Himachal Pradesh</option>
                                                    <option value="01">Jammu and Kashmir</option>
                                                    <option value="20">Jharkhand</option>
                                                    <option value="29">Karnataka</option>
                                                    <option value="32">Kerala</option>
                                                    <option value="31">Lakshadweep</option>
                                                    <option value="23">Madhya Pradesh</option>
                                                    <option value="27">Maharastra</option>
                                                    <option value="14">Manipur</option>
                                                    <option value="17">Meghalaya</option>
                                                    <option value="15">Mizoram</option>
                                                    <option value="13">Nagaland</option>
                                                    <option value="21">Orissa</option>
                                                    <option value="34">Puducherry</option>
                                                    <option value="03">Punjab</option>
                                                    <option value="08">Rajasthan</option>
                                                    <option value="11">Sikkim</option>
                                                    <option value="33">Tamil Nadu</option>
                                                    <option value="36">Telangana</option>
                                                    <option value="16">Tripura</option>
                                                    <option value="09">Uttar Pradesh</option>
                                                    <option value="05">Uttarakhand</option>
                                                    <option value="19">West Bengal</option>

                                                </select>
                                            </div>



                                            <div class="col-lg-4">
                                                <label for="exampleInputEmail1">
                                                    IC Number
                                                </label>
                                                <div class="input-group m-input-group">
                                                    <div class="input-group-prepend">
														<span class="input-group-text">
															<i class="la la-user-plus"></i>
														</span>
                                                    </div>
                                                    <input type="text"
                                                           readonly
                                                           class="form-control m-input--air"
                                                           placeholder="IC Number"
                                                           name="ic_number"  value="{{$customer->ic_number}}"
                                                           autocomplete="off"
                                                           aria-describedby="basic-addon1">
                                                </div>
                                                <span class="m-form__help">
													Applicant's IC Number
												</span>
                                            </div>

                                            <div class="col-lg-4">
                                                <label for="exampleInputEmail1">
                                                    Sponser Id
                                                </label>
                                                <div class="input-group m-input-group">
                                                    <div class="input-group-prepend">
														<span class="input-group-text">
															<i class="la la-user-plus"></i>
														</span>
                                                    </div>
                                                    <input type="text"
                                                           readonly
                                                           class="form-control m-input--air"
                                                           placeholder="Sponser Id"
                                                           name="sponser_id"  value="{{$customer->sponser_id}}"
                                                           autocomplete="off"
                                                           aria-describedby="basic-addon1">
                                                </div>
                                                <span class="m-form__help">
													Applicant's Sponser Id
												</span>
                                            </div>




                                            <div class="col-lg-4">
                                                <label for="exampleInputEmail1">
                                                    Rank
                                                </label>
                                                <div class="input-group m-input-group">
                                                    <div class="input-group-prepend">
														<span class="input-group-text">
															%
														</span>
                                                    </div>
                                                    <input type="text"
                                                           readonly
                                                           class="form-control m-input--air"
                                                           placeholder="Rank"
                                                           name="commission"   value="{{$customer->commission}}"
                                                           autocomplete="off"
                                                           aria-describedby="basic-addon1">
                                                </div>
                                                <span class="m-form__help">
													Applicant's Rank
												</span>
                                            </div>


                                        </div>



                                        <div class="form-group m-form__group row">
                                            <div class="col-lg-4">
                                                <label for="exampleInputEmail1">
                                                    Email
                                                </label>
                                                <div class="input-group m-input-group">
                                                    <div class="input-group-prepend">
														<span class="input-group-text">
															<i class="la la-envelope"></i>
														</span>
                                                    </div>
                                                    <input type="text"
                                                           class="form-control m-input--air"
                                                           placeholder="Email"  value="{{$customer->email}}"
                                                           name="email"
                                                           autocomplete="off"
                                                           aria-describedby="basic-addon1">
                                                </div>
                                                <span class="m-form__help">
													Email of customer
												</span>
                                            </div>
                                            <div class="col-lg-4">
                                                <label for="exampleInputEmail1">
                                                    Mobile
                                                </label>
                                                <div class="input-group m-input-group">
                                                    <div class="input-group-prepend">
														<span class="input-group-text">
															<i class="la la-mobile-phone"></i>
														</span>
                                                    </div>
                                                    <input type="text"
                                                           class="form-control m-input--air"
                                                           placeholder="Mobile"
                                                           name="mobile"   value="{{$customer->mobile}}"
                                                           autocomplete="off"
                                                           aria-describedby="basic-addon1">
                                                </div>
                                                <span class="m-form__help">
													Mobile of customer
												</span>
                                            </div>
                                            <div class="col-lg-4">
                                                <label for="exampleInputEmail1">
                                                    Phone Number
                                                </label>
                                                <div class="input-group m-input-group">
                                                    <div class="input-group-prepend">
														<span class="input-group-text">
															<i class="la la-phone"></i>
														</span>
                                                    </div>
                                                    <input type="text"
                                                           class="form-control m-input--air"
                                                           placeholder="Phone Number"
                                                           name="phone"   value="{{$customer->phone}}"
                                                           autocomplete="off"
                                                           aria-describedby="basic-addon1">
                                                </div>
                                                <span class="m-form__help">
													Phone Number of customer
												</span>
                                            </div>
                                        </div>


                                        <div class="form-group m-form__group row">
                                            <div class="col-lg-4">
                                                <label for="exampleInputEmail1">
                                                    Street Name
                                                </label>
                                                <div class="input-group m-input-group">
                                                    <div class="input-group-prepend">
														<span class="input-group-text">
															<i class="la la-street-view"></i>
														</span>
                                                    </div>
                                                    <input type="text"
                                                           class="form-control m-input--air"
                                                           placeholder="Street Name"
                                                           name="street"   value="{{$customer->street}}"
                                                           autocomplete="off"
                                                           aria-describedby="basic-addon1">
                                                </div>



                                            </div>
                                            <div class="col-lg-4">
                                                <label for="exampleInputEmail1">
                                                    Locality
                                                </label>
                                                <div class="input-group m-input-group">
                                                    <div class="input-group-prepend">
														<span class="input-group-text">
															<i class="la la-location-arrow"></i>
														</span>
                                                    </div>
                                                    <input type="text"
                                                           class="form-control m-input--air"
                                                           placeholder="Locality"
                                                           name="locality"   value="{{$customer->locality}}"
                                                           autocomplete="off"
                                                           aria-describedby="basic-addon1">
                                                </div>



                                            </div>
                                            <div class="col-lg-4">
                                                <label for="exampleInputEmail1">
                                                    City / Village
                                                </label>
                                                <div class="input-group m-input-group">
                                                    <div class="input-group-prepend">
														<span class="input-group-text">
															<i class="la la-map-signs"></i>
														</span>
                                                    </div>
                                                    <input type="text"
                                                           class="form-control m-input--air"
                                                           placeholder="City / Village"
                                                           name="city"   value="{{$customer->city}}"
                                                           autocomplete="off"
                                                           aria-describedby="basic-addon1">
                                                </div>



                                            </div>
                                            <div class="col-lg-4">
                                                <label for="exampleInputEmail1">
                                                    State
                                                </label>
                                                <div class="input-group m-input-group">
                                                    <div class="input-group-prepend">
														<span class="input-group-text">
															<i class="la la-map-o"></i>
														</span>
                                                    </div>
                                                    <input type="text"
                                                           class="form-control m-input--air"
                                                           placeholder="State"
                                                           name="state"   value="{{$customer->state}}"
                                                           autocomplete="off"
                                                           aria-describedby="basic-addon1">
                                                </div>



                                            </div>
                                            <div class="col-lg-4">
                                                <label for="exampleInputEmail1">
                                                    Country
                                                </label>
                                                <div class="input-group m-input-group">
                                                    <div class="input-group-prepend">
														<span class="input-group-text">
															<i class="la la-crosshairs"></i>
														</span>
                                                    </div>
                                                    <input type="text"
                                                           class="form-control m-input--air"
                                                           placeholder="Country"
                                                           name="country"   value="{{$customer->country}}"
                                                           autocomplete="off"
                                                           aria-describedby="basic-addon1">
                                                </div>



                                            </div>
                                            <div class="col-lg-4">
                                                <label for="exampleInputEmail1">
                                                    Pincode
                                                </label>
                                                <div class="input-group m-input-group">
                                                    <div class="input-group-prepend">
														<span class="input-group-text">
															<i class="la la-map-marker"></i>
														</span>
                                                    </div>
                                                    <input type="text"
                                                           class="form-control m-input--air"
                                                           placeholder="Pincode"
                                                           name="pin"   value="{{$customer->pin}}"
                                                           autocomplete="off"
                                                           aria-describedby="basic-addon1">
                                                </div>



                                            </div>




                                        </div>
                                        <div class="form-group m-form__group row">
                                            <div class="col-lg-4">
                                                <label for="exampleInputEmail1">
                                                    Nomiee Name
                                                </label>
                                                <div class="input-group m-input-group">
                                                    <div class="input-group-prepend">
														<span class="input-group-text">
															<i class="la la-user"></i>
														</span>
                                                    </div>
                                                    <input type="text"
                                                           class="form-control m-input--air"
                                                           placeholder="Nominee Name"
                                                           name="nominee_name"  value="{{$customer->nominee_name}}"
                                                           autocomplete="off"
                                                           aria-describedby="basic-addon1">
                                                </div>



                                            </div>
                                            <div class="col-lg-4">
                                                <label for="exampleInputEmail1">
                                                    Relation with Applicant
                                                </label>
                                                <div class="input-group m-input-group">
                                                    <div class="input-group-prepend">
														<span class="input-group-text">
															<i class="la la-venus-mars"></i>
														</span>
                                                    </div>
                                                    <input type="text"
                                                           class="form-control m-input--air"
                                                           placeholder="Relation with Applicant"
                                                           name="nominee_relation"  value="{{$customer->nominee_relation}}"
                                                           autocomplete="off"
                                                           aria-describedby="basic-addon1">
                                                </div>



                                            </div>
                                            <div class="col-lg-4">
                                                <label for="exampleInputEmail1">
                                                    Dob
                                                </label>
                                                <div class="input-group m-input-group">
                                                    <div class="input-group-prepend">
														<span class="input-group-text">
															<i class="la la-calendar"></i>
														</span>
                                                    </div>
                                                    <input type="text"
                                                           class="form-control m-input--air"
                                                           placeholder="DOB"  value="{{$customer->nominee_dob}}"
                                                           id="m_datepicker_1" readonly
                                                           name="nominee_dob"
                                                           autocomplete="off"
                                                           aria-describedby="basic-addon1">
                                                </div>



                                            </div>

                                        </div>
                                        <div class="form-group m-form__group row">
                                            <div class="col-lg-4">
                                                <label for="exampleInputEmail1">
                                                    Bank Name
                                                </label>
                                                <div class="input-group m-input-group">
                                                    <div class="input-group-prepend">
														<span class="input-group-text">
															<i class="la la-bank"></i>
														</span>
                                                    </div>
                                                    <input type="text"
                                                           class="form-control m-input--air"
                                                           placeholder="Bank Name"
                                                           name="bank_name"   value="{{$customer->bank_name}}"
                                                           autocomplete="off"
                                                           aria-describedby="basic-addon1">
                                                </div>



                                            </div>
                                            <div class="col-lg-4">
                                                <label for="exampleInputEmail1">
                                                    Bank Branch Name
                                                </label>
                                                <div class="input-group m-input-group">
                                                    <div class="input-group-prepend">
														<span class="input-group-text">
															<i class="la la-bullseye"></i>
														</span>
                                                    </div>
                                                    <input type="text"
                                                           class="form-control m-input--air"
                                                           placeholder="Bank Branch Name"
                                                           name="bank_branch"
                                                           autocomplete="off"  value="{{$customer->bank_branch}}"
                                                           aria-describedby="basic-addon1">
                                                </div>



                                            </div>
                                            <div class="col-lg-4">
                                                <label for="exampleInputEmail1">
                                                    IFSC Code
                                                </label>
                                                <div class="input-group m-input-group">
                                                    <div class="input-group-prepend">
														<span class="input-group-text">
															<i class="la la-calendar"></i>
														</span>
                                                    </div>
                                                    <input type="text"
                                                           class="form-control m-input--air"
                                                           placeholder="IFSC Code"
                                                           name="bank_ifsc"  value="{{$customer->bank_ifsc}}"
                                                           autocomplete="off"
                                                           aria-describedby="basic-addon1">
                                                </div>



                                            </div>
                                            <div class="col-lg-4">
                                                <label for="exampleInputEmail1">
                                                    Account Holder Name
                                                </label>
                                                <div class="input-group m-input-group">
                                                    <div class="input-group-prepend">
														<span class="input-group-text">
															<i class="la la-user"></i>
														</span>
                                                    </div>
                                                    <input type="text"
                                                           class="form-control m-input--air"
                                                           placeholder="Account Holder Name"
                                                           name="bank_account_holder_name"
                                                           autocomplete="off"  value="{{$customer->bank_account_holder_name}}"
                                                           aria-describedby="basic-addon1">
                                                </div>



                                            </div>
                                            <div class="col-lg-4">
                                                <label for="exampleInputEmail1">
                                                    Account Number
                                                </label>
                                                <div class="input-group m-input-group">
                                                    <div class="input-group-prepend">
														<span class="input-group-text">
															<i class="la la-calculator"></i>
														</span>
                                                    </div>
                                                    <input type="text"
                                                           class="form-control m-input--air"
                                                           placeholder="Account Number"
                                                           name="bank_account_number"
                                                           autocomplete="off"  value="{{$customer->bank_account_number}}"
                                                           aria-describedby="basic-addon1">
                                                </div>



                                            </div>
                                        </div>







                                    </div>


                                    <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                                        <div class="m-form__actions m-form__actions--solid">
                                            <div class="row">
                                                <div class="col-lg-4"></div>
                                                <div class="col-lg-8">
                                                    <button type="submit" class="btn btn-primary">
                                                        Submit
                                                    </button>
                                                    <button type="reset" class="btn btn-secondary">
                                                        Reset
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                </form>







                            </div>







                            <div class="tab-pane" id="customerfiles" role="tabpanel">


                                @if (count($errors) > 0)
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                @endif



                                    <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed"
                                          role="form" method="post"
                                          enctype="multipart/form-data"
                                          action="{{ route('admin.customer.file.post') }}"
                                    >
                                        {{ csrf_field() }}
                                        <div class="m-portlet__body">







                                            <div class="form-group m-form__group row">


                                                <input type="hidden" value="{{$customer->id}}" name="customer_id">

                                                <div class="col-lg-4">
                                                    <label for="exampleInputEmail1">
                                                        File Name
                                                    </label>
                                                    <div class="input-group m-input-group">
                                                        <div class="input-group-prepend">
														<span class="input-group-text">
															<i class="la la-user"></i>
														</span>
                                                        </div>
                                                        <input type="text"
                                                               class="form-control m-input--air"
                                                               placeholder="File Name"
                                                               name="name"
                                                               required
                                                               value=""
                                                               autocomplete="off"
                                                               aria-describedby="basic-addon1">
                                                    </div>
                                                    <span class="m-form__help">
													File Name
												</span>
                                                </div>
                                                <div class="col-lg-4">
                                                    <label for="exampleInputEmail1">
                                                        File Desscription
                                                    </label>
                                                    <div class="input-group m-input-group">
                                                        <div class="input-group-prepend">
														<span class="input-group-text">
															<i class="la la-user"></i>
														</span>
                                                        </div>
                                                        <input type="text"
                                                               class="form-control m-input--air"
                                                               placeholder="File Desscription"
                                                               name="description"
                                                               value=""
                                                               required
                                                               autocomplete="off"
                                                               aria-describedby="basic-addon1">
                                                    </div>
                                                    <span class="m-form__help">
													File Desscription
												</span>
                                                </div>
                                                <div class="col-lg-4">
                                                    <label for="exampleInputEmail1">
                                                        File Sno
                                                    </label>
                                                    <div class="input-group m-input-group">
                                                        <div class="input-group-prepend">
														<span class="input-group-text">
															<i class="la la-user"></i>
														</span>
                                                        </div>
                                                        <input type="text"
                                                               class="form-control m-input--air"
                                                               placeholder="Sno"
                                                               name="id_number"
                                                               required
                                                               value=""
                                                               autocomplete="off"
                                                               aria-describedby="basic-addon1">
                                                    </div>
                                                    <span class="m-form__help">
													File Sno
												</span>
                                                </div>
                                                <div class="col-lg-12">
                                                    <label for="exampleInputEmail1">
                                                        File
                                                    </label>
                                                    <div class="input-group m-input-group">

                                                        <input type="file"
                                                               class="form-control"
required
                                                               name="image"
                                                               value=""

                                                               >
                                                    </div>

                                                </div>

                                                <br>
                                                <br>

                                                <button class="btn btn-primary btn-block" type="submit">Submit</button>



                                            </div>
                                        </div>

                                    </form>


                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th scope="col">#</th>
                                            <th scope="col">Name</th>
                                            <th scope="col">Description</th>
                                            <th scope="col">ID Number</th>
                                            <th scope="col">preview</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($files as $file)
                                        <tr>
                                            <th scope="row">{{$file->name}}</th>
                                            <td>{{$file->name}}</td>

                                            <td>{{$file->id_number}}</td>
                                            <td>{{$file->description}}</td>
                                            <td><img src="{{$file->location}}" alt="">
                                            


                                            
                                            
                                            </td>

                                        </tr>
                                        @endforeach

                                        </tbody>
                                    </table>









                            </div>
                            <div class="tab-pane" id="treeView" role="tabpanel">







                                        <!--begin: Search Form -->


                                        <div class="row">

                                            <div class="col-4">

                                                <h5>Primary Member</h5>

                                                <button class="btn btn-primary primary-member">

                                                </button>



                                            </div>
                                            <div  class="col-8">

                                                <h5>Down line Member</h5>
                                                <div id="downline"></div>





                                            </div>

                                        </div>








                                    <!--end::Section-->














                            </div>



                            <div class="tab-pane" id="changepassword" role="tabpanel">
                                <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed"
                                      role="form" method="post"
                                      action="{{ route('admin.customer.password.change',$customer->id) }}"
                                >
                                    {{ csrf_field() }}
                                    <div class="m-portlet__body">
                                        <div class="form-group m-form__group row">
                                            <div class="col-lg-4">
                                                <label for="exampleInputEmail1">
                                                    New Password
                                                </label>
                                                <div class="input-group m-input-group">
                                                    <div class="input-group-prepend">
														<span class="input-group-text">
															<i class="la la-user"></i>
														</span>
                                                    </div>
                                                    <input type="password"
                                                           class="form-control m-input--air"
                                                           placeholder="New Password"
                                                           name="password"
                                                           value=""
                                                           autocomplete="off"
                                                           aria-describedby="basic-addon1">


                                                    <input type="hidden" name="_method" value="PUT">


                                                </div>
                                                <span class="m-form__help">
													New Password
												</span>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                                        <div class="m-form__actions m-form__actions--solid">
                                            <div class="row">
                                                <div class="col-lg-4"></div>
                                                <div class="col-lg-8">
                                                    <button type="submit" class="btn btn-primary">
                                                        Submit
                                                    </button>
                                                    <button type="reset" class="btn btn-secondary">
                                                        Reset
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>



                                </form>





                            </div>



                            <div class="tab-pane" id="rank" role="tabpanel">
                                <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed"
                                      role="form" method="post"
                                      action="{{ route('admin.customer.change.rank',$customer->id) }}"
                                >
                                    {{ csrf_field() }}
                                    <div class="m-portlet__body">
                                        <div class="form-group m-form__group row">
                                            <div class="col-lg-4">
                                                <label for="exampleInputEmail1">
                                                    Rank
                                                </label>
                                                <div class="input-group m-input-group">
                                                    <div class="input-group-prepend">
														<span class="input-group-text">
															<i class="la la-arrow-up"></i>
														</span>
                                                    </div>
                                                    <input type="text"
                                                           class="form-control m-input--air"
                                                           placeholder="Rank"
                                                           name="rank"
                                                           required
                                                           value="{{$customer->commission}}"
                                                           autocomplete="off"
                                                           aria-describedby="basic-addon1">


                                                    <input type="hidden" name="_method" value="PUT">


                                                </div>
                                                <span class="m-form__help">
													Rank
												</span>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                                        <div class="m-form__actions m-form__actions--solid">
                                            <div class="row">
                                                <div class="col-lg-4"></div>
                                                <div class="col-lg-8">
                                                    <button type="submit" class="btn btn-primary">
                                                        Submit
                                                    </button>
                                                    <button type="reset" class="btn btn-secondary">
                                                        Reset
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>



                                </form>





                            </div>



                            <div class="tab-pane" id="waller" role="tabpanel">






                                <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed"
                                      role="form" method="post"
                                      action="{{ route('admin.customer.wallet.add.money') }}"
                                >
                                    {{ csrf_field() }}
                                    <div class="m-portlet__body">




                                        <div class="form-group m-form__group row">


                                            <input type="hidden" name="fname" value="{{$customer->fname}}">
                                            <input type="hidden" name="lname" value="{{$customer->lname}}">
                                            <input type="hidden" name="mobile" value="{{$customer->mobile}}">
                                            <input type="hidden" name="email" value="{{$customer->email}}">
                                            <input type="hidden" name="customer_id" value="{{$customer->id}}">
                                            <input type="hidden" name="taxes" value="0">


                                            <div class="col-lg-4">
                                                <label for="exampleInputEmail1">
                                                    Amount
                                                </label>
                                                <div class="input-group m-input-group">
                                                    <div class="input-group-prepend">
														<span class="input-group-text">
															<i class="la la-inr"></i>
														</span>
                                                    </div>
                                                    <input type="text"
                                                           class="form-control m-input--air"
                                                           placeholder="Amount"
                                                           name="amount"   required
                                                           value=""
                                                           autocomplete="off"
                                                           aria-describedby="basic-addon1">
                                                </div>

                                            </div>



                                            <div class="col-lg-4">
                                                <label for="exampleInputEmail1">
                                                    Payment Methods
                                                </label>
                                                <div class="input-group m-input-group">
                                                    <div class="input-group-prepend">
														<span class="input-group-text">
															<i class="la la-cc-diners-club"></i>
														</span>
                                                    </div>
                                                    <input type="text"
                                                           class="form-control m-input--air"
                                                           placeholder="Payment Methods"
                                                           name="methods"   required
                                                           value=""
                                                           autocomplete="off"
                                                           aria-describedby="basic-addon1">
                                                </div>

                                            </div>



                                            <div class="col-lg-4">
                                                <label for="exampleInputEmail1">
                                                    Payment Charges
                                                </label>
                                                <div class="input-group m-input-group">
                                                    <div class="input-group-prepend">
														<span class="input-group-text">
															<i class="la la-tag"></i>
														</span>
                                                    </div>
                                                    <input type="text"
                                                           class="form-control m-input--air"
                                                           placeholder="Payment Charges"
                                                           name="gateway_charges"
                                                           value=""   required
                                                           autocomplete="off"
                                                           aria-describedby="basic-addon1">
                                                </div>

                                            </div>







                                        </div>
                                    </div>


                                    <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                                        <div class="m-form__actions m-form__actions--solid">
                                            <div class="row">
                                                <div class="col-lg-4"></div>
                                                <div class="col-lg-8">
                                                    <button type="submit" class="btn btn-primary">
                                                        Submit
                                                    </button>
                                                    <button type="reset" class="btn btn-secondary">
                                                        Reset
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>



                                </form>

                                <br>
                                <br>


                                <table class="table table-bordered" id="wallet-table">
                                    <thead>
                                    <tr>
                                        <th>TXT ID</th>

                                        <th>Amount</th>
                                        <th>Gateway Charges</th>
                                        <th>Other Charges</th>
                                        <th>Date</th>
                                        <th>Action</th>


                                    </tr>
                                    </thead>
                                </table>



                            </div>
                            <div class="tab-pane" id="wallerbv" role="tabpanel">






                                <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed"
                                      role="form" method="post"
                                      action="{{ route('admin.customer.bvwallet.add.money') }}"
                                >
                                    {{ csrf_field() }}
                                    <div class="m-portlet__body">




                                        <div class="form-group m-form__group row">


                                            <input type="hidden" name="fname" value="{{$customer->fname}}">
                                            <input type="hidden" name="lname" value="{{$customer->lname}}">
                                            <input type="hidden" name="mobile" value="{{$customer->mobile}}">
                                            <input type="hidden" name="email" value="{{$customer->email}}">
                                            <input type="hidden" name="customer_id" value="{{$customer->id}}">
                                            <input type="hidden" name="taxes" value="0">


                                            <div class="col-lg-3">
                                                <label for="exampleInputEmail1">
                                                    Amount
                                                </label>
                                                <div class="input-group m-input-group">
                                                    <div class="input-group-prepend">
														<span class="input-group-text">
															<i class="la la-inr"></i>
														</span>
                                                    </div>
                                                    <input type="text"
                                                           class="form-control m-input--air"
                                                           placeholder="Amount"
                                                           name="amount"
                                                           value=""
                                                           required
                                                           autocomplete="off"
                                                           aria-describedby="basic-addon1">
                                                </div>

                                            </div>


                                            <div class="col-lg-3">
                                                <label for="exampleInputEmail1">
                                                    Business Volume
                                                </label>
                                                <div class="input-group m-input-group">
                                                    <div class="input-group-prepend">

                                                    </div>
                                                    <input type="text"
                                                           class="form-control m-input--air"
                                                           placeholder="Business Volume"
                                                           name="bv"   required
                                                           autocomplete="off"
                                                           aria-describedby="basic-addon1">
                                                </div>

                                            </div>



                                            <div class="col-lg-3">
                                                <label for="exampleInputEmail1">
                                                    Payment Methods
                                                </label>
                                                <div class="input-group m-input-group">
                                                    <div class="input-group-prepend">
														<span class="input-group-text">
															<i class="la la-cc-diners-club"></i>
														</span>
                                                    </div>
                                                    <input type="text"
                                                           class="form-control m-input--air"
                                                           placeholder="Payment Methods"
                                                           name="methods"
                                                           value=""   required
                                                           autocomplete="off"
                                                           aria-describedby="basic-addon1">
                                                </div>

                                            </div>



                                            <div class="col-lg-3">
                                                <label for="exampleInputEmail1">
                                                    Payment Charges
                                                </label>
                                                <div class="input-group m-input-group">
                                                    <div class="input-group-prepend">
														<span class="input-group-text">
															<i class="la la-tag"></i>
														</span>
                                                    </div>
                                                    <input type="text"
                                                           class="form-control m-input--air"
                                                           placeholder="Payment Charges"
                                                           name="gateway_charges"
                                                           value=""   required
                                                           autocomplete="off"
                                                           aria-describedby="basic-addon1">
                                                </div>

                                            </div>







                                        </div>
                                    </div>


                                    <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                                        <div class="m-form__actions m-form__actions--solid">
                                            <div class="row">
                                                <div class="col-lg-4"></div>
                                                <div class="col-lg-8">
                                                    <button type="submit" class="btn btn-primary">
                                                        Submit
                                                    </button>
                                                    <button type="reset" class="btn btn-secondary">
                                                        Reset
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>



                                </form>

                                <br>
                                <br>


                                <table class="table table-bordered" id="walletbv-table">
                                    <thead>
                                    <tr>
                                        <th>TXT ID</th>

                                        <th>Amount</th>
                                        <th>Business Volume</th>
                                        <th>Gateway Charges</th>
                                        <th>Other Charges</th>
                                        <th>Date</th>
                                        <th>Action</th>


                                    </tr>
                                    </thead>
                                </table>



                            </div>
















                        </div>
                    </div>
                </div>


            </div>








        </div>
    </div>



 @endsection

@section('admin_footer_script')


    <script>
        //== Class definition

        var BootstrapDatepicker = function () {

            //== Private functions
            var demos = function () {
                // minimum setup
                $('#m_datepicker_1, #m_datepicker_1_validate').datepicker({
                    todayHighlight: true,
                    orientation: "bottom left",
                    templates: {
                        leftArrow: '<i class="la la-angle-left"></i>',
                        rightArrow: '<i class="la la-angle-right"></i>'
                    }
                });
            }

            return {
                // public functions
                init: function() {
                    demos();
                }
            };
        }();

        jQuery(document).ready(function() {
            BootstrapDatepicker.init();
        });
    </script>
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
    <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
    <script>
        $(function() {

            $('#report-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: '/customer/order/datatable/{{$customer->id}}',
                columns: [
                    { data: 'invoice_number', name: 'invoice_number' },
                    { data: 'invoice_date', name: 'invoice_date' },
                    { data: 'invoice_total_tax', name: 'invoice_total_tax' },
                    { data: 'tax_val', name: 'tax_val' },
                    { data: 'invoice_total', name: 'invoice_total' },
                    { data: 'invoice_return_quater', name: 'invoice_return_quater' },
                    { data: 'invoice_return_month', name: 'invoice_return_month' },
                    { data: 'created_at', name: 'created_at' },
                    { data: 'action', name: 'action' },
                ]
            });

            $('#wallet-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{{route('admin.customer.wallet.data.table',$customer->id)}}',
                columns: [
                    { data: 'txt_id', name: 'txt_id' },
                    { data: 'amount', name: 'amount' },
                    { data: 'gateway_charges', name: 'gateway_charges' },
                    { data: 'charges', name: 'charges' },
                    { data: 'created_at', name: 'created_at' },
                    { data: 'action', name: 'action' }


                ]
            });

            $('#walletbv-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{{route('admin.customer.bvwallet.data.table',$customer->id)}}',
                columns: [
                    { data: 'txt_id', name: 'txt_id' },
                    { data: 'amount', name: 'amount' },
                    { data: 'bv_value', name: 'bv_value' },
                    { data: 'gateway_charges', name: 'gateway_charges' },
                    { data: 'charges', name: 'charges' },
                    { data: 'created_at', name: 'created_at' },
                    { data: 'action', name: 'action' }


                ]
            });
        });
    </script>



    <style>
        .treeviewbutton{
            margin-top: 5px;
        }
    </style>


    <script>






        var downline = [];

        $.ajax({
            /* The whisperingforest.org URL is not longer valid, I found a new one that is similar... */
            url:"{{route('admin.tree.view.by.id.downline',$customer->id)}}",
            //  async: true,
            dataType: 'json',

            success: function(obj){
                // var json = $.parseJSON(obj);

                $(".primary-member").replaceWith('<i style="font-size: 33px; color: #e12500!important; text-align: center; padding-left: 75px" class="fa fa-user"></i> <br> ' +
                    '<button  class="btn btn-danger primary-member">'+obj.customer.fname+'<br> Sponcer Id: '+obj.customer.sponser_id+'<br> IC NO: '+obj.customer.ic_number+'</button> ')



                $.each(obj.downline,function(k,v){
                    $("#downline").append('<i style="font-size: 33px; color: #5de178!important; text-align: center; padding-left: 125px" class="fa fa-user"></i> <br>' +
                        '<a href="/dashboard/admin/tree-view/view/'+v.id+'"  class="btn btn-success treeviewbutton">'+v.fname+ ' '+v.lname +
                        ' <br> IC No:  '+v.ic_number+
                        ' <br> Sponcer ID:  '+v.sponser_id+'</a> ' +
                        '<br>')


                    console.log(v.fname);
                });
            },
            error: function(error){
                alert(error);
            }
        })








    </script>






@endsection