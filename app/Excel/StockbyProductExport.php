<?php
/**
 * Created by PhpStorm.
 * User: kobid
 * Date: 13/04/18
 * Time: 5:44 AM
 */




namespace App\Excel;




use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Illuminate\Support\Collection;

class StockbyProductExport implements FromQuery, WithHeadings
{
    use Exportable;




    public function headings(): array
    {
        return [
            'ID',
            'NAME',
            'CODE NAME',
            'COLOR',
            'HSN Code',
            'MRP'


        ];
    }






    public function query()
    {

        $users = \App\ProductVatient::all();

        $collect = collect($users);


        $lengths = $collect->map(function ($name, $key) {
            return [
                'id' => $name->id,
                'name' => $name->name,
                'color' => $name->color,
                'size' => $name->size,
                'stockout' => $name->Productsale()->sum('quantity'),
                'stockin' => $name->stock()->sum('amount')
            ];
        });

        $lengths->toArray();



       return $users;

      //  return CreateInvoice::query()->whereYear('created_at', $this->year);
    }
}