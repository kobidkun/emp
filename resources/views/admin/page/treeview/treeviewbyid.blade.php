@extends('admin.index');

@section('content')
    <link rel="stylesheet" href="{{asset('/js/vis/dist/vis.min.css') }}" />
    <div class="m-content">
        <div class="row">
            <div class="col-md-12">


                            <div class="m-portlet m-portlet--mobile">
                                <div class="m-portlet__head">
                                    <div class="m-portlet__head-caption">
                                        <div class="m-portlet__head-title">
                                            <h3 class="m-portlet__head-text">
                                                Tree View
                                            </h3>
                                        </div>
                                    </div>
                                    <div class="m-portlet__head-tools">

                                    </div>
                                </div>
                                <div class="m-portlet__body">
                                    <!--begin: Search Form -->


                                    <div class="row">

                                        <div class="col-12" style="text-align: center">

                                            <h5>Primary Member</h5>

                                            <button class="btn btn-primary primary-member">

                                            </button>



                                        </div>



                                        <hr>


                                        {{--downlines--}}

                                        <div  class="col-12" id="downlinecontainer" style="text-align: center">
                                            <br>
                                            <br>
                                            <div class="m-stack m-stack--ver m-stack--general  m-stack--demo" id="downline">

                                            </div>
                                        </div>
                                        {{--downlines--}}


                                        <hr>

                                        {{--downlines--}}

                                        <div  class="col-12" id="downlinecontainer2" style="text-align: center">
                                            <br>
                                            <div style="font-weight: 500;font-size: 24px" id="memup2"></div>
                                            <br>
                                            <div class="m-stack m-stack--ver m-stack--general  m-stack--demo" id="downline2">



                                            </div>
                                        </div>
                                        {{--downlines--}}

                                        <hr>


                                        {{--downlines--}}

                                        <div  class="col-12" id="downlinecontainer3" style="text-align: center">
                                            <br>
                                            <div style="font-weight: 500;font-size: 24px" id="memup3"></div>
                                            <br>
                                            <div class="m-stack m-stack--ver m-stack--general  m-stack--demo" id="downline3">

                                            </div>
                                        </div>
                                        {{--downlines--}}

                                        <hr>

                                        {{--downlines--}}

                                        <div  class="col-12" id="downlinecontainer4" style="text-align: center">
                                            <br>
                                            <div style="font-weight: 500;font-size: 24px" id="memup4"></div>
                                            <br>
                                            <div class="m-stack m-stack--ver m-stack--general  m-stack--demo" id="downline4">

                                            </div>
                                        </div>
                                        {{--downlines--}}/
                                        <hr>
                                        {{--downlines--}}

                                        <div  class="col-12" id="downlinecontainer5" style="text-align: center">
                                            <br>
                                            <div style="font-weight: 500;font-size: 24px" id="memup5"></div>
                                            <br>
                                            <div class="m-stack m-stack--ver m-stack--general  m-stack--demo" id="downline5">

                                            </div>
                                        </div>
                                        {{--downlines--}}

                                        <hr>

                                        {{--downlines--}}

                                        <div  class="col-12" id="downlinecontainer6" style="text-align: center">
                                            <br>
                                            <div style="font-weight: 500;font-size: 24px" id="memup6"></div>
                                            <br>
                                            <div class="m-stack m-stack--ver m-stack--general  m-stack--demo" id="downline6">

                                            </div>
                                        </div>
                                        {{--downlines--}}


                                        {{--downlines--}}

                                        <div  class="col-12" id="downlinecontainer7" style="text-align: center">
                                            <br>
                                            <div style="font-weight: 500;font-size: 24px" id="memup7"></div>
                                            <br>
                                            <div class="m-stack m-stack--ver m-stack--general  m-stack--demo" id="downline7">

                                            </div>
                                        </div>
                                        {{--downlines--}}


                                        {{--downlines--}}

                                        <div  class="col-12" id="downlinecontainer8" style="text-align: center">
                                            <br>
                                            <div style="font-weight: 500;font-size: 24px" id="memup8"></div>
                                            <br>
                                            <div class="m-stack m-stack--ver m-stack--general  m-stack--demo" id="downline8">

                                            </div>
                                        </div>
                                        {{--downlines--}}



                                        {{--downlines--}}

                                        <div  class="col-12" id="downlinecontainer9" style="text-align: center">
                                            <br>
                                            <div style="font-weight: 500;font-size: 24px" id="memup9"></div>
                                            <br>
                                            <div class="m-stack m-stack--ver m-stack--general  m-stack--demo" id="downline9">

                                            </div>
                                        </div>
                                        {{--downlines--}}



                                        {{--downlines--}}

                                        <div  class="col-12" id="downlinecontainer10" style="text-align: center">
                                            <br>
                                            <div style="font-weight: 500;font-size: 24px" id="memup10"></div>
                                            <br>
                                            <div class="m-stack m-stack--ver m-stack--general  m-stack--demo" id="downline10">

                                            </div>
                                        </div>
                                        {{--downlines--}}








                                    </div>







                        </div>
                        <!--end::Section-->

                    </div>


            </div>
        </div>
    </div>

 @endsection


@section('admin_footer_script')

    <style>
        .treeviewbutton{
            margin-top: 5px;
        }
    </style>

    <script>






     var downline = [];

     $.ajax({
         /* The whisperingforest.org URL is not longer valid, I found a new one that is similar... */
         url:"{{route('admin.tree.view.by.id.downline',$id)}}",
       //  async: true,
         dataType: 'json',

         success: function(obj){
             // var json = $.parseJSON(obj);

             $(".primary-member").replaceWith('<i style="font-size: 128px; color: #e12500!important; text-align: center; align-items: center; padding: auto" class="fa fa-user"></i> <br> ' +
                 '<button  class="btn btn-danger primary-member">'+obj.customer.fname+'<br> Sponcer Id: '+obj.customer.sponser_id+'<br> IC NO: '+obj.customer.ic_number+'</button> ')



             $.each(obj.downline,function(k,v){
                 $("#downline").append(
                     '<div class="m-stack__item fdownline">' +
                     '<i style="font-size: 64px; color: #5de178!important; text-align: center; align-items: center; padding: auto" class="fa fa-user"></i> <br>' +
                     '<button  value="'+v.id+'" class="btn btn-success treeviewbutton dwnlinemembers">'+v.fname+ ' '+v.lname +
                     ' <br> IC No:  '+v.ic_number+
                     ' <br> Sponcer ID:  '+v.sponser_id+'</button> <br><br>' +
                     '<a href="/dashboard/admin/tree-view/view/'+v.id+'"  class="btn btn-outline-accent" target="_blank"> View Details</a>'+



                     '</div>'


                 )
                 console.log(v.fname);
             });
         },
         error: function(error){
             alert(error);
         }
     });


     $(document).on("click", ".dwnlinemembers" , function(){

         $("#downline2").html('');
         $("#memup2").html('');


         $("#downline3").html('');
         $("#memup3").html('');

         $("#downline4").html('');
         $("#memup4").html('');

         $("#downline5").html('');
         $("#memup5").html('');

         $("#downline6").html('');
         $("#memup6").html('');

         $("#downline7").html('');
         $("#memup7").html('');


         $("#downline8").html('');
         $("#memup8").html('');


         $("#downline9").html('');
         $("#memup9").html('');


         $("#downline10").html('');
         $("#memup10").html('');



         var USERID = $(this).val()
         alert ( USERID);



         var downline = [];

         $.ajax({
             /* The whisperingforest.org URL is not longer valid, I found a new one that is similar... */
             url:'/dashboard/admin/tree-view/by-id/down-line/'+USERID,
             //  async: true,
             dataType: 'json',

             success: function(obj){
                 // var json = $.parseJSON(obj);

                 $("#memup2").html(obj.customer.fname +' ' + obj.customer.lname + '<br> IC:' + obj.customer.ic_number

                 )



                 $.each(obj.downline,function(k,v){



                     $("#downline2").append(





                         '<div class="m-stack__item fdownline">' +
                         '<i style="font-size: 64px; color: #6a9fff!important; text-align: center; align-items: center; padding: auto" class="fa fa-user"></i> <br>' +
                         '<button  value="'+v.id+'" style="background-color: 6a9fff" class="btn  treeviewbutton dwnlinemembers3">'+v.fname+ ' '+v.lname +
                         ' <br> IC No:  '+v.ic_number+
                         ' <br> Sponcer ID:  '+v.sponser_id+'</button> <br><br>' +
                         '<a href="/dashboard/admin/tree-view/view/'+v.id+'"  class="btn btn-outline-accent" target="_blank"> View Details</a>'+


                         '</div>'






                     );


                     console.log(v.fname);
                 });
             },
             error: function(error){
                 alert(error);
             }
         })




     });

     // next

     $(document).on("click", ".dwnlinemembers3" , function(){




         $("#downline3").html('');
         $("#memup3").html('');

         $("#downline4").html('');
         $("#memup4").html('');

         $("#downline5").html('');
         $("#memup5").html('');

         $("#downline6").html('');
         $("#memup6").html('');

         $("#downline7").html('');
         $("#memup7").html('');


         $("#downline8").html('');
         $("#memup8").html('');


         $("#downline9").html('');
         $("#memup9").html('');


         $("#downline10").html('');
         $("#memup10").html('');

         var USERID = $(this).val()
         alert ( USERID);



         var downline = [];

         $.ajax({
             /* The whisperingforest.org URL is not longer valid, I found a new one that is similar... */
             url:'/dashboard/admin/tree-view/by-id/down-line/'+USERID,
             //  async: true,
             dataType: 'json',

             success: function(obj){
                 // var json = $.parseJSON(obj);
                 $("#memup3").html(obj.customer.fname +' ' + obj.customer.lname + '<br> IC:' + obj.customer.ic_number)



                 $.each(obj.downline,function(k,v){



                     $("#downline3").append(





                         '<div class="m-stack__item fdownline">' +
                         '<i style="font-size: 64px; color: #ffc30a!important; text-align: center; align-items: center; padding: auto" class="fa fa-user"></i> <br>' +
                         '<button  value="'+v.id+'" style="background-color: #ffc30a" class="btn  treeviewbutton dwnlinemembers4">'+v.fname+ ' '+v.lname +
                         ' <br> IC No:  '+v.ic_number+
                         ' <br> Sponcer ID:  '+v.sponser_id+'</button> <br><br>' +
                         '<a href="/dashboard/admin/tree-view/view/'+v.id+'"  class="btn btn-outline-accent" target="_blank"> View Details</a>'+

                         '</div>'






                     );


                     console.log(v.fname);
                 });
             },
             error: function(error){
                 alert(error);
             }
         })




     });


     $(document).on("click", ".dwnlinemembers4" , function(){



         $("#downline4").html('');
         $("#memup4").html('');

         $("#downline5").html('');
         $("#memup5").html('');

         $("#downline6").html('');
         $("#memup6").html('');

         $("#downline7").html('');
         $("#memup7").html('');


         $("#downline8").html('');
         $("#memup8").html('');


         $("#downline9").html('');
         $("#memup9").html('');


         $("#downline10").html('');
         $("#memup10").html('');

         var USERID = $(this).val()
         alert ( USERID);



         var downline = [];

         $.ajax({
             /* The whisperingforest.org URL is not longer valid, I found a new one that is similar... */
             url:'/dashboard/admin/tree-view/by-id/down-line/'+USERID,
             //  async: true,
             dataType: 'json',

             success: function(obj){
                 // var json = $.parseJSON(obj);
                 $("#memup4").html(obj.customer.fname +' ' + obj.customer.lname + '<br> IC:' + obj.customer.ic_number)



                 $.each(obj.downline,function(k,v){



                     $("#downline4").append(





                         '<div class="m-stack__item fdownline">' +
                         '<i style="font-size: 64px; color: #cf1fe1!important; text-align: center; align-items: center; padding: auto" class="fa fa-user"></i> <br>' +
                         '<button  value="'+v.id+'" style="background-color: #cf1fe1" class="btn  treeviewbutton dwnlinemembers5">'+v.fname+ ' '+v.lname +
                         ' <br> IC No:  '+v.ic_number+
                         ' <br> Sponcer ID:  '+v.sponser_id+'</button> <br><br>' +
                         '<a href="/dashboard/admin/tree-view/view/'+v.id+'"  class="btn btn-outline-accent" target="_blank"> View Details</a>'+

                         '</div>'






                     );


                     console.log(v.fname);
                 });
             },
             error: function(error){
                 alert(error);
             }
         })




     });

     // five

     $(document).on("click", ".dwnlinemembers5" , function(){



         $("#downline5").html('');
         $("#memup5").html('');

         $("#downline6").html('');
         $("#memup6").html('');

         $("#downline7").html('');
         $("#memup7").html('');


         $("#downline8").html('');
         $("#memup8").html('');


         $("#downline9").html('');
         $("#memup9").html('');


         $("#downline10").html('');
         $("#memup10").html('');

         var USERID = $(this).val()
         alert ( USERID);



         var downline = [];

         $.ajax({
             /* The whisperingforest.org URL is not longer valid, I found a new one that is similar... */
             url:'/dashboard/admin/tree-view/by-id/down-line/'+USERID,
             //  async: true,
             dataType: 'json',

             success: function(obj){
                 // var json = $.parseJSON(obj);
                 $("#memup5").html(obj.customer.fname +' ' + obj.customer.lname + '<br> IC:' + obj.customer.ic_number)



                 $.each(obj.downline,function(k,v){



                     $("#downline5").append(





                         '<div class="m-stack__item fdownline">' +
                         '<i style="font-size: 64px; color: #b7b7b7!important; text-align: center; align-items: center; padding: auto" class="fa fa-user"></i> <br>' +
                         '<button  value="'+v.id+'" style="background-color: #b7b7b7" class="btn  treeviewbutton dwnlinemembers6">'+v.fname+ ' '+v.lname +
                         ' <br> IC No:  '+v.ic_number+
                         ' <br> Sponcer ID:  '+v.sponser_id+'</button> <br><br>' +
                         '<a href="/dashboard/admin/tree-view/view/'+v.id+'"  class="btn btn-outline-accent" target="_blank"> View Details</a>'+

                         '</div>'






                     );


                     console.log(v.fname);
                 });
             },
             error: function(error){
                 alert(error);
             }
         })




     });


     $(document).on("click", ".dwnlinemembers6" , function(){



         $("#downline6").html('');
         $("#memup6").html('');

         $("#downline7").html('');
         $("#memup7").html('');


         $("#downline8").html('');
         $("#memup8").html('');


         $("#downline9").html('');
         $("#memup9").html('');


         $("#downline10").html('');
         $("#memup10").html('');


         var USERID = $(this).val()
         alert ( USERID);



         var downline = [];

         $.ajax({
             /* The whisperingforest.org URL is not longer valid, I found a new one that is similar... */
             url:'/dashboard/admin/tree-view/by-id/down-line/'+USERID,
             //  async: true,
             dataType: 'json',

             success: function(obj){
                 // var json = $.parseJSON(obj);
                 $("#memup6").html(obj.customer.fname +' ' + obj.customer.lname + '<br> IC:' + obj.customer.ic_number)



                 $.each(obj.downline,function(k,v){



                     $("#downline6").append(





                         '<div class="m-stack__item fdownline">' +
                         '<i style="font-size: 64px; color: #9affe2!important; text-align: center; align-items: center; padding: auto" class="fa fa-user"></i> <br>' +
                         '<button  value="'+v.id+'" style="background-color: #9affe2" class="btn  treeviewbutton dwnlinemembers7">'+v.fname+ ' '+v.lname +
                         ' <br> IC No:  '+v.ic_number+
                         ' <br> Sponcer ID:  '+v.sponser_id+'</button> <br><br>' +
                         '<a href="/dashboard/admin/tree-view/view/'+v.id+'"  class="btn btn-outline-accent" target="_blank"> View Details</a>'+

                         '</div>'






                     );


                     console.log(v.fname);
                 });
             },
             error: function(error){
                 alert(error);
             }
         })




     });


     $(document).on("click", ".dwnlinemembers7" , function(){



         $("#downline7").html('');
         $("#memup7").html('');


         $("#downline8").html('');
         $("#memup8").html('');


         $("#downline9").html('');
         $("#memup9").html('');


         $("#downline10").html('');
         $("#memup10").html('');


         var USERID = $(this).val()
         alert ( USERID);



         var downline = [];

         $.ajax({
             /* The whisperingforest.org URL is not longer valid, I found a new one that is similar... */
             url:'/dashboard/admin/tree-view/by-id/down-line/'+USERID,
             //  async: true,
             dataType: 'json',

             success: function(obj){
                 // var json = $.parseJSON(obj);
                 $("#memup7").html(obj.customer.fname +' ' + obj.customer.lname + '<br> IC:' + obj.customer.ic_number)



                 $.each(obj.downline,function(k,v){



                     $("#downline7").append(





                         '<div class="m-stack__item fdownline">' +
                         '<i style="font-size: 64px; color: #59ff00!important; text-align: center; align-items: center; padding: auto" class="fa fa-user"></i> <br>' +
                         '<button  value="'+v.id+'" style="background-color: #59ff00" class="btn  treeviewbutton dwnlinemembers8">'+v.fname+ ' '+v.lname +
                         ' <br> IC No:  '+v.ic_number+
                         ' <br> Sponcer ID:  '+v.sponser_id+'</button> <br><br>' +
                         '<a href="/dashboard/admin/tree-view/view/'+v.id+'"  class="btn btn-outline-accent" target="_blank"> View Details</a>'+

                         '</div>'






                     );


                     console.log(v.fname);
                 });
             },
             error: function(error){
                 alert(error);
             }
         })




     });

     $(document).on("click", ".dwnlinemembers8" , function(){




         $("#downline8").html('');
         $("#memup8").html('');


         $("#downline9").html('');
         $("#memup9").html('');


         $("#downline10").html('');
         $("#memup10").html('');


         var USERID = $(this).val()
         alert ( USERID);



         var downline = [];

         $.ajax({
             /* The whisperingforest.org URL is not longer valid, I found a new one that is similar... */
             url:'/dashboard/admin/tree-view/by-id/down-line/'+USERID,
             //  async: true,
             dataType: 'json',

             success: function(obj){
                 // var json = $.parseJSON(obj);
                 $("#memup8").html(obj.customer.fname +' ' + obj.customer.lname + '<br> IC:' + obj.customer.ic_number)



                 $.each(obj.downline,function(k,v){



                     $("#downline8").append(





                         '<div class="m-stack__item fdownline">' +
                         '<i style="font-size: 64px; color: #ff4c00!important; text-align: center; align-items: center; padding: auto" class="fa fa-user"></i> <br>' +
                         '<button  value="'+v.id+'" style="background-color: #ff4c00" class="btn  treeviewbutton dwnlinemembers9">'+v.fname+ ' '+v.lname +
                         ' <br> IC No:  '+v.ic_number+
                         ' <br> Sponcer ID:  '+v.sponser_id+'</button> <br><br>' +
                         '<a href="/dashboard/admin/tree-view/view/'+v.id+'"  class="btn btn-outline-accent" target="_blank"> View Details</a>'+

                         '</div>'






                     );


                     console.log(v.fname);
                 });
             },
             error: function(error){
                 alert(error);
             }
         })




     });

     $(document).on("click", ".dwnlinemembers9" , function(){



         $("#downline9").html('');
         $("#memup9").html('');


         $("#downline10").html('');
         $("#memup10").html('');

         var USERID = $(this).val()
         alert ( USERID);



         var downline = [];

         $.ajax({
             /* The whisperingforest.org URL is not longer valid, I found a new one that is similar... */
             url:'/dashboard/admin/tree-view/by-id/down-line/'+USERID,
             //  async: true,
             dataType: 'json',

             success: function(obj){
                 // var json = $.parseJSON(obj);
                 $("#memup9").html(obj.customer.fname +' ' + obj.customer.lname + '<br> IC:' + obj.customer.ic_number)



                 $.each(obj.downline,function(k,v){



                     $("#downline9").append(





                         '<div class="m-stack__item fdownline">' +
                         '<i style="font-size: 64px; color: #0092ff!important; text-align: center; align-items: center; padding: auto" class="fa fa-user"></i> <br>' +
                         '<button  value="'+v.id+'" style="background-color: #0092ff" class="btn  treeviewbutton dwnlinemembers10">'+v.fname+ ' '+v.lname +
                         ' <br> IC No:  '+v.ic_number+
                         ' <br> Sponcer ID:  '+v.sponser_id+'</button> <br><br>' +
                         ' <br> Rank:  '+v.commission+'</button> <br><br>' +
                         '<a href="/dashboard/admin/tree-view/view/'+v.id+'"  class="btn btn-outline-accent" target="_blank"> View Details</a>'+

                         '</div>'






                     );


                     console.log(v.fname);
                 });
             },
             error: function(error){
                 alert(error);
             }
         })




     });


     $(document).on("click", ".dwnlinemembers10" , function(){



         $("#downline10").html('');
         $("#memup10").html('');


         var USERID = $(this).val()
         alert ( USERID);



         var downline = [];

         $.ajax({
             /* The whisperingforest.org URL is not longer valid, I found a new one that is similar... */
             url:'/dashboard/admin/tree-view/by-id/down-line/'+USERID,
             //  async: true,
             dataType: 'json',

             success: function(obj){
                 // var json = $.parseJSON(obj);
                 $("#memup10").html(obj.customer.fname +' ' + obj.customer.lname + '<br> IC:' + obj.customer.ic_number)



                 $.each(obj.downline,function(k,v){



                     $("#downline10").append(





                         '<div class="m-stack__item fdownline">' +
                         '<i style="font-size: 64px; color: #ff0085!important; text-align: center; align-items: center; padding: auto" class="fa fa-user"></i> <br>' +
                         '<button  value="'+v.id+'" style="background-color: #ff0085" class="btn  treeviewbutton dwnlinemembers11">'+v.fname+ ' '+v.lname +
                         ' <br> IC No:  '+v.ic_number+
                         ' <br> Sponcer ID:  '+v.sponser_id+'</button> <br><br>' +
                         '<a href="/dashboard/admin/tree-view/view/'+v.id+'"  class="btn btn-outline-accent" target="_blank"> View Details</a>'+

                         '</div>'






                     );


                     console.log(v.fname);
                 });
             },
             error: function(error){
                 alert(error);
             }
         })




     });








    </script>









    @endsection