<?php

namespace App\Customer;

use Illuminate\Database\Eloquent\Model;

class CustomerCartItems extends Model
{
    protected $hidden = [
        'updated_at',
        'created_at',
    ];

    public function product()
    {
        return $this->hasone('App\ProductVatient','id','product_id');
    }

    public function customer()
    {
        return $this->belongsTo('App\Customer');
    }


    public function images()
    {
        return $this->hasMany('App\ProductVatienttoImages','product_id','product_variant_id');
    }

    public function image()
    {
        return $this->hasOne('App\ProductImage','product_id','product_variant_id');
    }




}
