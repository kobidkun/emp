<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductToAtributesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_to_atributes', function (Blueprint $table) {
            $table->increments('id');
            $table->text('name')->nullable();
            $table->text('type')->nullable();
            $table->text('value')->nullable();
            $table->text('is_filterable')->nullable();
            $table->text('is_searchable')->nullable();
            $table->text('description')->nullable();
            $table->string('product_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_to_atributes');
    }
}
