@extends('admin.index');

@section('content')

    <div class="m-content">
        <div class="row">
            <div class="col-md-12">


                            <div class="m-portlet m-portlet--mobile">

                                <div class="m-portlet__body">
                                    <!--begin: Search Form -->


                                    <style>
                                        .table > tbody > tr > td {
                                            vertical-align: middle;
                                        }


                                        .dataTables_wrapper .dataTables_paginate .paginate_button:hover {
                                            /* color: white !important; */
                                            /* border: 1px solid #111; */
                                            background: #ffffff !important;
                                            border-color: #5867dd !important;

                                        }


                                        .table .thead-dark th {
                                            color: #fff;
                                            background-color: #5867dd!important;
                                            border-color: #32383e;
                                        }





                                    </style>



                                    <table class="table table-striped table-bordered" id="product-table" style="width: 100%">
                                        <thead class="thead-dark">
                                        <tr>
                                            <th>Product Name</th>
                                            <th>CODE</th>
                                            <th>MRP</th>
                                            <th>IGST %</th>
                                            <th>PRICE</th>
                                            <th>STOCK</th>
                                            <th>Variant</th>
                                            <th>ACTION</th>


                                            @if(Auth::guard('admin')->check())


                                                <th>Delete</th>

                                            @elseif(Auth::guard('storeuser')->check())



                                            @elseif(Auth::guard('cashier')->check())



                                            @endif
                                            @guest()


                                            @endguest




                                        </tr>
                                        </thead>
                                    </table>







                        </div>
                        <!--end::Section-->

                    </div>


            </div>
        </div>
    </div>

 @endsection


@section('admin_footer_script')

    <link rel="stylesheet" type="text/css" href="{{asset('/datatable/datatables.css')}}"/>

    <script type="text/javascript" src="{{asset('/datatable/datatables.min.js')}}"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js"></script>

    <script>
        $(function() {
            $('#product-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{{route('admin.product.alldatatable')}}',
                columns: [
                    { data: 'name', name: 'name' },
                    { data: 'code_name', name: 'code_name' },
                    { data: 'mrp_price', name: 'mrp_price' },
                    { data: 'igst_percentage', name: 'igst_percentage' },
                    { data: 'listing_price', name: 'listing_price' },
                    { data: 'stock', name: 'stock' },
                    { data: 'product_to_varients', name: 'product_to_varients' },
                    {data: 'action', name: 'action', orderable: false, searchable: false},


                @if(Auth::guard('admin')->check())

                    {data: 'delete', name: 'delete', orderable: false, searchable: false}


                @elseif(Auth::guard('storeuser')->check())



                @elseif(Auth::guard('cashier')->check())



                @endif

                    @guest()


                    @endguest



                ]
            });

        });
    </script>





    @endsection