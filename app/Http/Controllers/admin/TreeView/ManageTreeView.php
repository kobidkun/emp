<?php

namespace App\Http\Controllers\admin\TreeView;

use App\Customer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;


class ManageTreeView extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth:admin,storeuser,cashier']);


    }

    public function ViewTreeView(){

        return view('admin.page.treeview.treeview');




    }


    public function ViewTreeViewbyid($id){

        return view('admin.page.treeview.treeviewbyid')->with([
            'id' => $id
        ]);




    }
    public function GetAllDownloneByid($id){



        $findReqUserid = Customer::findorfail($id);

      //  dd($findReqUserid);



        $findReqUser = Customer::findorfail($findReqUserid->id);

        $getCustomerToCustomer = $findReqUser->customer_to_customers->direct_down_line_customer_id;



       $convertuplinetoarray = (explode(',',$getCustomerToCustomer));

      $impode = $str = implode (", ", $convertuplinetoarray);


        $splitted = explode(",", $getCustomerToCustomer);
      $z =  json_encode($splitted);



        $y = json_decode($z);



        $users = DB::table('customers')
            ->whereIn('id', $y)
            ->get();



        return response()->json(

           ['customer' => $findReqUser,
             'downline' =>   $users
]



        );
















    }
    public function GetAllDownlone(){



        $findReqUserid = Customer::find(479);

       // dd($findReqUserid);



        $findReqUser = Customer::findorfail($findReqUserid->id);

        $getCustomerToCustomer = $findReqUser->customer_to_customers->direct_down_line_customer_id;



       $convertuplinetoarray = (explode(',',$getCustomerToCustomer));

      $impode = $str = implode (", ", $convertuplinetoarray);


        $splitted = explode(",", $getCustomerToCustomer);
      $z =  json_encode($splitted);



        $y = json_decode($z);



        $users = DB::table('customers')
            ->whereIn('id', $y)
            ->get();



        return response()->json(

           ['customer' => $findReqUser,
             'downline' =>   $users
]



        );
















    }
}
