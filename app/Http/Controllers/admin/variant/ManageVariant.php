<?php

namespace App\Http\Controllers\admin\variant;

use App\VariantType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\DataTables;
use Carbon\Carbon;
class ManageVariant extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth:admin,storeuser,cashier']);


    }

    public function allVariant(){
        return view('admin.page.variant.all');
    }


    public function createVariantindex(){
        return view('admin.page.variant.create');
    }

    public function delete(Request $request, $id){
        $a = VariantType::findorfail($id);
        $a->delete();
        return back();

    }

    public function SaveVariant(Request $request){
        $a = new VariantType();
        $a->name = $request->name;
        $a->value = $request->value;

        $a->save();
        return back();
    }

    public function getBasicData(Request $request)
    {


        $users = VariantType::select(['id','name','value','created_at']);



        return Datatables::of($users)
            ->editColumn('created_at', function($user) {
                return Carbon::createFromFormat('Y-m-d H:i:s', $user->created_at)->toDayDateTimeString();
            })
            ->addColumn('action', function ($user) {
                return '<a href="/admin/variant/delete/'.$user->id.'" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i> Delete</a>';
            })
            ->rawColumns(['action'])
            ->make();
    }

    public function getoptions($name){
        $a = VariantType::where('name', $name)->get();
        return response()->json($a);
    }
}
