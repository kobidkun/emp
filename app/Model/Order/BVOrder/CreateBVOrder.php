<?php

namespace App\Model\Order\BVOrder;

use Illuminate\Database\Eloquent\Model;

class CreateBVOrder extends Model
{
    protected $fillable = [
        'id',
    ];

    public function order_to_products()
    {
        return $this->hasMany('App\Model\Order\BVOrder\CreateBVOrderToProduct', 'create_orders_id', 'id');
    }

    public function customer()
    {
        return $this->hasOne('App\Customer', 'id', 'customer_id');
    }

    public function create_b_v_order_to_invoices()
    {
        return $this->hasOne('App\Model\Order\BVOrder\CreateBVOrderToInvoice', 'create_b_v_order_id', 'id');
    }

    public function create_b_v_order_to_payments()
    {
        return $this->hasOne('App\Model\BVinvoice\Payment\PaymentDetails',
            'create_order_id', 'id')->latest();
    }

    public function order_to_pickups()
    {
        return $this->hasOne('App\Model\Order\BVOrder\OrderToPickup',
            'create_b_v_order_id', 'id')->latest();
    }

}
