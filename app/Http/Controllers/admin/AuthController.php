<?php

namespace App\Http\Controllers\admin;

use App\Admin;
use App\Http\Requests\AdminRegister;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use Illuminate\Support\Facades\Auth;
class AuthController extends Controller
{

    public function __construct()
    {
        $this->middleware('guest:admin', ['except' => ['logout']]);
    }



    public function loginPage(){

        return view('admin.page.login');

    }




    public function CustomerRegisterSave(AdminRegister $request ){

        $s = new Admin();
        $s->fname = 'Amit';
        $s->lname = 'Kar';
        $s->email = $request->email;
       // $s->ref = ' ';
        $s->mobile = ' ';
        $s->password = bcrypt($request->password);
        $s->save();
        return response()->json('successful',200);

    }

    public function CustomerLoginShow(Request $request){

    }


    public function CustomerLogin(Request $request){

        $validator = Validator::make($request->all(), [
            'email' => 'required|exists:admins,email',
            'password' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput($request->only('email', 'remember'));
        }


        // Attempt to log the user in
        if (Auth::guard('admin')->attempt(['email' => $request->email, 'password' => $request->password], $request->remember)) {
            // if successful, then redirect to their intended location
            return redirect()->intended(route('admin.dashboard'));
        }
        // if unsuccessful, then redirect back to the login with the form data
        return redirect() ->back()
            ->withErrors($validator, 'password')
            ->withInput($request->only('email', 'remember'));

    }

    public function logout()
    {
        Auth::guard('admin')->logout();
        return redirect('/');
    }
}
