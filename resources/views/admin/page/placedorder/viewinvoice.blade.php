@extends('admin.index');

@section('content')
    <link rel="stylesheet" type="text/css" href="{{asset('/css/print/print.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('/css/print/invoicedetails.css')}}">
    <style>
        .select2-options {


        }
    </style>
    <div class="m-content">
        <div class="row">
            <div class="col-md-12">



                <div class="m-portlet">






                    <style>
                        thead{display: table-header-group;}
                        tfoot {display: table-row-group;}
                        tr {page-break-inside: avoid;}
                        .container{
                            font-family: 'Montserrat', sans-serif;
                        }
                    </style>
                    <style>
                        .product .img-responsive {
                            margin: 0 auto;
                        }
                    </style>


                    <div class="container">
                        <div class="row">


                            <div class="col-xs-12" style="text-align: center">

                                <h2><srrong>Emporium Marketing Pvt.Ltd</srrong></h2>
                                <p> 2 1/2 Mile,Prakash Nagar, <br>
                                    Limbu Busty,(Kaviyatri Parijat Path) <br>P.O. Salugara P.S. Bhakti Nagar <br>Dist :
                                    Jalpaiguri Siliguri 734008, West Bengal</p>
                                <P>GSTIN: <strong>19AADCE7821P1Z2</strong>  	&nbsp;	&nbsp;   PAN: <strong>AADCE7821P</strong>   </P>

                                <p>
                                    <span>Phone: <i class="fas fa-rupee-sign"></i>  +91 9593488168</span> &nbsp; &nbsp;
                                    <span>Email: <i class="fas fa-rupee-sign"></i>  info@emporiummkt.com</span>
                                </p>

                            </div>









                            <div class="col-xs-12">
                                <div style="border-top: 2px solid #000000;"></div>

                                <div class="col-xs-12">
                                    <h3>Order Id: <span style="font-size: 18px;font-weight: 700">EMPL/2018-19-{{$invoice->order_secure_id}}</span></h3>
                                </div>




                                <div class="col-xs-4">
                                    <address>

                                        Invoice Date: <strong>{{$invoice->created_at}} </strong> <br>
                                        Invoice Reference: <strong>{{$invoice->id}} </strong> <br>
                                    </address>

                                    <address>
                                        Customer Name: {{$invoice->customer->fname}}  {{$invoice->customer->lname}}
                                        <strong></strong> <br>
                                        GSTIN: <strong>{{$invoice->customer_gstin}}</strong> <br>

                                    </address>
                                    Remark: {{$invoice->payment_type}} <br>
                                    Booked from: {{$invoice->bookedfrom}}
                                </div>
                                <div class="col-xs-4">
                                    <address>
                                        <strong>Billed To:</strong><br>
                                        {{$invoice->customer->street}} <br>
                                        {{$invoice->customer->locality}} <br>
                                        {{$invoice->customer->city}} <br>
                                        {{$invoice->customer->state}} <br>
                                        {{$invoice->customer->pin}} <br>

                                    </address>
                                    <address style="width: 250px">
                                        <strong>Delived at:</strong><br>
                                        {{$invoice->customer->street}} <br>
                                        {{$invoice->customer->locality}} <br>
                                        {{$invoice->customer->city}} <br>
                                        {{$invoice->customer->state}} <br>
                                        {{$invoice->customer->pin}} <br>
                                        <br>
                                    </address>
                                </div>
                                <div class="col-xs-4" >
                                    <figure class="image is-96x96" style="display: block;margin-left: auto;margin-right: auto;">
                                        {!! QrCode::size(150)->generate($invoice->order_secure_id); !!}
                                    </figure>
                                </div>



                            </div>
                        </div>

                        <div class="row">
                            <div class="col-xs-12">


                                <div class="panel-body">

                                    <style>
                                        thead{
                                            background-color: #0a8cf0;
                                            color: #ffffff;
                                        }
                                        tbody tr td{
                                            background-color: #ffffff;
                                        }
                                    </style>

                                    <table class="table table-bordered table-hover table-striped">
                                        <thead>
                                        <tr>
                                            <td><strong>S No</strong></td>
                                            <td class="text-center"><strong>Name</strong></td>
                                            <td class="text-center"><strong>Type</strong></td>
                                            <td class="text-center"><strong>HSN</strong></td>
                                            <td class="text-center"><strong>QTY</strong></td>

                                            <td class="text-center"><strong>Rate &#8377;</strong></td>
                                            <td class="text-center"><strong>Tax. Rate &#8377;</strong></td>
                                            <td class="text-center"><strong>Tax. Amt &#8377;</strong></td>


                                            <td class="text-center"><strong>Total &#8377;</strong></td>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <!-- foreach ($order->lineItems as $line) or some such thing here -->
                                        @foreach ($invoice->order_to_products as $key => $product)
                                            <tr>

                                                <td class="text-center"><i class="fas fa-rupee-sign"></i> {{$key++ +1 }}
                                                </td>
                                                <td class="text-center" style="min-width: 100px;">
                                                    {{$product->product_name}} - {{$product->color}}
                                                    - {{$product->size}} </td>
                                                <td class="text-center">
                                                    {{$product->type}}</td>
                                                <td class="text-center">
                                                    {{$product->hsn}}</td>
                                                <td class="text-center"> {{$product->quantity}}</td>


                                                <td class="text-center"> {{$product->taxable_rate}}



                                                </td>
                                                <td class="text-center"> {{$product->taxable_value}}</td>


                                                <td class="text-center"> {{$product->taxable_tax_total}}
                                                    <hr>
                                                    {{$product->taxable_tax_igst_percentage}}%
                                                </td>


                                                <td class="text-center">  {{$product->total}}</td>

                                            </tr>
                                        @endforeach
                                        <tr>

                                            <td class="text-center" style="background-color: #ffffff; color: #ffffff; border:none;"></td>
                                            <td class="text-center" style="background-color: #ffffff; color: #ffffff; border:none;"></td>
                                            <td class="text-center" style="background-color: #ffffff; color: #ffffff; border:none;"></td>
                                            <td class="text-center" style="background-color: #ffffff; color: #ffffff; border:none;"></td>
                                            <td class="text-center" style="background-color: #ffffff; color: #ffffff; border:none;"></td>
                                            <td class="text-center" style="background-color: #ffffff; color: #ffffff; border:none;"></td>
                                            <td class="text-center" style="background-color: #ffffff; color: #000000;">
                                                <strong>Total</strong></td>
                                            <td class="text-center" style="background-color: #0a8cf0; color: #ffffff;">
                                                {{$invoice->order_total_tax}}
                                            </td>


                                            <td class="text-center" style="background-color: #0a8cf0; color: #ffffff;">
                                                {{$invoice->order_total}}</td>

                                        </tr>
                                        </tbody>
                                    </table>



                                </div>













                            </div>
                        </div>
                    </div>

                    <!--end::Form-->
                </div>
                <!--end::Portlet-->







            </div>
        </div>
    </div>

























    <div class="m-content">
        <div class="row">
            <div class="col-md-12">



                <div class="m-portlet">

                    <div class="container">
                        <div class="row">


                            <div class="panel-body">
                                <div class="col-lg-4">
                                    <a class="btn btn-success"
                                       href="{{route('dashboard.placedorder.download.pdf',$invoice->id)}}">Download
                                        PDF</a>

                                </div>

                                <div class="col-lg-4">

   {{--              <a class="btn btn-info" href="{{route('dashboard.invoice.print.pdf',$invoice->id)}}" target="_blank">Print Invoice</a>--}}


                                    <button class="btn btn-info"
                                            onclick="printJS({printable:'/dashboard/placedorder/download/{{$invoice->id}}/print', type:'pdf', showModal:true})">
                                        Print Order Recipt
                                    </button>



                                </div>

                                <div class="col-lg-4">




                                    @if( !empty($invoice->order_to_invoices))
                                        <a href="{{route('dashboard.invoice.view.single',$invoice->order_to_invoices->create_invoice_id)}}"
                                           class="btn btn-primary" >View Invoice</a>
                                    @endif


                                    @if( empty($invoice->order_to_invoices))
                                            <a href="{{route('convert.placedorder.to.invoice.get',$invoice->id)}}" class="btn btn-primary btn-lg" >
                                                Convert to Invoice
                                            </a>


                                @endif


                                    {{--modeal



                                 --}}

                                <!-- Button trigger modal -->


                                    <!-- Modal -->


                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('admin_footer_script')

    <script src="{{asset('/js/print/print.min.js')}}"></script>

@endsection