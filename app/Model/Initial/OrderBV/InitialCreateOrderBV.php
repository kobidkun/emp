<?php

namespace App\Model\Initial\OrderBV;

use Illuminate\Database\Eloquent\Model;

class InitialCreateOrderBV extends Model
{
    public function initial_create_order_to_products()
    {
        return $this->hasMany('App\Model\Initial\OrderBV\InitialCreateOrderToProductBV', 'initial_create_orders_id', 'id');
    }

    public function customer()
    {
        return $this->hasOne('App\Customer', 'id', 'customer_id');
    }
}
