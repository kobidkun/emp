@extends('admin.index');

@section('content')

    <div class="m-content">
        <div class="row" >




            <div class="col-xl-3 col-lg-4">
                <div class="m-portlet m-portlet--full-height">
                    <div class="m-portlet__body">
                        <div class="m-card-profile">
                            <div class="m-card-profile__title m--hide">
                                Register new Applicant
                            </div>
                            <div class="m-card-profile__pic">
                                <div class="m-card-profile__pic-wrapper">
                                    <img src="{{asset('/assets/app/media/img/users/user4.jpg')}}" alt=""/>
                                </div>
                            </div>



                            <div class="m-card-profile__details">
												<span class="m-card-profile__name">
													{{$customer->fname}} {{$customer->lname}}
												</span>
                                <a href="mailto: {{$customer->email}}" target="_blank" class="m-card-profile__email m-link">
                                    {{$customer->email}}
                                </a>

                                <a href="tel: {{$customer->mobile}}" target="_blank" class="m-card-profile__email m-link">
                                    {{$customer->mobile}}
                                </a>
                                <br>






                                <div>

                                </div>

                            </div>
                        </div>

                        <div class="m-portlet__body-separator"></div>




                    </div>
                </div>
            </div>



            <div class="col-xl-9 col-lg-8">



                <div class="m-portlet m-portlet--tabs">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-tools">
                            <ul class="nav nav-tabs m-tabs-line m-tabs-line--danger m-tabs-line--2x m-tabs-line--right" role="tablist">

                                <li class="nav-item m-tabs__item">
                                    <a class="nav-link m-tabs__link" data-toggle="tab" href="#profileedit" role="tab">
                                        <i class="la la-user-md" aria-hidden="true"></i>
                                        Edit
                                    </a>
                                </li>








                            </ul>
                        </div>
                    </div>


                    <div class="m-portlet__body">
                        <div class="tab-content">





                            <div class="tab-pane active" id="profileedit" role="tabpanel">


                                <style>
                                    .sponcerspace{
                                        text-align: center;
                                    }

                                    .userimage{
                                        color: #0a6aa1;
                                    }
                                </style>


                                <div class="sponcerspace">


                                    @if($sponcer != null)

                                        <h2>  Sponcer Details</h2>

                                        <i class="fa fa-user fa-5x userimage"></i>


                                        <br>





                                        <h3>
                                            Name:  {{$sponcer->fname}}   {{$sponcer->lname}} <br>
                                            IC:   {{$sponcer->ic_number}} <br>
                                        </h3>


                                        <a class="btn btn-metal" href="{{route('admin.customer.details',$sponcer->id)}}"> View Profile</a>



                                    @else

                                        <h2 style="color: red">  Sponcer id Doesn't exist Please re check  </h2>
                                        <i class="fa fa-times fa-5x " style="color: red"></i>


                                    @endif




                                </div>


                                @if ($errors->any())
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif





                                   <form   role="form" method="post"
                                      action="{{ route('admin.customer.create.save',$customer->id) }}"
                                >
                                    {{ csrf_field() }}
                                    <div class="m-portlet__body">
                                        <div class="form-group m-form__group row">
                                            <div class="col-lg-4">
                                                <label for="exampleInputEmail1">
                                                    First Name
                                                </label>
                                                <div class="input-group m-input-group">
                                                    <div class="input-group-prepend">
														<span class="input-group-text">
															<i class="la la-user"></i>
														</span>
                                                    </div>
                                                    <input type="text"
                                                           class="form-control m-input--air"
                                                           placeholder="First Name"
                                                           name="fname"
                                                           value="{{$customer->fname}}"
                                                           autocomplete="off"
                                                           aria-describedby="basic-addon1">
                                                </div>
                                                <span class="m-form__help">
													First Name of customer
												</span>
                                            </div>
                                            <div class="col-lg-4">
                                                <label for="exampleInputEmail1">
                                                    Last Name
                                                </label>
                                                <div class="input-group m-input-group">
                                                    <div class="input-group-prepend">
														<span class="input-group-text">
															<i class="la la-users"></i>
														</span>
                                                    </div>
                                                    <input type="text"
                                                           class="form-control m-input--air"
                                                           placeholder="Last Name"
                                                           name="lname"  value="{{$customer->lname}}"
                                                           autocomplete="off"
                                                           aria-describedby="basic-addon1">
                                                </div>
                                                <span class="m-form__help">
													Last Name of customer
												</span>
                                            </div>


                                            <div class="col-lg-4">
                                                <label for="exampleInputEmail1">
                                                    Password
                                                </label>
                                                <div class="input-group m-input-group">
                                                    <div class="input-group-prepend">
														<span class="input-group-text">
															<i class="la la-code"></i>
														</span>
                                                    </div>
                                                    <input type="password"
                                                           class="form-control m-input--air"
                                                           placeholder="Password"
                                                           name="password"

                                                           value=""
                                                           autocomplete="new-password"
                                                           required

                                                           aria-describedby="basic-addon1">
                                                </div>
                                                <span class="m-form__help">
													Applicant's Password
												</span>
                                            </div>



                                            <div class="col-lg-4">
                                                <label for="exampleInputEmail1">
                                                    Gurdian's Name
                                                </label>
                                                <div class="input-group m-input-group">
                                                    <div class="input-group-prepend">
														<span class="input-group-text">
															<i class="la la-child"></i>
														</span>
                                                    </div>
                                                    <input type="text"
                                                           class="form-control m-input--air"
                                                           placeholder="Gurdian's Name"
                                                           name="g_name"
                                                           autocomplete="off"
                                                           aria-describedby="basic-addon1">
                                                </div>
                                                <span class="m-form__help">
													Father, Gurdian, Husband's Name
												</span>
                                            </div>
                                            <div class="col-lg-4">
                                                <label for="exampleInputEmail1">
                                                    Date of Birth
                                                </label>
                                                <div class="input-group m-input-group">
                                                    <div class="input-group-prepend">
														<span class="input-group-text">
															<i class="la la-calendar"></i>
														</span>
                                                    </div>
                                                    <input type="text"
                                                           class="form-control m-input--air"
                                                           placeholder="Date of Birth"
                                                           id="m_datepicker_1" readonly
                                                           name="dob"  value="{{$customer->dob}}"
                                                           autocomplete="off"
                                                           aria-describedby="basic-addon1">
                                                </div>
                                                <span class="m-form__help">
													Applicant's Date of Birth
												</span>
                                            </div>
                                            <div class="col-lg-4">
                                                <label for="exampleInputEmail1">
                                                    Pan Number for Indian
                                                </label>
                                                <div class="input-group m-input-group">
                                                    <div class="input-group-prepend">
														<span class="input-group-text">
															<i class="la la-square-o"></i>
														</span>
                                                    </div>
                                                    <input type="text"
                                                           class="form-control m-input--air"
                                                           placeholder="Pan Number"
                                                           name="pan"
                                                           autocomplete="off"
                                                           aria-describedby="basic-addon1">
                                                </div>
                                                <span class="m-form__help">
													Applicant's Pan Number if Indian
												</span>
                                            </div>
                                            <div class="col-lg-4">
                                                <label for="exampleInputEmail1">
                                                    Aadhar for Indian
                                                </label>
                                                <div class="input-group m-input-group">
                                                    <div class="input-group-prepend">
														<span class="input-group-text">
															<i class="la la-tablet"></i>
														</span>
                                                    </div>
                                                    <input type="text"
                                                           class="form-control m-input--air"
                                                           placeholder="Aadhar Number"
                                                           name="aadhar"
                                                           autocomplete="off"
                                                           aria-describedby="basic-addon1">
                                                </div>
                                                <span class="m-form__help">
													Applicant's Aadhar Number if Indian
												</span>
                                            </div>
                                            <div class="col-lg-4">
                                                <label for="exampleInputEmail1">
                                                    Billing Name
                                                </label>
                                                <div class="input-group m-input-group">
                                                    <div class="input-group-prepend">
														<span class="input-group-text">
															<i class="la la-building-o"></i>
														</span>
                                                    </div>
                                                    <input type="text"
                                                           class="form-control m-input--air"
                                                           placeholder="Billing Name"
                                                           name="billing_name"
                                                           autocomplete="off"
                                                           aria-describedby="basic-addon1">
                                                </div>
                                                <span class="m-form__help">
													Billing Name
												</span>
                                            </div>
                                            <div class="col-lg-4">
                                                <label for="exampleInputEmail1">
                                                    GSTIN for Indian
                                                </label>
                                                <div class="input-group m-input-group">
                                                    <div class="input-group-prepend">
														<span class="input-group-text">
															<i class="la la-life-ring"></i>
														</span>
                                                    </div>
                                                    <input type="text"
                                                           class="form-control m-input--air"
                                                           placeholder="GSTIN Number"
                                                           name="gstin"
                                                           autocomplete="off"
                                                           aria-describedby="basic-addon1">
                                                </div>
                                                <span class="m-form__help">
													Applicant's GSTIN Number if Indian
												</span>
                                            </div>
                                            <div class="col-lg-4">
                                                <label for="exampleInputEmail1">
                                                    GSTIN State
                                                </label>
                                                <select  name="gstin_state" class="form-control m-input">

                                                    <option value="35">Andaman and Nicobar</option>
                                                    <option value="37">Andhra Pradesh</option>
                                                    <option value="12">Arunachal Pradesh</option>
                                                    <option value="18">Assam</option>
                                                    <option value="10">Bihar</option>
                                                    <!-- <option value="Chandigarh">Chandigarh</option> -->
                                                    <option value="04">Chhattisgarh</option>
                                                    <option value="26">Dadra and Nagar Haveli</option>
                                                    <option value="25">Daman and Diu</option>
                                                    <option value="07">Delhi</option>
                                                    <!-- <option value="Foreign">Foreign</option> -->
                                                    <option value="30">Goa</option>
                                                    <option value="24">Gujarat</option>
                                                    <option value="06">Haryana</option>
                                                    <option value="02">Himachal Pradesh</option>
                                                    <option value="01">Jammu and Kashmir</option>
                                                    <option value="20">Jharkhand</option>
                                                    <option value="29">Karnataka</option>
                                                    <option value="32">Kerala</option>
                                                    <option value="31">Lakshadweep</option>
                                                    <option value="23">Madhya Pradesh</option>
                                                    <option value="27">Maharastra</option>
                                                    <option value="14">Manipur</option>
                                                    <option value="17">Meghalaya</option>
                                                    <option value="15">Mizoram</option>
                                                    <option value="13">Nagaland</option>
                                                    <option value="21">Orissa</option>
                                                    <option value="34">Puducherry</option>
                                                    <option value="03">Punjab</option>
                                                    <option value="08">Rajasthan</option>
                                                    <option value="11">Sikkim</option>
                                                    <option value="33">Tamil Nadu</option>
                                                    <option value="36">Telangana</option>
                                                    <option value="16">Tripura</option>
                                                    <option value="09">Uttar Pradesh</option>
                                                    <option value="05">Uttarakhand</option>
                                                    <option value="19">West Bengal</option>

                                                </select>
                                            </div>



                                            <div class="col-lg-4">
                                                <label for="exampleInputEmail1">
                                                    IC Number
                                                </label>
                                                <div class="input-group m-input-group">
                                                    <div class="input-group-prepend">
														<span class="input-group-text">
															<i class="la la-user-plus"></i>
														</span>
                                                    </div>
                                                    <input type="text"

                                                           class="form-control m-input--air"
                                                           placeholder="IC Number"
                                                           name="ic_number"
                                                           value="{{$ic_number}}"
                                                           autocomplete="off"
                                                           aria-describedby="basic-addon1">
                                                </div>
                                                <span class="m-form__help">
													Applicant's IC Number
												</span>
                                            </div>

                                            <div class="col-lg-4">
                                                <label for="exampleInputEmail1">
                                                    Sponser Id
                                                </label>
                                                <div class="input-group m-input-group">
                                                    <div class="input-group-prepend">
														<span class="input-group-text">
															<i class="la la-user-plus"></i>
														</span>
                                                    </div>
                                                    <input type="text"

                                                           class="form-control m-input--air sponcer--id"

                                                           id="sponcerid"

                                                           placeholder="Sponser Id"
                                                           name="sponser_id"  value="{{$customer->sponser_id}}"
                                                           autocomplete="off"
                                                           aria-describedby="basic-addon1">
                                                </div>
                                                <span class="m-form__help">
													Applicant's Sponser Id
												</span>
                                            </div>




                                            <div class="col-lg-4">
                                                <label for="exampleInputEmail1">
                                                    Rank
                                                </label>
                                                <div class="input-group m-input-group">
                                                    <div class="input-group-prepend">
														<span class="input-group-text">
															%
														</span>
                                                    </div>
                                                    <input type="text"
                                                           class="form-control m-input--air"
                                                           placeholder="Rank"
                                                           name="commission"
                                                           autocomplete="off"
                                                           aria-describedby="basic-addon1">
                                                </div>
                                                <span class="m-form__help">
													Applicant's Rank
												</span>
                                            </div>


                                        </div>



                                        <div class="form-group m-form__group row">
                                            <div class="col-lg-4">
                                                <label for="exampleInputEmail1">
                                                    Email
                                                </label>
                                                <div class="input-group m-input-group">
                                                    <div class="input-group-prepend">
														<span class="input-group-text">
															<i class="la la-envelope"></i>
														</span>
                                                    </div>
                                                    <input type="text"
                                                           class="form-control m-input--air"
                                                           placeholder="Email"  value="{{$customer->email}}"
                                                           name="email"
                                                           autocomplete="off"
                                                           aria-describedby="basic-addon1">
                                                </div>
                                                <span class="m-form__help">
													Email of customer
												</span>
                                            </div>
                                            <div class="col-lg-4">
                                                <label for="exampleInputEmail1">
                                                    Mobile
                                                </label>
                                                <div class="input-group m-input-group">
                                                    <div class="input-group-prepend">
														<span class="input-group-text">
															<i class="la la-mobile-phone"></i>
														</span>
                                                    </div>
                                                    <input type="text"
                                                           class="form-control m-input--air"
                                                           placeholder="Mobile"
                                                           name="mobile"   value="{{$customer->mobile}}"
                                                           autocomplete="off"
                                                           aria-describedby="basic-addon1">
                                                </div>
                                                <span class="m-form__help">
													Mobile of customer
												</span>
                                            </div>
                                            <div class="col-lg-4">
                                                <label for="exampleInputEmail1">
                                                    Phone Number
                                                </label>
                                                <div class="input-group m-input-group">
                                                    <div class="input-group-prepend">
														<span class="input-group-text">
															<i class="la la-phone"></i>
														</span>
                                                    </div>
                                                    <input type="text"
                                                           class="form-control m-input--air"
                                                           placeholder="Phone Number"
                                                           name="phone"
                                                           required
                                                           autocomplete="off"
                                                           aria-describedby="basic-addon1">
                                                </div>
                                                <span class="m-form__help">
													Phone Number of customer
												</span>
                                            </div>
                                        </div>


                                        <div class="form-group m-form__group row">
                                            <div class="col-lg-4">
                                                <label for="exampleInputEmail1">
                                                    Street Name
                                                </label>
                                                <div class="input-group m-input-group">
                                                    <div class="input-group-prepend">
														<span class="input-group-text">
															<i class="la la-street-view"></i>
														</span>
                                                    </div>
                                                    <input type="text"
                                                           class="form-control m-input--air"
                                                           placeholder="Street Name"
                                                           name="street"
                                                           autocomplete="off"
                                                           aria-describedby="basic-addon1">
                                                </div>



                                            </div>
                                            <div class="col-lg-4">
                                                <label for="exampleInputEmail1">
                                                    Locality
                                                </label>
                                                <div class="input-group m-input-group">
                                                    <div class="input-group-prepend">
														<span class="input-group-text">
															<i class="la la-location-arrow"></i>
														</span>
                                                    </div>
                                                    <input type="text"
                                                           class="form-control m-input--air"
                                                           placeholder="Locality"
                                                           name="locality"
                                                           autocomplete="off"
                                                           aria-describedby="basic-addon1">
                                                </div>



                                            </div>
                                            <div class="col-lg-4">
                                                <label for="exampleInputEmail1">
                                                    City / Village
                                                </label>
                                                <div class="input-group m-input-group">
                                                    <div class="input-group-prepend">
														<span class="input-group-text">
															<i class="la la-map-signs"></i>
														</span>
                                                    </div>
                                                    <input type="text"
                                                           class="form-control m-input--air"
                                                           placeholder="City / Village"
                                                           name="city"
                                                           autocomplete="off"
                                                           aria-describedby="basic-addon1">
                                                </div>



                                            </div>
                                            <div class="col-lg-4">
                                                <label for="exampleInputEmail1">
                                                    State
                                                </label>
                                                <div class="input-group m-input-group">
                                                    <div class="input-group-prepend">
														<span class="input-group-text">
															<i class="la la-map-o"></i>
														</span>
                                                    </div>
                                                    <input type="text"
                                                           class="form-control m-input--air"
                                                           placeholder="State"
                                                           name="state"
                                                           autocomplete="off"
                                                           aria-describedby="basic-addon1">
                                                </div>



                                            </div>
                                            <div class="col-lg-4">
                                                <label for="exampleInputEmail1">
                                                    Country
                                                </label>
                                                <div class="input-group m-input-group">
                                                    <div class="input-group-prepend">
														<span class="input-group-text">
															<i class="la la-crosshairs"></i>
														</span>
                                                    </div>
                                                    <input type="text"
                                                           class="form-control m-input--air"
                                                           placeholder="Country"
                                                           name="country"
                                                           autocomplete="off"
                                                           aria-describedby="basic-addon1">
                                                </div>



                                            </div>
                                            <div class="col-lg-4">
                                                <label for="exampleInputEmail1">
                                                    Pincode
                                                </label>
                                                <div class="input-group m-input-group">
                                                    <div class="input-group-prepend">
														<span class="input-group-text">
															<i class="la la-map-marker"></i>
														</span>
                                                    </div>
                                                    <input type="text"
                                                           class="form-control m-input--air"
                                                           placeholder="Pincode"
                                                           name="pin"
                                                           autocomplete="off"
                                                           aria-describedby="basic-addon1">
                                                </div>



                                            </div>




                                        </div>
                                        <div class="form-group m-form__group row">
                                            <div class="col-lg-4">
                                                <label for="exampleInputEmail1">
                                                    Nomiee Name
                                                </label>
                                                <div class="input-group m-input-group">
                                                    <div class="input-group-prepend">
														<span class="input-group-text">
															<i class="la la-user"></i>
														</span>
                                                    </div>
                                                    <input type="text"
                                                           class="form-control m-input--air"
                                                           placeholder="Nominee Name"
                                                           name="nominee_name"
                                                           autocomplete="off"
                                                           aria-describedby="basic-addon1">
                                                </div>



                                            </div>
                                            <div class="col-lg-4">
                                                <label for="exampleInputEmail1">
                                                    Relation with Applicant
                                                </label>
                                                <div class="input-group m-input-group">
                                                    <div class="input-group-prepend">
														<span class="input-group-text">
															<i class="la la-venus-mars"></i>
														</span>
                                                    </div>
                                                    <input type="text"
                                                           class="form-control m-input--air"
                                                           placeholder="Relation with Applicant"
                                                           name="nominee_relation"
                                                           autocomplete="off"
                                                           aria-describedby="basic-addon1">
                                                </div>



                                            </div>
                                            <div class="col-lg-4">
                                                <label for="exampleInputEmail1">
                                                    Dob
                                                </label>
                                                <div class="input-group m-input-group">
                                                    <div class="input-group-prepend">
														<span class="input-group-text">
															<i class="la la-calendar"></i>
														</span>
                                                    </div>
                                                    <input type="text"
                                                           class="form-control m-input--air"
                                                           placeholder="DOB"
                                                           id="m_datepicker_1" readonly
                                                           name="nominee_dob"
                                                           autocomplete="off"
                                                           aria-describedby="basic-addon1">
                                                </div>



                                            </div>

                                        </div>
                                        <div class="form-group m-form__group row">
                                            <div class="col-lg-4">
                                                <label for="exampleInputEmail1">
                                                    Bank Name
                                                </label>
                                                <div class="input-group m-input-group">
                                                    <div class="input-group-prepend">
														<span class="input-group-text">
															<i class="la la-bank"></i>
														</span>
                                                    </div>
                                                    <input type="text"
                                                           class="form-control m-input--air"
                                                           placeholder="Bank Name"
                                                           name="bank_name"
                                                           autocomplete="off"
                                                           aria-describedby="basic-addon1">
                                                </div>



                                            </div>
                                            <div class="col-lg-4">
                                                <label for="exampleInputEmail1">
                                                    Bank Branch Name
                                                </label>
                                                <div class="input-group m-input-group">
                                                    <div class="input-group-prepend">
														<span class="input-group-text">
															<i class="la la-bullseye"></i>
														</span>
                                                    </div>
                                                    <input type="text"
                                                           class="form-control m-input--air"
                                                           placeholder="Bank Branch Name"
                                                           name="bank_branch"
                                                           autocomplete="off"
                                                           aria-describedby="basic-addon1">
                                                </div>



                                            </div>
                                            <div class="col-lg-4">
                                                <label for="exampleInputEmail1">
                                                    IFSC Code
                                                </label>
                                                <div class="input-group m-input-group">
                                                    <div class="input-group-prepend">
														<span class="input-group-text">
															<i class="la la-calendar"></i>
														</span>
                                                    </div>
                                                    <input type="text"
                                                           class="form-control m-input--air"
                                                           placeholder="IFSC Code"
                                                           name="bank_ifsc"
                                                           autocomplete="off"
                                                           aria-describedby="basic-addon1">
                                                </div>



                                            </div>
                                            <div class="col-lg-4">
                                                <label for="exampleInputEmail1">
                                                    Account Holder Name
                                                </label>
                                                <div class="input-group m-input-group">
                                                    <div class="input-group-prepend">
														<span class="input-group-text">
															<i class="la la-user"></i>
														</span>
                                                    </div>
                                                    <input type="text"
                                                           class="form-control m-input--air"
                                                           placeholder="Account Holder Name"
                                                           name="bank_account_holder_name"
                                                           autocomplete="off"
                                                           aria-describedby="basic-addon1">
                                                </div>



                                            </div>
                                            <div class="col-lg-4">
                                                <label for="exampleInputEmail1">
                                                    Account Number
                                                </label>
                                                <div class="input-group m-input-group">
                                                    <div class="input-group-prepend">
														<span class="input-group-text">
															<i class="la la-calculator"></i>
														</span>
                                                    </div>
                                                    <input type="text"
                                                           class="form-control m-input--air"
                                                           placeholder="Account Number"
                                                           name="bank_account_number"
                                                           autocomplete="off"
                                                           aria-describedby="basic-addon1">
                                                </div>



                                            </div>
                                        </div>







                                    </div>


                                    <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                                        <div class="m-form__actions m-form__actions--solid">
                                            <div class="row">
                                                <div class="col-lg-4"></div>
                                                <div class="col-lg-8">




                                                    @if($sponcer != null)

                                                        <button type="submit" class="btn btn-primary">
                                                            Submit
                                                        </button>



                                                    @else



                                                        <button type="submit" class="btn btn-primary registercustomer">
                                                            Submit
                                                        </button>


                                                    @endif




                                                    <button type="reset" class="btn btn-secondary">
                                                        Reset
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                </form>







                            </div>









                        </div>
                    </div>
                </div>


            </div>








        </div>
    </div>



 @endsection

@section('admin_footer_script')


    <script>
        //== Class definition

        var BootstrapDatepicker = function () {

            //== Private functions
            var demos = function () {
                // minimum setup
                $('#m_datepicker_1, #m_datepicker_1_validate').datepicker({
                    todayHighlight: true,
                    orientation: "bottom left",
                    templates: {
                        leftArrow: '<i class="la la-angle-left"></i>',
                        rightArrow: '<i class="la la-angle-right"></i>'
                    }
                });
            }

            return {
                // public functions
                init: function() {
                    demos();
                }
            };
        }();

        jQuery(document).ready(function() {
            BootstrapDatepicker.init();
        });
    </script>



    <link rel="stylesheet" href="{{ asset('/plugin/ui-autocomplete/jquery-ui.min.css')}}"/>
    <link rel="stylesheet" href="{{ asset('/plugin/ui-autocomplete/jquery-ui.theme.min.css')}}"/>
    <script src="{{ asset('/plugin/ui-autocomplete/jquery-ui.min.js')}}" type="text/javascript"></script>

    {{--auto serch--}}




    <style>
        .treeviewbutton{
            margin-top: 5px;
        }
    </style>




    @if($sponcer != null)





    @else



        {{--auto serch--}}
        <script>
            $(document).ready(function () {


                var src = "{{ route('api.get.customer.apisearch') }}";
                $("#sponcerid").autocomplete({
                    source: function (request, response) {
                        $.ajax({
                            url: src,
                            dataType: "json",
                            data: {
                                term: request.term
                            },
                            success: function (data) {
                                response(data);

                            }


                        });
                    },
                    minLength: 1,
                    select: function (event, ui) {
                        event.preventDefault();
                        $(this).val(ui.item.ic_number);
                        //  $('.sponser--id').val(ui.item.sponser_id);

                        if(ui.item.id >= 1){

                            $(".registercustomer").removeAttr("disabled");

                        } else {
                            alert('Sponcer id not Available')
                        }










                    }

                });
            });
        </script>


    @endif

{{--
    <script>






        var downline = [];

        $.ajax({
            /* The whisperingforest.org URL is not longer valid, I found a new one that is similar... */
            url:"{{route('admin.tree.view.by.id.downline',$customer->id)}}",
            //  async: true,
            dataType: 'json',

            success: function(obj){
                // var json = $.parseJSON(obj);

                $(".primary-member").replaceWith('<i style="font-size: 33px; color: #e12500!important; text-align: center; padding-left: 75px" class="fa fa-user"></i> <br> ' +
                    '<button  class="btn btn-danger primary-member">'+obj.customer.fname+'<br> Sponcer Id: '+obj.customer.sponser_id+'<br> IC NO: '+obj.customer.ic_number+'</button> ')



                $.each(obj.downline,function(k,v){
                    $("#downline").append('<i style="font-size: 33px; color: #5de178!important; text-align: center; padding-left: 125px" class="fa fa-user"></i> <br>' +
                        '<a href="/dashboard/admin/tree-view/view/'+v.id+'"  class="btn btn-success treeviewbutton">'+v.fname+ ' '+v.lname +
                        ' <br> IC No:  '+v.ic_number+
                        ' <br> Sponcer ID:  '+v.sponser_id+'</a> ' +
                        '<br>')


                    console.log(v.fname);
                });
            },
            error: function(error){
                alert(error);
            }
        })








    </script>
--}}





@endsection