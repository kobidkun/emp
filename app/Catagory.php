<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Catagory extends Model
{
    public function products()
    {
        return $this->belongsToMany('App\Product');
    }

    public function category2()
    {
        return $this->hasMany('App\Category2','id','catagory_id');
    }
}
