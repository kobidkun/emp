<?php

namespace App\Http\Controllers\admin\customer;

use App\Customer;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\DataTables;

class CustomerWallet extends Controller
{
    public function AddAmount(Request $request){

        $a = new \App\Customer\CustomerWallet();

        $a->fname = $request->fname;
        $a->lname = $request->lname;
        $a->txt_id = $this->generateRandomString();
        $a->mobile = $request->mobile;
        $a->email = $request->email;
        $a->customer_id = $request->customer_id;
        $a->amount = $request->amount;
        $a->methods = $request->methods;
        $a->gateway_charges = $request->gateway_charges;
        $a->charges = $request->charges;
        $a->taxes = $request->taxes;

        $a->save();



        $b = Customer::findorfail($request->customer_id);

        $currentbalance = $b->waller_balance;

        $b->waller_balance = ($currentbalance+ $request->amount);
        $b->save();


        return back();






    }


    public function GetDatatables($id){



        $users = \App\Customer\CustomerWallet::where('customer_id', $id)->select(
            ['id','txt_id',
                'amount',
                'fname',
                'lname',
                'mobile',
                'email',
                'methods',
                'gateway_charges',
                'charges',
                'created_at']);



        return DataTables::of($users)
            ->editColumn('created_at', function($user) {
                return Carbon::createFromFormat('Y-m-d H:i:s', $user->created_at)->toDateString();
            })

            ->editColumn('amount', function($user) {
                return '<i class="la la-inr"></i> '.$user->amount;
            })

            ->editColumn('charges', function($user) {
                return '<i class="la la-inr"></i> '.$user->charges;
            })

            ->addColumn('action', function ($user) {

                return '<a href="'.route('admin.customer.wallet.delete',$user->id).'" class="btn btn-danger"> Delete</a>';

            })

            ->rawColumns(['amount','charges','action'])
            ->make();





    }


    public function Delete($id){
        $a =  \App\Customer\CustomerWallet::findorfail($id);

        $amount = $a->amount;





        $b = Customer::findorfail($a->customer_id);

        $currentbalance = $b->waller_balance;

        $b->waller_balance = ($currentbalance-$amount);
        $a->delete();

        $b->save();
        return back();


    }

    function generateRandomString($length = 10) {
        $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }



}
