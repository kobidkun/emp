@extends('admin.index');

@section('content')
    <style>
        .select2-options {


        }
    </style>
    <div class="m-content">
        <div class="row">
            <div class="col-md-12">



                <div class="m-portlet">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
												<span class="m-portlet__head-icon m--hide">
													<i class="la la-gear"></i>
												</span>
                                <h3 class="m-portlet__head-text">
                                    Create new Product
                                </h3>
                            </div>
                        </div>
                    </div>


                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                @endif



                    <!--begin::Form-->
                    <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed"
                          role="form" method="post"
                          action="{{ route('admin.product.create.save') }}"
                    >
                        {{ csrf_field() }}
                        <div class="m-portlet__body">



                        {{--

                            <div class="form-group m-form__group row">



                                <div class="col-lg-12">


                                    <div class="col-md-3">
                                        <label for="exampleInputEmail1">
                                            Product Name
                                        </label>
                                        <div class="input-group m-input-group">
                                            <div class="input-group-prepend">
                                                         <span class="input-group-text">
                                                             <i class="la la-barcode"></i>
                                                         </span>
                                            </div>
                                            <input type="text"
                                                   class="form-control m-input--air"
                                                   placeholder="Product Name"
                                                   name="name"
                                                   autocomplete="off"
                                                   aria-describedby="basic-addon1">
                                        </div>
                                    </div>






                                </div>





                            </div>


--}}

                            <div class="form-group m-form__group row">
                                <div class="col-lg-4">
                                    <label for="exampleInputEmail1">
                                        Product Name
                                    </label>
                                    <div class="input-group m-input-group">
                                        <div class="input-group-prepend">
                                                         <span class="input-group-text">
                                                             <i class="la la-barcode"></i>
                                                         </span>
                                        </div>
                                        <input type="text"
                                               class="form-control m-input--air"
                                               placeholder="Product Name"
                                               name="name"
                                               autocomplete="off"
                                               aria-describedby="basic-addon1">
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <label for="exampleInputEmail1">
                                        Product HSN
                                    </label>
                                    <div class="input-group m-input-group">
                                        <div class="input-group-prepend">
                                                         <span class="input-group-text">
                                                             <i class="la la-barcode"></i>
                                                         </span>
                                        </div>
                                        <input type="text"
                                               class="form-control m-input--air"
                                               placeholder="Product HSN"
                                               name="hsn"
                                               autocomplete="off"
                                               aria-describedby="basic-addon1">
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <label for="exampleInputEmail1">
                                        Product Code
                                    </label>
                                    <div class="input-group m-input-group">
                                        <div class="input-group-prepend">
                                                         <span class="input-group-text">
                                                             <i class="la la-barcode"></i>
                                                         </span>
                                        </div>
                                        <input type="text"
                                               class="form-control m-input--air"
                                               placeholder="Product Code"
                                               name="code_name"
                                               autocomplete="off"
                                               aria-describedby="basic-addon1">
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <label for="exampleInputEmail1">
                                        Product Type
                                    </label>
                                    <div class="input-group m-input-group">

                                        <select

                                                class="form-control"
                                                name="type"
                                        >
                                            <option value="Goods">Goods</option>
                                            <option value="Service">Service</option>

                                        </select>
                                    </div>
                                </div>

                                <div class="col-lg-2">
                                    <label for="exampleInputEmail1">
                                        Product Unit
                                    </label>
                                    <div class="input-group m-input-group">

                                        <select

                                                class="form-control"
                                                name="unit"
                                        >
                                            <option value="PC">PC</option>
                                            <option value="KG">KG</option>
                                            <option value="Unit">Unit</option>
                                            <option value="Meter">Meter</option>
                                            <option value="Gram">Gram</option>
                                            <option value="Decimal">Decimal</option>

                                        </select>
                                    </div>
                                </div>

                            </div>








                            <div class="form-group m-form__group row">
                                <div class="col-lg-4">
                                    <label for="exampleInputEmail1">
                                        Product Category
                                    </label>
                                    <div class="input-group m-input-group">
                                        <select class="form-control m-bootstrap-select m_selectpicker"
                                                name="catagory_id"
                                                id="primarycatplaceholder"
                                        >
                                            @foreach($cats as $cat)
                                                <option value="{{$cat->id}}">{{$cat->name}}</option>
                                            @endforeach
                                        </select>

                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <label for="exampleInputEmail1">
                                        Product Secondary Category
                                    </label>
                                    <div class="">
                                        <select

                                                class="form-control
                                                     seccatph"
                                                name="catagory2_id"
                                                id="secondarycatplaceholder">

                                        </select>

                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <label for="exampleInputEmail1">
                                        Tertiary  Product Category
                                    </label>
                                    <div class="">
                                        <select
                                                class=" form-control"
                                                name="catagory3_id"
                                                id="turtiaryycatplaceholder">

                                        </select>

                                    </div>
                                </div>

                            </div>




                            <div class="form-group m-form__group row">
                                <div class="col-lg-3">
                                    <label for="exampleInputEmail1">
                                        Maximum Retail Price
                                    </label>
                                    <div class="input-group m-input-group">
                                        <div class="input-group-prepend">
                                                         <span class="input-group-text">
                                                             <i class="la la-rupee"></i>
                                                         </span>
                                        </div>
                                        <input type="text"
                                               class="form-control m-input--ai mrp"
                                               placeholder="Price"
                                               name="mrp_price"
                                              required>
                                    </div>
                                </div>



                                <div class="col-lg-3">
                                    <label for="exampleInputEmail1">
                                        IGST / TAX %
                                    </label>
                                    <div class="input-group m-input-group">
                                        <div class="input-group-prepend">
                                                         <span class="input-group-text">
                                                             %
                                                         </span>
                                        </div>
                                        <select class="form-control m-input--ai igst-percentage"
                                                name="igst_percentage" id="">
                                            <option value="0">0%</option>
                                            <option value="5">5%</option>
                                            <option value="12">12%</option>
                                            <option value="18">18%</option>
                                            <option value="28">28%</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-lg-3">
                                    <label for="exampleInputEmail1">
                                        CGST / SGST %
                                    </label>
                                    <div class="input-group m-input-group">
                                        <div class="input-group-prepend">
                                                         <span class="input-group-text">
                                                            %
                                                         </span>
                                        </div>
                                        <input type="text"
                                               class="form-control m-input--air cgst-percentage"

                                               placeholder="CGST / SGST"
                                               readonly
                                               name="cgst_percentage">
                                    </div>

                                </div>
                                <div class="col-lg-3">
                                    <label for="exampleInputEmail1">
                                        CGST / SGST Value
                                    </label>
                                    <div class="input-group m-input-group">
                                        <div class="input-group-prepend">
                                                         <span class="input-group-text">
                                                             <i class="la la-rupee"></i>
                                                         </span>
                                        </div>
                                        <input type="text"
                                               class="form-control m-input--air cgst-amount"
                                               placeholder="CGST / SGST"
                                               name="cgst"
                                               readonly
                                               aria-describedby="basic-addon1">
                                    </div>

                                </div>







                            </div>
                            <div class="form-group m-form__group row">







                                <div class="col-lg-3">
                                    <label for="exampleInputEmail1">
                                        Taxable Rate
                                    </label>
                                    <div class="input-group m-input-group">
                                        <div class="input-group-prepend">
                                                         <span class="input-group-text">
                                                            <i class="la la-rupee"></i>
                                                         </span>
                                        </div>
                                        <input type="text"
                                               class="form-control m-input--air taxable-rate"
                                               id="discount-percentage"
                                               placeholder="Auto Calculated"
                                               name="taxable_rate"
                                               readonly
                                               aria-describedby="basic-addon1">
                                    </div>
                                    <span class="m-form__help">
                                        Auto Calculated
                                                 </span>

                                </div>

                                <div class="col-lg-3">
                                    <label for="exampleInputEmail1">
                                        Total  Tax Amount
                                    </label>
                                    <div class="input-group m-input-group">
                                        <div class="input-group-prepend">
                                                          <span class="input-group-text">
                                                             <i class="la la-rupee"></i>
                                                         </span>
                                        </div>
                                        <input type="text"
                                               readonly
                                               class="form-control m-input--air tax-amount"
                                               placeholder="Total Tax Amount"
                                               name="total_tax"
                                        >
                                    </div>


                                </div>

                                <div class="col-lg-3">
                                    <label for="exampleInputEmail1">
                                        Listing Price (TOTAL)
                                    </label>
                                    <div class="input-group m-input-group">
                                        <div class="input-group-prepend">
                                                          <span class="input-group-text">
                                                             <i class="la la-rupee"></i>
                                                         </span>
                                        </div>
                                        <input type="text"
                                               readonly
                                               class="form-control m-input--air total"
                                               id="listing-price"
                                               placeholder="Discounted Price"
                                               name="listing_price"

                                        >
                                    </div>


                                </div>
                                <div class="col-lg-3">
                                    <label for="exampleInputEmail1">
                                        Initial Stock
                                    </label>
                                    <div class="input-group m-input-group">
                                        <div class="input-group-prepend">
                                                          <span class="input-group-text">
                                                             <i class="la la-plus-square"></i>
                                                         </span>
                                        </div>
                                        <input type="number"
                                               class="form-control m-input--air"

                                               placeholder="Initial Stock"
                                               name="stock"

                                        >
                                    </div>


                                </div>






                            </div>





                            <div class="form-group m-form__group row">
                                <div class="col-lg-6">
                                    <label for="exampleInputEmail1">
                                        Short Description
                                    </label>
                                    <div class="input-group m-input-group">

                                             <textarea class="form-control m-input"
                                                       name="s_desc"

                                                       id="mytextarea"
                                                       rows="6"></textarea>
                                    </div>
                                    <span class="m-form__help">
                                                     Short Description
                                                 </span>
                                </div>

                                <div class="col-lg-6">

                                    <label for="exampleInputEmail1">
                                        Long Description
                                    </label>
                                    <div class="input-group m-input-group">

                                             <textarea class="form-control m-input"
                                                       name="l_desc"

                                                       id="mytextarea2" rows="6"></textarea>
                                    </div>
                                    <span class="m-form__help">
                                                     Long Description
                                                 </span>
                                </div>
                            </div>
















                        </div>





                        <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                            <div class="m-form__actions m-form__actions--solid">
                                <div class="row">
                                    <div class="col-lg-4"></div>
                                    <div class="col-lg-8">
                                        <button type="submit" class="btn btn-primary">
                                            Next
                                        </button>
                                        <button type="reset" class="btn btn-secondary">
                                            Reset
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!--end::Form-->
                </div>
                <!--end::Portlet-->





            </div>
        </div>
    </div>

@endsection

@section('admin_footer_script')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script src='{{asset('/production/js/admin/tinymce/tinymce.min.js')}}'></script>
    <script>
        tinymce.init({
            selector: '#mytextarea',

        });

        tinymce.init({
            selector: '#mytextarea2',

        });
    </script>
    <script>
        //== Class definition

        var BootstrapSelect = function () {

            //== Private functions
            var demos = function () {
                // minimum setup
                $('.m_selectpicker').selectpicker();
            }

            return {
                // public functions
                init: function() {
                    demos();
                }
            };
        }();

        jQuery(document).ready(function() {
            BootstrapSelect.init();
        });



        $(".js-example-placeholder-single").select2({
            placeholder: "Select a Secondary Category",
            allowClear: true
        });
    </script>

    <script>

        $( ".m_selectpicker" )
            .change(


                function () {
                    // preventDefault();
                    var abr = '<option value="">==select Primary categories==</option>';
                    $("#secondarycatplaceholder").html(abr);


                    var str = $( "#primarycatplaceholder option:selected" ).val();


                    // console.log(str)
                    $.ajax({
                        type: "GET",
                        url: "/admin/category/sec/searchforproducy/"+str,

                        success: function( resp) {
                            //   resp.preventDefault();
                            // $("#msg").prepend(resp);
                            console.log(resp.cat2.length);
                            //  var str = '<option value="0">Select Secondary Category</option>';
                            if (resp.cat2.length > 0 ) {
                                $.each(resp.cat2, function (index, value) {
                                    str = str+'<option  value="'+value.id+'">'+value.name+'</option>';
                                });
                                $("#secondarycatplaceholder").html(str);
                            } else {
                                //  var abr = '';
                                $("#secondarycatplaceholder").html('<option value="0">No Secondary categories</option>');
                            }

                            //turtiary

                            var abr2 = '<option value="">==select Secondary categories==</option>';
                            $("#turtiaryycatplaceholder").html(abr2);
                            var str2 = $( "#secondarycatplaceholder option:selected" ).val();
                            $.ajax({
                                type: "GET",
                                url: "/admin/category/tur/searchforproducy/"+str2,
                                success: function( resp2) {
                                    console.log(resp2.cat3.length);
                                    if (resp2.cat3.length > 0 ) {
                                        $.each(resp2.cat3, function (index, value) {
                                            str2 = str2+'<option  value="'+value.id+'">'+value.name+'</option>';
                                        });
                                        $("#turtiaryycatplaceholder").html(str2);
                                    } else {
                                        $("#turtiaryycatplaceholder").html('<option value="0">No Tertiary categories</option>');
                                    }
                                }
                            });



                        }
                    });




                    // console.log(str)
                }



            )
            .change();




        //turtiary



        $( ".seccatph" )
            .change(
                function () {
                    var abr2 = '<option value="">==select Secondary categories==</option>';
                    $("#turtiaryycatplaceholder").html(abr2);
                    var str2 = $( "#secondarycatplaceholder option:selected" ).val();
                    $.ajax({
                        type: "GET",
                        url: "/admin/category/tur/searchforproducy/"+str2,
                        success: function( resp2) {
                            console.log(resp2.cat3.length);
                            if (resp2.cat3.length > 0 ) {
                                $.each(resp2.cat3, function (index, value) {
                                    str2 = str2+'<option  value="'+value.id+'">'+value.name+'</option>';
                                });
                                $("#turtiaryycatplaceholder").html(str2);
                            } else {
                                $("#turtiaryycatplaceholder").html('<option value="0">No Tertiary categories</option>');
                            }
                        }
                    });
                }).change();
    </script>

    <script>
$(document).ready(function() {







    $(".mrp").on("change paste keyup keydown keypress",(function () {
        //  var LISTINGPRICE = ;
        triggertaxupdate()
    }));



    $(".igst-percentage").on("change paste keyup keydown keypress", (function () {
        triggertaxupdate()
    }));


    function triggertaxupdate() {


        var MRP = Number($(".mrp").inputVal());
        var IGST = Number($(".igst-percentage").val());
          var  TAXPERCENTAGE = (Number(IGST/100)).toFixed(2);

          var TOTALTAX = Number(MRP*TAXPERCENTAGE).toFixed(2);
           var TOTAL = (Number(MRP) + Number(TOTALTAX)).toFixed(2);


        $(".cgst-percentage").inputVal(IGST/2);
        $(".cgst-amount").inputVal(TOTALTAX/2);
        $(".tax-amount").inputVal(TOTALTAX);
        $(".taxable-rate").inputVal(MRP);
        $(".total").inputVal(TOTAL);





       // console.log(IGST);

    }



});
    </script>





@endsection