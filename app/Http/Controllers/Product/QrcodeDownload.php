<?php

namespace App\Http\Controllers\Product;

use App\ProductVatient;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use QrCode;
use Storage;

class QrcodeDownload extends Controller
{
    public function DownloadQRCode($id){



        $b = ProductVatient::findorfail($id);

       $c = $b->slug;
       $d = $b->name;
       $size = $b->size;
       $color = $b->color;
//

       $h =    QrCode::format('png')->size(512)->generate($c);

       $name = time().$d.' '.$size.' '.$color.'.png';

       $a =    Storage::put('qrcode/'.$name, $h);



       $path = storage_path('app/qrcode/');


      return response()->download($path.$name);








    }
}
