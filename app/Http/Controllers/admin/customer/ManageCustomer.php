<?php

namespace App\Http\Controllers\admin\customer;

use App\Customer;
use App\Customer\CustomerFiles as CF;
use App\CustomerToCustomer;
use App\Http\Requests\CustomerRegister;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;

class ManageCustomer extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth:admin,storeuser,cashier']);


    }

    public function CreateCustomer(){
        return view('admin.page.customer.create');
    }

    public function AdminDetailsUser($id){

        $customer = Customer::findorfail($id);

        $files = CF::where('customer_id', $id)->get();



        return view('admin.page.customer.details',['customer' => $customer,'files' => $files]);



    }

    public function CreateCustomerSave(CustomerRegister $request ){







        $s = new Customer();
        $s->fname = $request->fname;
        $s->lname = $request->lname;
        $s->ic_number = $request->ic_number;
        $s->g_name = $request->g_name;
        $s->commission = $request->commission;
        $s->dob = $request->dob;
        $s->sex = $request->sex;
        $s->ref = $request->ref;
        $s->sponser_id = $request->sponser_id;
        $s->street = $request->street;
        $s->locality = $request->locality;
        $s->city = $request->city;
        $s->state = $request->state;
        $s->country = $request->country;
        $s->pin = $request->pin;
        $s->permanent_address = $request->permanent_address;
        $s->supply_address = $request->supply_address;
        $s->email = $request->email;
        $s->mobile = $request->mobile;
        $s->phone = $request->phone;
        $s->nominee_name = $request->nominee_name;
        $s->nominee_relation = $request->nominee_relation;
        $s->nominee_address = $request->nominee_address;
        $s->nominee_dob = $request->nominee_dob;
        $s->bank_name = $request->bank_name;
        $s->bank_ifsc = $request->bank_ifsc;
        $s->bank_micr = $request->bank_micr;
        $s->bank_account_holder_name = $request->bank_account_holder_name;
        $s->bank_account_number = $request->bank_account_number;
        $s->bank_branch = $request->bank_branch;
        $s->pan = $request->pan;
        $s->aadhar = $request->aadhar;
        $s->gstin = $request->gstin;
        $s->gstin_state = $request->gstin_state;
        $s->billing_name = $request->billing_name;
        $s->password = bcrypt($request->password);
        $s->save();




       $data = $s;



       if ($request->sponser_id === null ){

            if ($data->sponcer_id === null){

   //   dd($data);


      $a = new CustomerToCustomer();
      $a->customer_id = $data->id;
      $a->ref_id = null;
      $a->ic_number = $data->ic_number;
      $a->customer_parient_ic_number = null;
      $a->up_line_customer_id = null;
      $a->down_line_customer_id = null;
      $a->customer_relation_id = null;
     // $a->direct_up_line_customer_id = null;

      $a->save();







  }

       } else {

           $this->PostRegisterUpdatemanyuser($data);

       }

























       return back();
    }



    public function PostRegisterUpdatemanyuser($data) {









     //  else {

           $findParentUser = Customer::where('ic_number',$data->sponser_id)->firstorfail();

        $updatelastdownline =  CustomerToCustomer::where('customer_id', $findParentUser->id)->firstOrFail();

        $firstDirectDownlineUpdate  = CustomerToCustomer::where('customer_id', $findParentUser->id)->firstOrFail();



        if ($firstDirectDownlineUpdate->direct_down_line_customer_id != null){

           // $findDirectDownline = $firstDirectDownlineUpdate->direct_down_line_customer_id;

            $firstDirectDownlineUpdate->direct_down_line_customer_id =  $firstDirectDownlineUpdate->direct_down_line_customer_id.",".$data->id;

        } else{
            $firstDirectDownlineUpdate->direct_down_line_customer_id = $data->id;
        }

        $firstDirectDownlineUpdate->save();



           if ($findParentUser->customer_to_customers->exists()){



             //  dd('jhjjh');


               //UPLINE DESNOT EXISTS



               if ($findParentUser->customer_to_customers->up_line_customer_id === null){



                   $getcustocus =  CustomerToCustomer::where('customer_id', $findParentUser->id)->firstOrFail();




                   //   dd($getcustocus);



                   if ($getcustocus->down_line_customer_id != null){



                       $getcustocus->down_line_customer_id = $getcustocus->down_line_customer_id.",".$data->id;


                   } else {


                       $getcustocus->down_line_customer_id = $data->id;
                       $getcustocus->direct_up_line_customer_id = $data->id;

                   }


                   $getcustocus->save();


                   $createnew = new CustomerToCustomer();
                   $createnew->customer_id = $data->id;
                   $createnew->ref_id = $data->sponser_id;
                   $createnew->ic_number = $data->ic_number;
                   $createnew->customer_parient_ic_number = $data->sponcer_id;

                   $createnew->up_line_customer_id = $findParentUser->id;
                   $createnew->down_line_customer_id = null;
                   $createnew->customer_relation_id = null;



                   // $createnew->up_line_customer_id = $findParentUser->customer_to_customers->up_line_customer_id.','.$findParentUser->customer_to_customers->customer_id;





               } else {






                   //UPLINE EXISTS


                   //  $updateparentifnull->down_line_customer_id = $getprevious.','.$data->id;







                   $getalluplineCustomer =   $findParentUser->customer_to_customers->up_line_customer_id;

                   $convertuplinetoarray = (explode(",",$getalluplineCustomer));


                   //  dd($convertuplinetoarray);


                   foreach ($convertuplinetoarray as $key => $member){




                       $getcustocus =  CustomerToCustomer::where('customer_id', $member)->firstOrFail();


                      //    dd($getcustocus->down_line_customer_id);
                      //    dd($getcustocus->down_line_customer_id);



                       if ($getcustocus->down_line_customer_id === null){



                           $getcustocus->down_line_customer_id = $member;


                       } else  {

                        //   dd('55');

                           $getcustocus->down_line_customer_id = $getcustocus->down_line_customer_id.",".$data->id;




                       }


                       $getcustocus->save();







                   }








                   // add last downline



                //   $updatelastdownline =  $findParentUser;




                   if ($updatelastdownline->down_line_customer_id === null) {

                       $updatelastdownline->down_line_customer_id = $data->id;

                   } else {

                       $updatelastdownlinepreviousval = $updatelastdownline->down_line_customer_id;

                       $updatelastdownline->down_line_customer_id = $updatelastdownlinepreviousval.','.$data->id;
                   }







                   $updatelastdownline->save();





















               }


               //create new customer to customer


               $createnew = new CustomerToCustomer();
               $createnew->customer_id = $data->id;
               $createnew->ref_id = $data->sponser_id;
               $createnew->ic_number = $data->ic_number;
               $createnew->customer_parient_ic_number = $data->sponser_id;

               $createnew->up_line_customer_id = null;
               $createnew->down_line_customer_id = null;
               $createnew->customer_relation_id = null;
               $createnew->direct_up_line_customer_id = $data->id;


               if ($findParentUser->customer_to_customers->up_line_customer_id === null){

                   $createnew->up_line_customer_id = $findParentUser->customer_to_customers->customer_id;

               } else {

                   $createnew->up_line_customer_id = $findParentUser->customer_to_customers->up_line_customer_id.','.$findParentUser->customer_to_customers->customer_id;
               }









               $createnew->save();







               //   dd($getallupline);

               //  return true;


           }






           else {

               $a = new CustomerToCustomer();
               $a->customer_id = $data->id;
               $a->ref_id = $data->sponser_id;
               $a->ic_number = $data->ic_number;
               $a->customer_parient_ic_number = $data->sponser_id;
               $a->up_line_customer_id = $findParentUser->id;
               $a->down_line_customer_id = null;
               $a->customer_relation_id = null;
               $a->direct_up_line_customer_id = $data->id;

               $a->save();

           }





              // return back();



       //    }





      //  return back();










    }



    public function AllCustomer(){
        return view('admin.page.customer.all');
    }

    public function getCustomerDatatables()
    {
        $customers = Customer::select([
            'id',
            'fname',
            'lname',
            'email',
            'mobile',
            'ic_number',
            'sponser_id',
            'pan',
            'waller_balance',
            'commission',
        ])->orderBy('updated_at', 'DESC');

        return DataTables::of($customers)





            ->addColumn('action', function ($invoice) {
                return '<a href="'.route('admin.customer.details',$invoice->id).'" class=" btn btn-xs btn-primary" title="View details"><i class="la la-edit"></i>View Details</a>';
            })


            ->rawColumns(['action'])
            ->make(true);
    }

    public function UpdateUser(Request $request, $id){
       $s = Customer::findorfail($id);
        $s->fname = $request->fname;
        $s->lname = $request->lname;
        $s->g_name = $request->g_name;
        $s->commission = $request->commission;
        $s->dob = $request->dob;
        $s->sex = $request->sex;
        $s->ref = $request->ref;
        $s->street = $request->street;
        $s->locality = $request->locality;
        $s->city = $request->city;
        $s->state = $request->state;
        $s->country = $request->country;
        $s->pin = $request->pin;
        $s->permanent_address = $request->permanent_address;
        $s->supply_address = $request->supply_address;
        $s->email = $request->email;
        $s->mobile = $request->mobile;
        $s->phone = $request->phone;
        $s->nominee_name = $request->nominee_name;
        $s->nominee_relation = $request->nominee_relation;
        $s->nominee_address = $request->nominee_address;
        $s->nominee_dob = $request->nominee_dob;
        $s->bank_name = $request->bank_name;
        $s->bank_ifsc = $request->bank_ifsc;
        $s->bank_micr = $request->bank_micr;
        $s->bank_account_holder_name = $request->bank_account_holder_name;
        $s->bank_account_number = $request->bank_account_number;
        $s->bank_branch = $request->bank_branch;
        $s->pan = $request->pan;
        $s->aadhar = $request->aadhar;
        $s->gstin = $request->gstin;
        $s->gstin_state = $request->gstin_state;
        $s->billing_name = $request->billing_name;
        $s->ic_number = $request->ic_number;
        $s->password = bcrypt($request->password);
        $s->save();
        return back();
    }



    public function ChangePassword(Request $request, $id){

        $password = bcrypt($request->password);

        $a = Customer::findorfail($id);

        $a->password = $password;

        $a->save();

        return back();



    }


    public function ChangeRank(Request $request, $id){

        $rank = $request->rank;

        $a = Customer::findorfail($id);

        $a->commission = $rank;

        $a->save();

        return back();



    }



    public function Deleteuser(Request $request, $id){


        $getuser = Customer::findorfail($id);

        $geticnumber = Customer::where('sponser_id',$getuser->ic_number)->get();

     //   dd($geticnumber);



        if (Customer::where('sponser_id',$getuser->ic_number)->count() === 0 ){


            $findctoc = CustomerToCustomer::where('customer_id', $id);

            $getuser->delete();
            $findctoc->delete();

            return redirect(route('admin.customer.all'));
        } else return 'cant delete down line exists';










    }



    public function GetAllCustomerviaSearch(Request $request){


        $query = $request->get('term','');

        $customers = Customer::where('fname', 'LIKE', '%' . $query . '%')
            ->orwhere('ic_number', 'LIKE', '%' . $query . '%')

            ->take(5)->get();

        $data=array();
        foreach ($customers as $customer) {
            $data[]=array(
                'value'=>$customer->fname .' '. $customer->lname,
                'lname'=>$customer->lname,
                'id'=>$customer->id,
                'ic_number'=>$customer->ic_number,
                'gstin'=>$customer->gstin,
                'street'=>$customer->street,
                'locality'=>$customer->locality,
                'city'=>$customer->city,
                'state'=>$customer->state,
                'country'=>$customer->country,
                'pin' => $customer->pin,
                'permanent_address' => $customer->permanent_address,
                'email' => $customer->email,
                'mobile' => $customer->mobile,
                'pan' => $customer->pan,
                'gstin_state' => $customer->gstin_state,
                'billing_name' => $customer->billing_name,
                'commission' => $customer->commission,

            );
        }
        if(count($data))
            return $data;
        else
            return ['value'=>'No Result Found','id'=>''];
    }




    public function getTempCustomerDatatables()
    {
        $customers = Customer\TempCustomer::select([
            'id',
            'fname',
            'lname',
            'email',
            'mobile',
            'sponser_id',
        ])->orderBy('updated_at', 'DESC');

        return DataTables::of($customers)





            ->addColumn('action', function ($invoice) {
                return '<a href="'.route('admin.customer.temp.profile',$invoice->id).'" class=" btn btn-xs btn-primary" title="View details"><i class="la la-edit"></i>View Details</a>';
            })


            ->rawColumns(['action'])
            ->make(true);
    }

    public function AllTempCustomer(){
        return view('admin.page.customer.temp.all');
    }


    public function ViewTempCustomer($id){




        $customer = Customer\TempCustomer::find($id);

        $sponcer = Customer::where('ic_number', $customer->sponser_id)->first();

        $icnumbe  = Customer::max('ic_number')+1;



      //  dd($sponcer);

        return view('admin.page.customer.temp.details')->with([
            'customer' => $customer,
            'sponcer' => $sponcer,
            'ic_number' => $icnumbe
        ]);
    }


}
