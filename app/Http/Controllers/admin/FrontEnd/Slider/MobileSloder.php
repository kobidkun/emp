<?php

namespace App\Http\Controllers\admin\FrontEnd\Slider;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class MobileSloder extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth:admin,storeuser,cashier']);


    }


    public function View(){
        $a = \App\Model\FrontEnd\Slider\MobileSloder::all();
        return view('admin.page.frontend.mobileslider')->with(['images' => $a]);
    }
    public function Upload(Request $request){

        $imageName1 = time().'.'.$request->image->getClientOriginalExtension();
        $imageName2 = str_replace(' ', '_', $imageName1);
        $imageName = str_replace(' ', '_', $imageName1);
        $image = $request->file('image');
        $t = Storage::disk('s3')->put('slider/mobile/'.$imageName, file_get_contents($image), 'slider/mobile/');
        $imageName = Storage::disk('s3')->url('slider/mobile/'.$imageName);
        // $imageName3 = Storage::disk('s3')->url('product_image/primary/'.$imageName);

        //  $replacesextraspaces1 = str_replace(' ', '_', $finalimageurl1);

        $s = new \App\Model\FrontEnd\Slider\MobileSloder();
        $s->url = $imageName;
        $s->title = $request->title;

        $s->cdn_url = env('cdnurl').'/slider/mobile/'.$imageName2;

        $s->name = $imageName2;
        $s->save();
        return $imageName2;
    }


    public function Delete($id){
        $img = \App\Model\FrontEnd\Slider\MobileSloder::findorfail($id);

        Storage::disk('s3')->delete('slider/mobile/'.$img->name);
        $img->delete();
        return redirect()->back();
    }



}
