<?php

namespace App\Http\Controllers\admin\Misc;

use App\Customer;
use App\Model\customer\Misc\DBNotification;

use App\Http\Controllers\Controller;


class DBHelperNotify extends Controller
{
    function sendDBMessage($user,$type,$message,$slug){
        $a = new DBNotification();
        $a->customer_id = $user;
        $a->type =$type;
        $a->read = false;
        $a->msg =$message;
        $a->slug =$slug;
        $a->save();


    }


}
