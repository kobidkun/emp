<?php

namespace App\Http\Controllers\admin\customer;

use App\Customer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\DataTables;
use Carbon\Carbon;
class ManageCustomerBVWallet extends Controller
{
    public function AddAmount(Request $request){

        $a = new \App\Customer\CustomerBVWallet();

        $a->fname = $request->fname;
        $a->lname = $request->lname;
        $a->txt_id = $this->generateRandomString();
        $a->mobile = $request->mobile;
        $a->email = $request->email;
        $a->customer_id = $request->customer_id;
        $a->amount = $request->amount;
        $a->bv_value = $request->bv;
        $a->methods = $request->methods;
        $a->gateway_charges = $request->gateway_charges;
        $a->charges = $request->charges;
        $a->taxes = $request->taxes;

        $a->save();



        $b = Customer::findorfail($request->customer_id);

        $currentbalance = $b->bvwaller_balance;

        $b->bvwaller_balance = ($currentbalance + $request->bv);
        $b->save();


        return back();






    }


    public function GetDatatables($id){



        $users = \App\Customer\CustomerBVWallet::where('customer_id', $id)->select(
            ['id','txt_id',
                'amount',
                'bv_value',
                'fname',
                'lname',
                'mobile',
                'email',
                'methods',
                'gateway_charges',
                'charges',
                'created_at'])->orderBy('updated_at', 'DESC');;



        return DataTables::of($users)
            ->editColumn('created_at', function($user) {
                return Carbon::createFromFormat('Y-m-d H:i:s', $user->created_at)->toDateString();
            })

            ->editColumn('amount', function($user) {
                return '<i class="la la-inr"></i> '.$user->amount;
            })

            ->editColumn('charges', function($user) {
                return '<i class="la la-inr"></i> '.$user->charges;
            })

            ->addColumn('action', function ($user) {

                return '<a href="'.route('admin.customer.bvwallet.delete',$user->id).'" class="btn btn-danger"> Delete</a>';

            })

            ->rawColumns(['amount','charges','action'])
            ->make();





    }


    public function Delete($id){
        $a =  \App\Customer\CustomerBVWallet::findorfail($id);

        $amount = $a->bv_value;





        $b = Customer::findorfail($a->customer_id);

        $currentbalance = $b->bvwaller_balance;

        $b->bvwaller_balance = ($currentbalance-$amount);


        $b->save();

        $a->delete();



        return back();


    }

    function generateRandomString($length = 10) {
        $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}
