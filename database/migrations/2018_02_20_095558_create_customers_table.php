<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->increments('id')->unique();
            $table->string('sponser_id')->nullable();
            $table->string('fname');
            $table->string('lname');
            $table->string('ic_number');
            $table->string('g_name')->nullable();
            $table->string('dob')->nullable();
            $table->string('sex')->nullable();
            $table->string('street')->nullable();
            $table->string('locality')->nullable();
            $table->string('city')->nullable();
            $table->string('state')->nullable();
            $table->string('country')->nullable();
            $table->string('pin')->nullable();
            $table->text('permanent_address')->nullable();
            $table->text('supply_address')->nullable();
            $table->string('ref')->nullable();
            $table->string('mobile')->unique();
            $table->string('phone')->unique()->nullable();
            $table->string('email')->unique();
            $table->string('nominee_name')->nullable();
            $table->string('nominee_relation')->nullable();
            $table->string('nominee_address')->nullable();
            $table->string('nominee_dob')->nullable();
            $table->string('bank_name')->nullable();
            $table->string('bank_ifsc')->nullable();
            $table->string('bank_branch')->nullable();
            $table->string('bank_micr')->nullable();
            $table->string('bank_account_holder_name')->nullable();
            $table->string('bank_account_number')->nullable();
            $table->string('pan')->nullable();
            $table->string('aadhar')->nullable();
            $table->string('password');
            $table->string('gstin')->nullable();
            $table->string('gstin_state')->nullable();
            $table->string('billing_name')->nullable();
            $table->string('commission')->nullable();
            $table->string('waller_balance')->nullable();

            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
