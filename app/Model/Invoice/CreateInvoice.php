<?php

namespace App\model\Invoice;

use Illuminate\Database\Eloquent\Model;

class CreateInvoice extends Model
{


    public function invoice_to_products()
    {
        return $this->hasMany('App\Model\Invoice\InvoiceToProduct', 'create_orders_id', 'id');
    }


    public function invoice_to_stock_out()
    {
        return $this->hasMany('App\StockOut', 'id', 'invoice_id');
    }

    public function order_to_invoices()
    {
        return $this->hasOne('App\Model\Order\OrderToInvoice', 'create_invoice_id', 'id');
    }

}
