<?php

namespace App\Http\Controllers\admin\customer;

use App\model\Invoice\CreateInvoice;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\DataTables;

use Carbon\Carbon;

class CustomerOrders extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth:admin,storeuser,cashier']);


    }


   public function CustomerOrder($id){



       $users = CreateInvoice::where('customer_id', $id)->select(
           ['id','invoice_number','invoice_date','invoice_total_tax',
               'invoice_total','invoice_return_quater',
               'invoice_return_month','created_at'])->orderBy('updated_at', 'DESC');



       return Datatables::of($users)
           ->editColumn('created_at', function($user) {
               return Carbon::createFromFormat('Y-m-d H:i:s', $user->created_at)->toDateString();
           })



           ->editColumn('invoice_total', function($user) {
               return '<i class="la la-inr"></i> '.$user->invoice_total;
           })

           ->editColumn('invoice_total_tax', function($user) {
               return '<i class="la la-inr"></i> '.$user->invoice_total_tax;
           })
           ->addColumn('action', function ($user) {

               return '<a href="/dashboard/invoice/view/'.$user->id.'" class="btn btn-xs btn-primary"><i class="fa fa-book"></i> View Invoice</a>';
           })

           ->addColumn('tax_val', function ($user) {

                return '<i class="la la-inr"></i> '.($user->invoice_total - $user->invoice_total_tax);
           })
           ->rawColumns(['invoice_total','invoice_total_tax','action','tax_val'])
           ->make();
   }
}
