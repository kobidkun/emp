<?php

namespace App\Product;

use Illuminate\Database\Eloquent\Model;

class ProductToAtribute extends Model
{
    protected $fillable = [
        'name',
        'value',
        'product_id',
    ];




    public function productstwo()
    {
        return $this->hasOne('App\Product','id','product_id');
    }
}
