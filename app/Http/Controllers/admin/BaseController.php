<?php

namespace App\Http\Controllers\admin;

use App\Model\BVInvoice\CreateInvoiceBV;
use App\Model\BVInvoice\InvoiceToProductBV;
use App\model\Invoice\CreateInvoice;
use App\model\Invoice\InvoiceToProduct;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;


class BaseController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function dashboard(Request $request){


        $monthlysalestext = CreateInvoiceBV::sum('invoice_total');
        $monthlytax = CreateInvoiceBV::sum('invoice_total_tax');
        $monthlyqtytext = InvoiceToProductBV::sum('quantity');





        $monthlysales = DB::table('create_invoice_b_vs')
            ->select(
                DB::raw('sum(invoice_total) as total'),
                DB::raw('date(created_at) as dates')


            )

            ->groupBy('dates')
            ->orderBy('dates','asc')
            ->get();


        $dailysalesqty = DB::table('invoice_to_product_b_vs')
            ->select(
                DB::raw('sum(quantity) as total'),
                DB::raw('date(created_at) as dates')
             //   DB::raw('created_at as kkk')


            )

            ->groupBy('dates')
            ->orderBy('dates','asc')
            ->get();


        return view('admin.page.internel.dashboard')->with([
            'monthlysales' => $monthlysales,
            'monthlysalestext' => $monthlysalestext,
            'monthlyqtytext' => $monthlyqtytext,
            'monthlytax' => $monthlytax,
            'dailysalesqty' => $dailysalesqty,
        ]);
    }
}
