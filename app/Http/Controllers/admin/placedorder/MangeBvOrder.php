<?php

namespace App\Http\Controllers\admin\placedorder;

use App\Customer;
use App\Http\Controllers\admin\Misc\SMSNotify;
use App\Http\Controllers\customer\CreateOrderBV;
use App\Http\Controllers\customer\Misc\HelperNotify;
use App\Model\BVinvoice\Payment\PaymentDetails;
use App\Model\Order\BVOrder\CreateBVOrder;
use App\Model\Order\BVOrder\OrderToPickup;
use App\Notifications\Customer\MiscNotification;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


use Illuminate\Notifications\Notification;
use Yajra\DataTables\DataTables;
use PDF;
use Carbon\Carbon;


class MangeBvOrder extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth:admin,storeuser,cashier']);


    }


    public function Allorders(){


        return view('admin.page.placedorderbv.all');



    }

    public function Pickuporders(){



        $invoices = OrderToPickup::whereDate('created_at', Carbon::today())
        ->where('status','pending')

            ->select(
            [
                'id',
                'customer_id',
                'create_b_v_order_id',
                'status',
                'created_at'
            ]);



        return Datatables::of($invoices)
            ->editColumn('created_at', function($invoice) {
                return Carbon::createFromFormat('Y-m-d H:i:s', $invoice->created_at)->toDayDateTimeString();
            })

            ->editColumn('customer_id', function($invoice) {
                $cust = Customer::find($invoice->customer_id);
                return $cust->fname.' '.$cust->lname ;
            })
            ->editColumn('create_b_v_order_id', function($invoice) {
                $cust = CreateBVOrder::find($invoice->create_b_v_order_id);
                return $cust->order_secure_id ;
            })

            ->addColumn('total', function ($invoice) {

                $cust = CreateBVOrder::find($invoice->create_b_v_order_id);

                return
                  '₹ '. $cust->order_total;
            })

            ->addColumn('payment', function ($invoice) {

                $cust = CreateBVOrder::find($invoice->create_b_v_order_id);

                if ($cust === null) {
                    return $cust->create_b_v_order_to_payments->status;
                } else {
                    return null;
                }


            })




            ->addColumn('action', function ($invoice) {
                return '<a href="'.route('dashboard.pickup.details',$invoice->id).'" class=" btn btn-xs btn-primary" title="View details"><i class="la la-edit"></i>View Details</a>';
            })


            ->addColumn('delete', function ($invoice) {


                return '<a href="'.route('dashboard.pickup.bv.delete',$invoice->id).'" class=" btn btn-xs btn-danger" title="View details"><i class="la la-edit"></i>Delete</a>';
            })


            ->rawColumns(['action','customer_id','delete'])
            ->orderColumn('id', 'created_at $1')
            ->make();



    }

    public function Gotoorder($id){

        $invoices = OrderToPickup::find($id);

        $order = OrderToPickup::find($id);

        $getOrder = CreateBVOrder::where('id', $order->create_b_v_order_id)->first(); //order_secure_id

        $user = Customer::where('id',$order->customer_id)->first();

        $slug = $getOrder->order_secure_id;

        $msg = 'We have started processing your order '.$slug;



     //   (new \App\Http\Controllers\admin\Misc\DBHelperNotify())->sendDBMessage($user->id,'OrderDetails',$msg,$slug);
       $order->status = 'complete';
        $order->save();

        $user = Customer::where('id',$order->customer_id)->first();

        $msg = 'We have started processing your order';

        $slug = 'test';

        (new \App\Http\Controllers\customer\Misc\HelperNotify)->sendOneSignalMessage($user->id,$msg);
          (new \App\Http\Controllers\admin\Misc\DBHelperNotify())->sendDBMessage($user->id,'OrderDetails',$msg,$slug);




       // Notification::send( new MiscNotification());





        $cust = CreateBVOrder::find($invoices->create_b_v_order_id);

        return redirect()->route('dashboard.placed.bv.create.view',$cust->id);
    }

    public function PickupordersDelete($id){
        $order = OrderToPickup::find($id);
        $order->delete();

        $user = Customer::where('id',$order->customer_id)->first();

        $msg = 'You missed your Pickup. Try reschedule again';

        (new \App\Http\Controllers\customer\Misc\HelperNotify)->sendOneSignalMessage($user->id,$msg);


        return redirect()->back();
    }


    public function PickupordersApi(){


        return view('admin.page.placedorderbv.pickup');



    }

    public function details($id){

        $a = \App\Model\Order\BVOrder\CreateBVOrder::findorfail($id);


        return view('admin.page.placedorderbv.viewinvoice')->with(['invoice' => $a]);



    }


    public function exportPdfOrderbv(Request $request, $id)
    {
        $invoice = \App\Model\Order\BVOrder\CreateBVOrder::findorfail($id);

        $invoicepdf = PDF::loadView('admin.page.placedorderbv.generatepdffrominvoice', ['invoice' => $invoice])->setPaper('a4');

        return $invoicepdf->download('invoice' . $invoice->order_secure_id . '-' . $invoice->customer->fname . '.pdf');


    }

    public function exportPrintOrderbv(Request $request, $id)
    {
        $invoice = \App\Model\Order\BVOrder\CreateBVOrder::findorfail($id);

        $invoicepdf = PDF::loadView('admin.page.placedorderbv.generatepdffrominvoice', ['invoice' => $invoice])->setPaper('a4');

        return $invoicepdf->stream('invoice' . $id . ' ' . $invoice->customer->fname . '.pdf', array("Attachment" => false));


    }


    public function Datatable(){


        $invoices = \App\Model\Order\BVOrder\CreateBVOrder::select(
            [
                'id',
                'order_secure_id',
                'customer_gstin',
                'customer_id',
                'order_total_taxable_amount',
                'order_total_tax_cgst',
                'order_total_tax_sgst',
                'order_total_tax',
                'order_total',
                'payment_type',
                'bookedfrom',
                'created_at',
            ])->orderBy('updated_at', 'DESC');



        return Datatables::of($invoices)
            ->editColumn('created_at', function($invoice) {
                return Carbon::createFromFormat('Y-m-d H:i:s', $invoice->created_at)->toDayDateTimeString();
            })

            ->addColumn('customer_id', function ($invoice) {

                $a = Customer::findorfail($invoice->customer_id);

                return
                    $a->fname;
            })




            ->addColumn('action', function ($invoice) {
                return '<a href="'.route('dashboard.placed.bv.create.view',$invoice->id).'" class=" btn btn-xs btn-primary" title="View details"><i class="la la-edit"></i>View Details</a>';
            })


            ->rawColumns(['action','customer_id'])
            ->orderColumn('id', 'created_at $1')
            ->make();



    }

    public function sendLinklfromadmin($order,$getcustomerid) {

        $getcustomer = Customer::find($getcustomerid);

        $slug = $this->random_str();
        $createorder = CreateBVOrder::where('order_secure_id', $order)->first();
        //  dd($createorder);


        $appid = env('TestAppid');
        $appsecret = env('TestSecretKey');
        $orderid = $slug;
        $orderamt = $createorder->order_total;
        $orderdesc = 'buy for order ' . $createorder->id;
        $customername = $getcustomer->fname . ' ' . $getcustomer->lname;
        $customerphone = $getcustomer->phone;
        $custoomeremail = $getcustomer->email;
        $returnurl = env('WEB_URL') . '/api/openurl/payorder/status/complete';
        $notifurl = env('WEB_URL') . '/api/openurl/payorder/status/complete';

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, 'https://test.cashfree.com/api/v1/order/create');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "appId=" . $appid . "&secretKey=" . $appsecret . "&orderId="
            . $orderid . "&orderAmount=" . $orderamt . "&orderNote=" . $orderdesc . "&customerName=" . $customername .
            "&customerPhone=" . $customerphone . "&customerEmail=" . $custoomeremail .
            "&sellerPhone=&returnUrl=" . $returnurl . "&notifyUrl=" . $notifurl . "&paymentModes=&pc=");

        $headers = array();
        $headers[] = 'Cache-Control: no-cache';
        $headers[] = 'Content-Type: application/x-www-form-urlencoded';
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        curl_close($ch);

        $gatestatus = json_decode($result);
        /*  return response()->json([
              $gatestatus
          ],200);*/

        //if ($gatestatus->)

        if ($gatestatus->status === "OK") {
            $createpay = new PaymentDetails();
            $createpay->create_order_id = $createorder->id;
            $createpay->customer_id = $getcustomer->id;
            $createpay->gateway_id = $slug;
            $createpay->slug = $slug;
            $createpay->url = env('WEB_URL') . '/openurl/payorder/' . $slug;
            $createpay->amount = $createorder->order_total;
            $createpay->email = $getcustomer->email;
            $createpay->purpose = 'buy for order ' . $createorder->id;
            $createpay->inidump = $result;
            $createpay->phone = $getcustomer->phone;
            $createpay->buyer_name = $getcustomer->fname . ' ' . $getcustomer->lname;
            $createpay->status = 'pending';
            $createpay->save();

            $msg = 'Hi '.$getcustomer->fname.' Please make a payment for order '.$orderid.' of Amount Rs '.$createpay->amount.' '.$createpay->url.'/web';

           (new \App\Http\Controllers\admin\Misc\SMSNotify())->SendSMS($getcustomer->mobile,$msg);
           (new \App\Http\Controllers\admin\Misc\HelperNotifyAdmin())->sendOneSignalMessage($getcustomer->id,$msg,$createpay->url.'/web');




            return back();
        } else {
            return back();
        }

    }

    function random_str($type = 'alphanum', $length = 10)
    {
        switch ($type) {
            case 'basic'    :
                return mt_rand();
                break;
            case 'alpha'    :
            case 'alphanum' :
            case 'num'      :
            case 'nozero'   :
                $seedings = array();
                $seedings['alpha'] = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                $seedings['alphanum'] = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                $seedings['num'] = '0123456789';
                $seedings['nozero'] = '123456789';

                $pool = $seedings[$type];

                $str = '';
                for ($i = 0; $i < $length; $i++) {
                    $str .= substr($pool, mt_rand(0, strlen($pool) - 1), 1);
                }
                return $str;
                break;
            case 'unique'   :
            case 'md5'      :
                return md5(uniqid(mt_rand()));
                break;
        }
    }
}
