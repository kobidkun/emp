<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductAtributesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_atributes', function (Blueprint $table) {
            $table->increments('id');
            $table->text('name')->nullable();
            $table->string('type')->nullable();
            $table->string('default_value')->nullable();
            $table->string('is_filterable')->nullable();
            $table->string('is_searchable')->nullable();
            $table->string('is_swatch')->nullable();
            $table->text('description')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_atributes');
    }
}
