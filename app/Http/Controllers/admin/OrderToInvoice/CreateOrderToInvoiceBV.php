<?php

namespace App\Http\Controllers\admin\OrderToInvoice;

use App\Customer;
use App\Model\BVInvoice\CreateInvoiceBV;
use App\Model\BVinvoice\Payment\PaymentDetails;
use App\Model\Order\BVOrder\CreateBVOrder;
use App\Model\Order\BVOrder\CreateBVOrderToInvoice;
use App\Model\Order\BVOrder\CreateBVOrderToProduct;
use App\ProductVatient;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Uuid;

class CreateOrderToInvoiceBV extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth:admin,storeuser,cashier']);


    }


    public function viewinvconverson($id){



        $getOrder = CreateBVOrder::findorfail($id);
        $customer = Customer::findorfail($getOrder->customer_id);

        $payment = $getOrder->create_b_v_order_to_payments;

        // dd($customer);

        // 0 = oline
        // 1 = cash

        //dd($payment);

        return view('admin.page.convertorder.orderbv.create')->with(['o'=> $getOrder, 'c'=>$customer,'p'=>$payment]);
    }


    public function ConvertOrderToInvoice($id, Request $request){



        $getOrder = CreateBVOrder::findorfail($id);
        $cust = Customer::find($getOrder->customer_id);

        $payment = $getOrder->create_b_v_order_to_payments;

       $pay =  $request->payment;

        if ($pay === '0'){

            // cash

             $uuid = Uuid::generate();


            $createpay = new PaymentDetails();
            $createpay->create_order_id = $getOrder->id;
            $createpay->customer_id = $cust->id;
            $createpay->gateway_id = $pay;
            $createpay->slug = $uuid;
            $createpay->url = null;
            $createpay->amount = $getOrder->order_total;
            $createpay->email = $cust->email;
            $createpay->purpose = 'buy for order ' . $getOrder->id;
            $createpay->inidump = null;
            $createpay->phone = $cust->phone;
            $createpay->buyer_name = $cust->fname . ' ' . $cust->lname;
            $createpay->status = 'SUCCESS';
            $createpay->type = 'Cash txt id ';
            $createpay->save();

        } elseif ($request->payment === '2'){
            $uuid = Uuid::generate();


            $createpay = new PaymentDetails();
            $createpay->create_order_id = $getOrder->id;
            $createpay->customer_id = $cust->id;
            $createpay->gateway_id = $pay;
            $createpay->slug = $uuid;
            $createpay->url = null;
            $createpay->amount = $getOrder->order_total;
            $createpay->email = $cust->email;
            $createpay->purpose = 'buy for order ' . $getOrder->id;
            $createpay->inidump = null;
            $createpay->phone = $cust->phone;
            $createpay->buyer_name = $cust->fname . ' ' . $cust->lname;
            $createpay->status = 'SUCCESS';
            $createpay->type = 'In Store Card';
            $createpay->save();
        }

        elseif ($request->payment === '3'){
            $uuid = Uuid::generate();


            $createpay = new PaymentDetails();
            $createpay->create_order_id = $getOrder->id;
            $createpay->customer_id = $cust->id;
            $createpay->gateway_id = $pay;
            $createpay->slug = $uuid;
            $createpay->url = null;
            $createpay->amount = $getOrder->order_total;
            $createpay->email = $cust->email;
            $createpay->purpose = 'buy for order ' . $getOrder->id;
            $createpay->inidump = null;
            $createpay->phone = $cust->phone;
            $createpay->buyer_name = $cust->fname . ' ' . $cust->lname;
            $createpay->status = 'SUCCESS';
            $createpay->type = 'cheque'.$request->txtid;
            $createpay->save();
        }


       // dd($pay);






        $getLastinvoice_number_generated  =   CreateInvoiceBV::orderBy('invoice_number_generated', 'desc')
            ->first();

        $sno = $getLastinvoice_number_generated->invoice_number_generated;

        $generateSno = ($sno+1);



        if ($generateSno  === $sno + 1) {
            $products = CreateBVOrderToProduct::where('create_orders_id', $getOrder->id)->get();


            foreach ($products as $key => $value) {
                $product_id = $value['product_id'];
                $qty = $value['quantity'];

                $findproduct = ProductVatient::findorfail($product_id);

                if ($qty > $findproduct){
                    //   return response()->json('Sorry Quality not available in stock');
                    return 'Sorry Quality not available in stock';
                } else{
                    $findproduct->stock = ($findproduct->stock - $qty );
                    $findproduct->save();



                }



            }


            $createorder = new CreateInvoiceBV();
            $createsecureorderid = ('OD'.$this->random_str('alphanum', 30));
            //invoice request
            $invoicenumber = ('EMP-'.$this->generateRandomString());

            $createorder->invoice_number_generated = $generateSno;


            $customer = Customer::findorfail($getOrder->customer_id);

            $createorder->customer_id = $getOrder->customer_id;
            $createorder->invoice_secure_id = $createsecureorderid;
            $createorder->invoice_number = $invoicenumber;
            //request


            $createorder->remark = $request->remark;

            $createorder->invoice_date = $request->invoice_date;
            $createorder->invoice_due_date = $request->invoice_due_date;
            $createorder->invoice_return_month = $request->invoice_return_month;

            $createorder->invoice_return_quater = $request->invoice_return_quater;

            $createorder->invoice_shipping_address_street = $request->invoice_shipping_address_street;
            $createorder->invoice_shipping_address_locality = $request->invoice_shipping_address_locality;
            $createorder->invoice_shipping_address_city = $request->invoice_shipping_address_city;
            $createorder->invoice_shipping_address_state = $request->invoice_shipping_address_state;
            $createorder->invoice_shipping_address_country = $request->invoice_shipping_address_country;
            $createorder->invoice_shipping_address_pin = $request->invoice_shipping_address_pin;

            //request

            $createorder->invoice_customer_name = $customer->fname.' '. $customer->lname;
            $createorder->invoice_customer_gstin = $customer->gstin;
            $createorder->invoice_place_of_supply = $customer->gstin_state;



            $createorder->invoice_billing_address_street = $customer->street;
            $createorder->invoice_billing_address_locality = $customer->locality;
            $createorder->invoice_billing_address_city = $customer->city;
            $createorder->invoice_billing_address_state = $customer->state;
            $createorder->invoice_billing_address_country = $customer->country;
            $createorder->invoice_billing_address_pin = $customer->pin;








            //auto generated



            $createorder->invoice_total_taxable_amount = round($getOrder->order_total_taxable_amount,2);
            $createorder->invoice_total_taxable_discount_amount = round($getOrder->order_total_taxable_discount_amount,2);
            $createorder->invoice_total_tax_cgst = round($getOrder->order_total_tax_cgst,2);
            $createorder->invoice_total_tax_sgst =round( $getOrder->order_total_tax_sgst,2);
            $createorder->invoice_total_tax_igst = round($getOrder->order_total_tax_cgst + $getOrder->order_total_tax_sgst,2);
            $createorder->invoice_total_tax_cess = round($getOrder->order_total_tax_cess,2);
            $createorder->invoice_total_tax = round($getOrder->order_total_tax,2);
            $createorder->invoice_total_adjustment = '0';
            $createorder->invoice_total_taxable_amt = round($getOrder->order_total_taxable_amount,2);
            $createorder->invoice_total_discount_amt = '0';
            $createorder->invoice_total = round($getOrder->order_total,2);


            $createorder->invoice_total_tax_value = round(($getOrder->order_total - $getOrder->order_total_tax),2);
            $createorder->total_bv = $getOrder->order_total_bv;




            $createorder->save();



            // create invoice to products


            $invoice_id = $createorder->id;
            $invoice_number = $createorder->invoice_number;
            $customerid = $createorder->customer_id;



            foreach ($products as $key => $value) {


                $product_id_get = $value->product_id;

                //   dd($product_id_get);


                $getProduct = ProductVatient::findorfail($product_id_get);


                $quantity = $value->quantity;


                $taxablecgst = round(($value->taxable_tax_cgst_percentage / 100) * ($value->taxable_rate * $quantity),2);
                $taxablesgst = round(($value->taxable_tax_sgst_percentage / 100) * ($value->taxable_rate * $quantity),2);
                $taxableigst = round($taxablecgst+ $taxablesgst,2) ;




                $createorder->invoice_to_products()->create([
                    'create_invoices_id' => $invoice_id,
                    'customer_id' => $customerid,
                    'product_id' => $getProduct->id,
                    'parent_product_id' => $getProduct->product_id,
                    'invoice_id' => $invoice_number,
                    'name' => $value->product_name .' Color:' .$value->color.' Size:'.$value->size,
                    'color' => $value->color,
                    'size' => $value->size,
                    'sno' => $key++ +1 ,
                    'unit' => $getProduct->unit,
                    'hsn' => $getProduct->hsn,
                    'type' => $getProduct->type,
                    'quantity' => $quantity,
                    'rate' => $value->taxable_rate,
                    'discount_amount' => '0',
                    'discount_percentage' => '0',
                    'taxable_rate' => $value->taxable_rate,
                    'taxable_amount' => round($value->taxable_rate * $quantity,2),
                    'tax_value' => round($taxableigst,2),
                    'cgst' => round($taxablecgst,2),
                    'sgst' => round($taxablesgst,2),
                    'igst' =>$taxableigst + $taxablesgst,
                    'sgst_percentage' => $value->taxable_tax_cgst_percentage,
                    'cgst_percentage' => $value->taxable_tax_sgst_percentage,
                    'igst_percentage' => $value->taxable_tax_igst_percentage,
                    'total' => round(($value->taxable_rate * $quantity) + ($taxablecgst + $taxablesgst),2),


                ]);
            }



            foreach ($products as $key => $value) {

                $createorder->invoice_to_stock_out()->create([




                    'customer_id' => $customerid,
                    'product_variant_id' => $value['product_id'],
                    'stock_out_quantity' => $value['quantity'],
                    'product_id' => $value['parent_product_id'],
                    'invoice_id' => $invoice_id,



                ]);


            }


            $g = new CreateBVOrderToInvoice();
            $g->create_invoice_b_v_id = $invoice_id;
            $g->create_b_v_order_id = $id;
            $g->save();

            $getOrder->create_invoice_b_v_id = $invoice_id;
            $getOrder->status = true;
            $getOrder->save();

            $createpay->createinvoicebvs_id = $invoice_id;
            $createpay->medium = $request->payment;
            $createpay->save();




            $msg = 'Your Order is Complete. Thank you for shopping with us.';

            (new \App\Http\Controllers\customer\Misc\HelperNotify)->sendOneSignalMessage($customer->id,$msg);




            //  }


            return redirect(route('dashboard.bvinvoice.view.single',$invoice_id));
        }

        elseif($generateSno  === $sno) {



            $generateSno = $generateSno+1;


            $products = CreateBVOrderToProduct::where('create_orders_id', $getOrder->id)->get();


            foreach ($products as $key => $value) {
                $product_id = $value['product_id'];
                $qty = $value['quantity'];

                $findproduct = ProductVatient::findorfail($product_id);

                if ($qty > $findproduct){
                    //   return response()->json('Sorry Quality not available in stock');
                    return 'Sorry Quality not available in stock';
                } else{
                    $findproduct->stock = ($findproduct->stock - $qty );
                    $findproduct->save();



                }



            }


            $createorder = new CreateInvoiceBV();
            $createsecureorderid = ('OD'.$this->random_str('alphanum', 30));
            //invoice request
            $invoicenumber = ('EMP-'.$this->generateRandomString());

            $createorder->invoice_number_generated = $generateSno;


            $customer = Customer::findorfail($getOrder->customer_id);

            $createorder->customer_id = $getOrder->customer_id;
            $createorder->invoice_secure_id = $createsecureorderid;
            $createorder->invoice_number = $invoicenumber;
            //request


            $createorder->remark = $request->remark;

            $createorder->invoice_date = $request->invoice_date;
            $createorder->invoice_due_date = $request->invoice_due_date;
            $createorder->invoice_return_month = $request->invoice_return_month;

            $createorder->invoice_return_quater = $request->invoice_return_quater;

            $createorder->invoice_shipping_address_street = $request->invoice_shipping_address_street;
            $createorder->invoice_shipping_address_locality = $request->invoice_shipping_address_locality;
            $createorder->invoice_shipping_address_city = $request->invoice_shipping_address_city;
            $createorder->invoice_shipping_address_state = $request->invoice_shipping_address_state;
            $createorder->invoice_shipping_address_country = $request->invoice_shipping_address_country;
            $createorder->invoice_shipping_address_pin = $request->invoice_shipping_address_pin;

            //request

            $createorder->invoice_customer_name = $customer->fname.' '. $customer->lname;
            $createorder->invoice_customer_gstin = $customer->gstin;
            $createorder->invoice_place_of_supply = $customer->gstin_state;



            $createorder->invoice_billing_address_street = $customer->street;
            $createorder->invoice_billing_address_locality = $customer->locality;
            $createorder->invoice_billing_address_city = $customer->city;
            $createorder->invoice_billing_address_state = $customer->state;
            $createorder->invoice_billing_address_country = $customer->country;
            $createorder->invoice_billing_address_pin = $customer->pin;








            //auto generated



            $createorder->invoice_total_taxable_amount = round($getOrder->order_total_taxable_amount,2);
            $createorder->invoice_total_taxable_discount_amount = round($getOrder->order_total_taxable_discount_amount,2);
            $createorder->invoice_total_tax_cgst = round($getOrder->order_total_tax_cgst,2);
            $createorder->invoice_total_tax_sgst =round( $getOrder->order_total_tax_sgst,2);
            $createorder->invoice_total_tax_igst = round($getOrder->order_total_tax_cgst + $getOrder->order_total_tax_sgst,2);
            $createorder->invoice_total_tax_cess = round($getOrder->order_total_tax_cess,2);
            $createorder->invoice_total_tax = round($getOrder->order_total_tax,2);
            $createorder->invoice_total_adjustment = '0';
            $createorder->invoice_total_taxable_amt = round($getOrder->order_total_taxable_amount,2);
            $createorder->invoice_total_discount_amt = '0';
            $createorder->invoice_total = round($getOrder->order_total,2);


            $createorder->invoice_total_tax_value = round(($getOrder->order_total - $getOrder->order_total_tax),2);
            $createorder->total_bv = $getOrder->order_total_bv;




            $createorder->save();



            // create invoice to products


            $invoice_id = $createorder->id;
            $invoice_number = $createorder->invoice_number;
            $customerid = $createorder->customer_id;



            foreach ($products as $key => $value) {


                $product_id_get = $value->product_id;

                //   dd($product_id_get);


                $getProduct = ProductVatient::findorfail($product_id_get);


                $quantity = $value->quantity;


                $taxablecgst = round(($value->taxable_tax_cgst_percentage / 100) * ($value->taxable_rate * $quantity),2);
                $taxablesgst = round(($value->taxable_tax_sgst_percentage / 100) * ($value->taxable_rate * $quantity),2);
                $taxableigst = round($taxablecgst+ $taxablesgst,2) ;




                $createorder->invoice_to_products()->create([
                    'create_invoices_id' => $invoice_id,
                    'customer_id' => $customerid,
                    'product_id' => $getProduct->id,
                    'parent_product_id' => $getProduct->product_id,
                    'invoice_id' => $invoice_number,
                    'name' => $value->product_name .' Color:' .$value->color.' Size:'.$value->size,
                    'color' => $value->color,
                    'size' => $value->size,
                    'sno' => $key++ +1 ,
                    'unit' => $getProduct->unit,
                    'hsn' => $getProduct->hsn,
                    'type' => $getProduct->type,
                    'quantity' => $quantity,
                    'rate' => $value->taxable_rate,
                    'discount_amount' => '0',
                    'discount_percentage' => '0',
                    'taxable_rate' => $value->taxable_rate,
                    'taxable_amount' => round($value->taxable_rate * $quantity,2),
                    'tax_value' => round($taxableigst,2),
                    'cgst' => round($taxablecgst,2),
                    'sgst' => round($taxablesgst,2),
                    'igst' =>$taxableigst + $taxablesgst,
                    'sgst_percentage' => $value->taxable_tax_cgst_percentage,
                    'cgst_percentage' => $value->taxable_tax_sgst_percentage,
                    'igst_percentage' => $value->taxable_tax_igst_percentage,
                    'total' => round(($value->taxable_rate * $quantity) + ($taxablecgst + $taxablesgst),2),


                ]);
            }



            foreach ($products as $key => $value) {

                $createorder->invoice_to_stock_out()->create([




                    'customer_id' => $customerid,
                    'product_variant_id' => $value['product_id'],
                    'stock_out_quantity' => $value['quantity'],
                    'product_id' => $value['parent_product_id'],
                    'invoice_id' => $invoice_id,



                ]);


            }


            $g = new CreateBVOrderToInvoice();
            $g->create_invoice_b_v_id = $invoice_id;
            $g->create_b_v_order_id = $id;
            $g->save();








            // send notify to next user



          return redirect(route('dashboard.bvinvoice.view.single',$invoice_id));
        } else{
            return 'Some Error Occured';
        }

















    }

    function sendMessage($player_id){
        $content = array(
            "en" => 'Welcome to Emporium Marketing. You will receive important messages & updates from now'
        );

        $fields = array(
            'app_id' => env('one_app_id'),
            'include_player_ids' => array($player_id),
            'data' => array("foo" => "bar"),
            'contents' => $content
        );

        $fields = json_encode($fields);
       // print("\nJSON sent:\n");
      //  print($fields);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        $response = curl_exec($ch);
        curl_close($ch);

        return $response;
    }




    function generateRandomString($length = 10) {
        $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    function random_str($type = 'alphanum', $length = 20)
    {
        switch($type)
        {
            case 'basic'    : return mt_rand();
                break;
            case 'alpha'    :
            case 'alphanum' :
            case 'num'      :
            case 'nozero'   :
                $seedings             = array();
                $seedings['alpha']    = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                $seedings['alphanum'] = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                $seedings['num']      = '0123456789';
                $seedings['nozero']   = '123456789';

                $pool = $seedings[$type];

                $str = '';
                for ($i=0; $i < $length; $i++)
                {
                    $str .= substr($pool, mt_rand(0, strlen($pool) -1), 1);
                }
                return $str;
                break;
            case 'unique'   :
            case 'md5'      :
                return md5(uniqid(mt_rand()));
                break;
        }
    }
}
