<?php

namespace App\Http\Controllers\export;

use App\Excel\CustomCustomerExport;
use App\Excel\CustomProductExport;
use App\Excel\CustomStockExport;
use App\Excel\StockbyProductExport;
use App\ProductVatient;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
class ExportExcel extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth:admin,storeuser,cashier']);


    }


    public function exportCustomProductsviewexcelview(Request $request)
    {

        return view('export.excel.saleproduct');

    }



    public function exportCustomProductsviewexcel(Request $request)
    {

        $from = Carbon::parse($request->from);
        $to = Carbon::parse($request->to);

        return (new CustomProductExport())->forYear($from, $to)->download($from.'-'.$to.'Products.xlsx');

    }



    public function exportCustomCustomerviewexcelview(Request $request)
    {

        return view('export.excel.salecustomer');

    }



    public function exportCustomCustomerviewexcel(Request $request)
    {

        $from = Carbon::parse($request->from);
        $to = Carbon::parse($request->to);

        $customer = $request->customer_id;

        return (new CustomCustomerExport())->forYear($from, $to, $customer)->download($from.'-'.$to.'Products.xlsx');

    }




    public function exportstockproductvariantsviewexcelview(Request $request)
    {

        return view('export.excel.stockproductvariants');

    }



    public function exportstockproductvariantsexcel(Request $request)
    {

        $from = Carbon::parse($request->from);
        $to = Carbon::parse($request->to);

    //    $customer = $request->customer_id;

        return (new CustomStockExport())->forYear($from, $to)->download($from.'-'.$to.'Products.xlsx');

    }

    public function StockInbyProduct($id, $request){

       $a = ProductVatient::find($id)->first();

        $from = Carbon::parse($request->from);
        $to = Carbon::parse($request->to);


        }


        public function Stockinout(){



            return (new StockbyProductExport())->download(time().'Products.xlsx');


        }
}
