@extends('admin.index');

@section('content')

    <div class="m-content">
        <div class="row">
            <div class="col-md-6">
                <!--begin::Portlet-->
                <div class="m-portlet m-portlet--tab">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
												<span class="m-portlet__head-icon m--hide">
													<i class="la la-gear"></i>
												</span>
                                <h3 class="m-portlet__head-text">
                                   Monthly Calculation
                                </h3>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <!--begin::Section-->
                        <div class="m-section">


                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif





                                <div class="m-portlet__body">

                                    <div class="form-group m-input-group--air">
                                        <label for="exampleInputEmail1">
                                            Return Month
                                        </label>
                                        <div class="input-group m-input-group">
                                            <div class="input-group-prepend">
                                                         <span class="input-group-text">
                                                          <i class="la la-calendar"></i>
                                                         </span>
                                            </div>
                                            <input type="text"
                                                   id="m_datepicker_return_month"
                                                   class="form-control m-input--air invoice_return_month"
                                                   placeholder="Return Month"
                                                   value="{{ date('m-Y') }}"
                                                   name="invoice_return_month"
                                                   autocomplete="off"
                                                   aria-describedby="basic-addon1">


                                        </div>


                                    </div>



                                </div>






                                <div class="m-portlet__foot m-portlet__foot--fit">
                                    <div class="m-form__actions">
                                        <button id="demo8" class="btn btn-primary">
                                            Submit
                                        </button>

                                    </div>
                                </div>

                        </div>
                        <!--end::Section-->

                    </div>
                </div>
                <!--end::Portlet-->
                <!--begin::Portlet-->

            </div>
        </div>
    </div>


    <div id="domMessage" style="display:none;">
        <h3>We are calculating Monthly Profits.This might Take upto 10 minutes.Please be Patience. Do not Close this Page</h3>

        <div class="kt-spinner kt-spinner--lg kt-spinner--warning"></div>

    </div>


@endsection



@section('admin_footer_script')
    <script src='https://malsup.github.io/jquery.blockUI.js'></script>

    <script>
        //== Class definition

        var BootstrapDatepicker = function () {

            //== Private functions
            var demos = function () {
                // minimum setup
                $('#m_datepicker_1, #m_datepicker_1_validate').datepicker({
                    format: "dd-mm-yyyy",
                    todayHighlight: true,
                    orientation: "bottom left",
                    templates: {
                        leftArrow: '<i class="la la-angle-left"></i>',
                        rightArrow: '<i class="la la-angle-right"></i>'
                    }
                });

                $('#m_datepicker_return_month').datepicker({
                    format: "mm-yyyy",
                    viewMode: "months",
                    minViewMode: "months",
                    orientation: "bottom left",
                    templates: {
                        leftArrow: '<i class="la la-angle-left"></i>',
                        rightArrow: '<i class="la la-angle-right"></i>'
                    }
                });

                // minimum setup for modal demo
            }

            return {
                // public functions
                init: function() {
                    demos();
                }
            };
        }();

        jQuery(document).ready(function() {
            BootstrapDatepicker.init();
        });
    </script>




    <script type="text/javascript">
        $(document).ready(function() {
            $('#demo8').click(function() {
                $.blockUI({ message: $('#domMessage') });

              //  $.blockUI();

                setTimeout(function() {
                    $.unblockUI({
                        onUnblock: function(){  swal("Sorry", 'Your Data is mis matching please check BV invoices', "error");; }
                    });
                }, 25000);
            });
        });

    </script>


@endsection