
@extends('admin.index');

@section('content')

    <div class="m-content">
        <div class="row">
            <div class="col-md-6">
                <!--begin::Portlet-->
                <div class="m-portlet m-portlet--tab">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
												<span class="m-portlet__head-icon m--hide">
													<i class="la la-gear"></i>
												</span>
                                <h3 class="m-portlet__head-text">
                                    Create Primary Category
                                </h3>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <!--begin::Section-->
                        <div class="m-section">


                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif




                            <form class="m-form m-form--fit m-form--label-align-right"
                                  role="form" method="post"
                                  action="{{ route('admin.variant.save') }}"
                            >
                                {{ csrf_field() }}
                                <div class="m-portlet__body">

                                    <div class="form-group m-input-group--air">
                                        <label for="exampleInputEmail1">
                                            Variant
                                        </label>
                                        <div class="input-group m-input-group">

                                            <select name="name" class="form-control" id="">
                                                <option value="size">Size</option>
                                                <option value="color">Color</option>
                                            </select>
                                        </div>
                                        <span class="m-form__help">
													Do not Create Same Variant Twice
												</span>
                                    </div>
                                    <div class="form-group m-input-group--air">
                                        <label for="exampleInputEmail1">
                                            Value
                                        </label>
                                        <div class="input-group m-input-group">

                                            <input type="text"

                                                   class="form-control m-input--air category-slug"
                                                   placeholder="XL, Green etc"
                                                   name="value"
                                                   autocomplete="off"
                                                   aria-describedby="basic-addon1">
                                        </div>

                                    </div>


                                </div>






                                <div class="m-portlet__foot m-portlet__foot--fit">
                                    <div class="m-form__actions">
                                        <button type="submit" class="btn btn-primary">
                                            Submit
                                        </button>
                                        <button type="reset" class="btn btn-secondary">
                                            Cancel
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <!--end::Section-->

                    </div>
                </div>
                <!--end::Portlet-->
                <!--begin::Portlet-->

            </div>
        </div>
    </div>

@endsection



@section('admin_footer_script')
    <script src='{{asset('/production/js/admin/tinymce/tinymce.min.js')}}'></script>
    <script>
        tinymce.init({
            selector: '#mytextarea'
        });
    </script>
    <script>



        $(document).ready(function(){
            $(".category-name").keyup(function(){

                var cat_name_val = $( this ).val();
                var actualSlug = cat_name_val.replace(/ /g,'-').toLowerCase()

                $(".category-slug").inputVal(actualSlug);



            });
        });


    </script>

@endsection








