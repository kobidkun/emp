<?php

namespace App\Http\Controllers\admin\bvinvoice;

use App\Customer;

use App\Excel\CustomInvoicesExport;
use App\Excel\CustomProductExport;
use App\Excel\InvoicesExport;
use App\Model\BVInvoice\CreateInvoiceBV;
use App\Model\BVInvoice\InvoiceToProductBV;


use App\ProductVatient;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Excel;
use Yajra\DataTables\DataTables;
use PDF;
use Carbon\Carbon;

class ManageBVinvoice extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth:admin,storeuser,cashier']);


    }

    public function CreateInvoiceView(){
        return view('admin.page.bvInvoice.create');
    }

    public function CreateInvoiceSave(Request $request){

        // $getcustomer = $request->user();

        //Create Order


        foreach ($request->input('products') as $key => $value) {
            $product_id = $value['product_id'];
            $qty = $value['quantity'];

            $findproduct = ProductVatient::findorfail($product_id);

            if ($qty > $findproduct){
                //   return response()->json('Sorry Quality not available in stock');
                return 'Sorry Quality not available in stock';
            } else{
                $findproduct->stock = ($findproduct->stock - $qty );
                $findproduct->save();



            }



        }

        //Create Order end

// generate invoice number

        $getLastinvoice_number_generated  =   CreateInvoiceBV::orderBy('invoice_number_generated', 'desc')
            ->first();


        $sno = $getLastinvoice_number_generated->invoice_number_generated;

        $generateSno = ($sno+1);








// generate invoice number end





        $createorder = new CreateInvoiceBV();
        $createsecureorderid = ('OD'.$this->random_str('alphanum', 30));
        //invoice request
        $invoicenumber = ('EMP-'.$this->generateRandomString());

        $createorder->invoice_number_generated = $generateSno;


        $createorder->customer_id = $request->customer_id;
        $createorder->remark = $request->remark;
        $createorder->typ = $request->typ;
        $createorder->invoice_secure_id = $createsecureorderid;
        $createorder->invoice_number = $invoicenumber;
        $createorder->invoice_date = $request->invoice_date;
        $createorder->invoice_due_date = $request->invoice_due_date;
        $createorder->invoice_return_month = $request->invoice_return_month;
        $createorder->invoice_customer_name = $request->invoice_customer_name;
        $createorder->invoice_customer_gstin = $request->invoice_customer_gstin;
        $createorder->invoice_place_of_supply = $request->invoice_place_of_supply;
        $createorder->invoice_return_quater = $request->invoice_return_quater;
        $createorder->invoice_billing_address_street = $request->invoice_billing_address_street;
        $createorder->invoice_billing_address_locality = $request->invoice_billing_address_locality;
        $createorder->invoice_billing_address_city = $request->invoice_billing_address_city;
        $createorder->invoice_billing_address_state = $request->invoice_billing_address_state;
        $createorder->invoice_billing_address_country = $request->invoice_billing_address_country;
        $createorder->invoice_billing_address_pin = $request->invoice_billing_address_pin;
        $createorder->invoice_shipping_address_street = $request->invoice_shipping_address_street;
        $createorder->invoice_shipping_address_locality = $request->invoice_shipping_address_locality;
        $createorder->invoice_shipping_address_city = $request->invoice_shipping_address_city;
        $createorder->invoice_shipping_address_state = $request->invoice_shipping_address_state;
        $createorder->invoice_shipping_address_country = $request->invoice_shipping_address_country;
        $createorder->invoice_shipping_address_pin = $request->invoice_shipping_address_pin;
        $createorder->invoice_total_taxable_amount = $request->invoice_total_taxable_amount;
        $createorder->invoice_total_taxable_discount_amount = $request->invoice_total_taxable_discount_amount;
        $createorder->invoice_total_tax_cgst = $request->invoice_total_tax_cgst;
        $createorder->invoice_total_tax_sgst = $request->invoice_total_tax_sgst;
        $createorder->invoice_total_tax_igst = $request->invoice_total_tax_igst;
        $createorder->invoice_total_tax_cess = $request->invoice_return_quater;
        $createorder->invoice_total_tax = $request->invoice_total_tax;
        $createorder->invoice_total_adjustment = $request->invoice_total_adjustment;
        $createorder->invoice_total_taxable_amt = $request->invoice_total_taxable_amt;
        $createorder->invoice_total_discount_amt = $request->invoice_total_discount_amt;
        $createorder->invoice_total = $request->invoice_total;
        $createorder->total_bv = $request->total_bv;


        $createorder->invoice_total_tax_value = ($request->invoice_total -$request->invoice_total_tax);




        $createorder->save();


        $invoice_id = $createorder->id;
        $invoice_number = $createorder->invoice_number;
        $customerid = $createorder->customer_id;

        $products = $request->input('products');

        foreach ($products as $key => $value) {
            $a =   $createorder->invoice_to_products()->create([
                'create_invoices_id' => $invoice_id,
                'customer_id' => $customerid,
                'product_id' => $value['product_id'],
                'parent_product_id' => $value['parent_product_id'],
                'invoice_id' => $invoice_number,
                'name' => $value['name'],
                'color' => $value['color'],
                'size' => $value['size'],
                'sno' => $value['sno'],
                'unit' => $value['unit'],
                'hsn' => $value['hsn'],
                'type' => $value['type'],
                'quantity' => $value['quantity'],
                'rate' => $value['rate'],
                'discount_amount' => $value['discount_amount'],
                'discount_percentage' => $value['discount_percentage'],
                'taxable_rate' => $value['taxable_rate'],
                'taxable_amount' => $value['taxable_amount'],
                'tax_value' => $value['tax_value'],
                'cgst' => $value['cgst'],
                'sgst' => $value['sgst'],
                'igst' => $value['igst'],
                'sgst_percentage' => $value['sgst_percentage'],
                'cgst_percentage' => $value['cgst_percentage'],
                'igst_percentage' => $value['igst_percentage'],
                'total' => $value['total'],


            ]);

        }









        foreach ($products as $key => $value) {

            $createorder->invoice_to_stock_out()->create([




                'customer_id' => $customerid,
                'product_variant_id' => $value['product_id'],
                'stock_out_quantity' => $value['quantity'],
                'product_id' => $value['parent_product_id'],
                'invoice_id' => $invoice_id,



            ]);


        }





        /*Send SMS using PHP*/
        $customerDetails = Customer::findorfail($request->customer_id);
        //Your authentication key
        $authKey = "143565AIE7jf1Mb5abd98a0";

        //Multiple mobiles numbers separated by comma
        $mobileNumber = $customerDetails->mobile;

        //Sender ID,While using route4 sender id should be 6 characters long.
        $senderId = "EMPMKT";

        //Your message to send, Add URL encoding here.
        $message = urlencode("Thanks for Shopping with Emporium Marketing your Invoice no is ".$generateSno." Have a Pleasent Day");

        //Define route
        $route = "4";
        //Prepare you post parameters
        $postData = array(
            'authkey' => $authKey,
            'mobiles' => $mobileNumber,
            'message' => $message,
            'sender' => $senderId,
            'route' => $route
        );

        //API URL
        $url="https://control.msg91.com/api/sendhttp.php";

        // init the resource
        $ch = curl_init();
        curl_setopt_array($ch, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $postData
            //,CURLOPT_FOLLOWLOCATION => true
        ));


        //Ignore SSL certificate verification
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);


        //get response
        $output = curl_exec($ch);



        //endsms











        return response()->json([
            'data' => 'success',
            'id' => $invoice_id,
            'products' => $products,
        ]);




    }

    function StockUpdateOfProduct(){

    }

    function generateRandomString($length = 10) {
        $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    function random_str($type = 'alphanum', $length = 20)
    {
        switch($type)
        {
            case 'basic'    : return mt_rand();
                break;
            case 'alpha'    :
            case 'alphanum' :
            case 'num'      :
            case 'nozero'   :
                $seedings             = array();
                $seedings['alpha']    = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                $seedings['alphanum'] = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                $seedings['num']      = '0123456789';
                $seedings['nozero']   = '123456789';

                $pool = $seedings[$type];

                $str = '';
                for ($i=0; $i < $length; $i++)
                {
                    $str .= substr($pool, mt_rand(0, strlen($pool) -1), 1);
                }
                return $str;
                break;
            case 'unique'   :
            case 'md5'      :
                return md5(uniqid(mt_rand()));
                break;
        }
    }

    public function ViewAllinv(Request $request){
        $a = CreateInvoiceBV::with('invoice_to_products')->get();
        return response()->json($a);
    }

    public function ViewCreatedInvoice(Request $request, $id){
        $invoice = CreateInvoiceBV::findorfail($id);
        return view('admin.page.bvInvoice.viewinvoice')->with(['invoice' => $invoice]);
    }

    public function getInvoiceDatatables()
    {
        $invoices = CreateInvoiceBV::select(
            [
                'id',
                'invoice_number',
                'invoice_date',
                'invoice_customer_name',
                'invoice_total_taxable_discount_amount',
                'invoice_total_taxable_amount',
                'invoice_total_tax_cgst',
                'invoice_total_tax_sgst',
                'invoice_total_tax_igst',
                'invoice_total_tax_value',
                'invoice_total_taxable_amt',
                'invoice_number_generated',
                'invoice_total_tax',
                'invoice_total',
                'created_at',
            ]);



        return Datatables::of($invoices)


            ->editColumn('invoice_number_generated', function($invoice) {
                return 'EMPL/2020-21-'.$invoice->invoice_number_generated;
            })




            ->addColumn('action', function ($invoice) {
                return '<a href="'.route('dashboard.bvinvoice.view.single',$invoice->id).'" class=" btn btn-xs btn-primary" title="View details"><i class="la la-edit"></i>View Details</a>';
            })


            ->rawColumns(['action'])
            ->orderColumn('id', 'created_at $1')
            ->make();







        /*return DataTables::of($invoices)
            ->make(true);*/
    }

    public function ViewInvoiceAllDatables(Request $request){
        return view('admin.page.bvInvoice.all');
    }

    public function getPrimaryProductDatatables($id)
    {





        $users = InvoiceToProductBV::where('parent_product_id', $id)->select(
            ['id','name','quantity','total',
                'customer_id','color',
                'size','created_at']);







        return Datatables::of($users)
            ->editColumn('created_at', function($user) {
                return Carbon::createFromFormat('Y-m-d H:i:s', $user->created_at)->toDateString();
            })

            ->editColumn('total', function($user) {
                return '<i class="la la-inr"></i> '.$user->total;
            })
            ->addColumn('customer', function ($user) {

                $a = Customer::findorfail($user->customer_id);

                return
                    $a->fname;
            })
            ->rawColumns(['total'])
            ->make();


    }


    public function getVariantProductDatatables($id)
    {





        $users = InvoiceToProductBV::where('product_id', $id)->select(
            ['id','name','quantity','total',
                'customer_id','color',
                'size','created_at'])->orderBy('updated_at', 'DESC');;



        return Datatables::of($users)
            ->editColumn('created_at', function($user) {
                return Carbon::createFromFormat('Y-m-d H:i:s', $user->created_at)->toDateString();
            })

            ->editColumn('total', function($user) {
                return '<i class="la la-inr"></i> '.$user->total;
            })
            ->addColumn('customer', function ($user) {

                $a = Customer::findorfail($user->customer_id);

                return
                    $a->fname;
            })
            ->rawColumns(['total'])
            ->make();


    }

    ///inv expoets single invoice

    public function exportPdfInvoice(Request $request, $id){
        $invoice = CreateInvoiceBV::findorfail($id);

        $invoicepdf = PDF::loadView('admin.page.bvInvoice.generatepdffrominvoice',['invoice' => $invoice])->setPaper('a2');

        return $invoicepdf->download('invoice'.$invoice->invoice_number.'-'.$invoice->invoice_customer_name.'.pdf');


    }

    public function exportPrintInvoice(Request $request, $id){
        $invoice = CreateInvoiceBV::findorfail($id);

        $invoicepdf = PDF::loadView('admin.page.bvInvoice.generatepdffrominvoice',['invoice' => $invoice])->setPaper('a2');

        return $invoicepdf->stream('invoice'.$id.' '.$invoice->invoice_customer_name.'.pdf', array("Attachment" => false));


    }

    //export multiple inv


    public function export(Excel $excel, InvoicesExport $export)
    {
        return (new InvoicesExport)->download('invoices.xlsx');

    }

    public function exportCustomInvoicesview()
    {
        return view('admin.page.bvInvoice.exportcustom');

    }

    public function exportCustomInvoicesviewexcel(Request $request)
    {

        $from = Carbon::parse($request->from);
        $to = Carbon::parse($request->to);

        return (new CustomInvoicesExport())->forYear($from, $to)->download($from.'-'.$to.'invoices.xlsx');

    }



    public function DeleteInvoice($id, Request $request){



        $getinvoicetoProduct = InvoiceToProductBV::where('create_invoices_id', $id)->get();




        foreach ($getinvoicetoProduct as $key => $value) {
            $product_id = $value['product_id'];
            $qty = $value['quantity'];

            $findproduct = ProductVatient::findorfail($product_id);

            $findproduct->stock = ($findproduct->stock + $qty );
            $findproduct->save();



        }



        $getinvoice = CreateInvoiceBV::findorfail($id);

        $getinvoicetoProductdelete = InvoiceToProductBV::where('invoice_id', $id)->delete();


        $getinvoicedelete = CreateInvoiceBV::findorfail($id)->delete();

        return redirect(route('dashboard.bvinvoice.view.all'));




    }






}
