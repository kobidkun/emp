<?php

namespace App\Http\Controllers\customer;

use App\Customer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class TreeController extends Controller
{




    public function __construct()
    {
        $this->middleware('auth:customer-api');
    }


    public function TreeApi($id){
        $findReqUserid = Customer::findorfail($id);

        //  dd($findReqUserid);



        $findReqUser = Customer::findorfail($findReqUserid->id);

        $getCustomerToCustomer = $findReqUser->customer_to_customers->direct_down_line_customer_id;



        $convertuplinetoarray = (explode(',',$getCustomerToCustomer));

        $impode = $str = implode (", ", $convertuplinetoarray);


        $splitted = explode(",", $getCustomerToCustomer);
        $z =  json_encode($splitted);



        $y = json_decode($z);



        $users = DB::table('customers')
            ->whereIn('id', $y)
            ->get();



        return response()->json(

            ['customer' => $findReqUser,
                'downline' =>   $users
            ]



        );


    }
}
