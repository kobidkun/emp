<?php

namespace App;


use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;

use Illuminate\Foundation\Auth\User as Authenticatable;

class StoreUser extends Authenticatable
{
    use HasApiTokens, Notifiable;

    protected $guard = 'storeuser';
    protected $fillable = [
        'name',
        'email',
        'user_type',
        'password',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];
}
