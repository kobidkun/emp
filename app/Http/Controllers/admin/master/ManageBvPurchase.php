<?php

namespace App\Http\Controllers\admin\master;

use App\Master\Types\BVTypes;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class ManageBvPurchase extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth:admin,storeuser,cashier']);


    }


    public function ViewBvType(){

        $a = BVTypes::all();
      //  $a = DB::table('create_bv_purchase_types')->get();

        return view('admin.page.master.bvpurchase.bvpurchase')->with(['a' => $a]);
    }



    public function ViewBvTypedelete($id){

        $a = BVTypes::findorfail($id);

       $a->delete();
        return back();
    }


    public function ViewBvTypesave(Request $request){

        $a =new BVTypes();

        $a->description = $request->description;
        $a->value = $request->value;
        $a->bv = $request->bv;
        $a->price = $request->price;
        $a->type = $request->type;
        $a->save();

        return back();
    }
}
