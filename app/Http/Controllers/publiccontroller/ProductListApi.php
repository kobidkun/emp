<?php

namespace App\Http\Controllers\publiccontroller;

use App\Catagory;
use App\Model\FrontEnd\ManageGallery;
use App\Http\Controllers\admin\FrontEnd\Slider\WebsiteSlider;
use App\Model\FrontEnd\Slider\MobileSloder;
use App\Model\Master\AppConfig;
use App\ProductsToCategory;

use App\ProductVatient;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Product;
use DB;
use Illuminate\Pagination\LengthAwarePaginator;

use Spatie\QueryBuilder\QueryBuilder;


class ProductListApi extends Controller
{
    public function ProductsApiList()
    {




       $pv = ProductVatient::where('stock', '>', 0)->get();

        $collection = collect(
            $pv
        );

        $plucked = $collection->pluck('product_id');

        $plucked->all();


        $collectiontwo = collect($plucked);

        $unique = $collectiontwo->unique();

        $unique->values()->all();

        $products = Product::with(
            'product_primary_images',
            'products_to_categories',
            'products_to_categories2',
            'products_to_categories3'
        )->whereIn('id',$unique)->paginate(12);








        return response()->json([

            'product' => $products,

        ]);








    }


    public function GetProductById($slug){
        $product = Product::with(
            'product_images',
            'product_primary_images',
            'product_ratingss',
            'products_to_categories2',
            'products_to_categories3',
            'products_to_categories',
            'product_to_varients'



        )->where('slug', $slug)->firstOrFail();
        return response()->json([
           'product' => $product
            ->makeVisible('l_desc')
        ]);
    }


    public function GetProductVariantById($slug){
        $product = ProductVatient::with(
            'Images'

        )->where('slug', $slug)->firstOrFail();
        $mainproduct = Product::with(
            'product_images',
            'product_primary_images',
            'product_ratingss',
            'products_to_categories2',
            'products_to_categories3',
            'products_to_categories',
            'product_to_varients'



        )->where('id',$product->product_id)->first();

        return response()->json([
           'product' => $product,
            'mainproduct' => $mainproduct
        ]);
    }


    public function GetProductByCount($count){
        $products = Product::take($count)->inRandomOrder()->get();


        $allproducts = [];

        foreach ($products as $key => $product) {
            $allproducts[] = [
                'id' => $product->id,
                'slug' => $product->slug,

                'name' => $product->name,
                //'img' => $product->product_primary_images->name,
                'price' => $product->base_price,
                'disc_price' => $product->disc_price,
                'img' => $product->product_primary_images
            ];
        }


        return response()->json($allproducts);

    }

    public function GetProductByCategory($calslud){

        $cat = ProductsToCategory::with('products','product_primary_images','product_to_atributes')
            ->where('slug', $calslud)->paginate(12);




        return response()->json($cat);

    }

    public function getcatcountandproduct($cat){

        $products = $cat->products;

        $allproducts  = [];


        foreach ($products as $key => $product) {
            $allproducts[] = [
                'id' => $product->id,
                'slug' => $product->slug,

                'name' => $product->name,
                //'img' => $product->product_primary_images->name,
                'price' => $product->base_price,
                'disc_price' => $product->disc_price,
                'img' => $product->product_primary_images
            ];
        }

        return $allproducts;
    }








    public function ProductsApiListbkp(){

      $products =  $p =  Product::with(
            'product_primary_images'

        )->get();


        return response()->json([
           'product' => $p
        ],200);


    }


    public function MobileSlider(){
        $a = MobileSloder::all();

        return response()->json($a,200);
    }


    public function Gallery($slug){
        $a = ManageGallery::where('type',$slug)->get();

        return response()->json($a,200);
    }

    public function WebsiteSlider(){
        $a = \App\Model\FrontEnd\Slider\WebsiteSlider::all();

        return response()->json($a,200);
    }

    public function ProductbyCategory(){

        // ProductToAtribute


        $users = QueryBuilder::for(Product\ProductToAtribute::class)
            ->allowedFilters('value')
            ->allowedIncludes('productstwo','productstwo.images','productstwo.image')
            ->paginate(12);




        $mul = $users;






        return response()->json([

            'product' => $mul,

        ]);
    }

    public function getConfig(){
        $a = AppConfig::first();
        return response()->json(json_decode($a->config),200);
    }

    public function GetProductImage($slug){
        $productvar = ProductVatient::where('slug', $slug)->first() ;

        $prod = Product::with('product_primary_images')->where('id', $productvar->product_id)->first();

        //dd($prod);

        if ($prod->product_primary_images === null){
            return response()->json(null,401);
        } else {

            return response()->json($prod->product_primary_images->cdn_url, 200);
        }
    }



}
