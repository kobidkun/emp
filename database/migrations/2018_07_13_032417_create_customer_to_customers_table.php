<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomerToCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_to_customers', function (Blueprint $table) {
            $table->increments('id');
            $table->text('customer_id');
            $table->text('ref_id')->nullable();
            $table->text('ic_number')->nullable();
            $table->text('customer_parient_ic_number')->nullable();
            $table->text('up_line_customer_id')->nullable();
            $table->text('down_ldown_line_customer_idine')->nullable();
            $table->text('down_line_member')->nullable();
            $table->text('customer_relation_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer_to_customers');
    }
}
