@extends('admin.index');

@section('content')

    <div class="m-content">
        <div class="row">
            <div class="col-md-6">
                <!--begin::Portlet-->
                <div class="m-portlet m-portlet--tab">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
												<span class="m-portlet__head-icon m--hide">
													<i class="la la-gear"></i>
												</span>
                                <h3 class="m-portlet__head-text">
                                    Create Secondary Category
                                </h3>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <!--begin::Section-->
                        <div class="m-section">


                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif




                            <form class="m-form m-form--fit m-form--label-align-right"
                                  role="form" method="post"
                                  action="{{ route('admin.category.create.tur.save') }}"
                            >
                                {{ csrf_field() }}
                                <div class="m-portlet__body">




                                    <div class="form-group m-form__group row">
                                        <label class="col-form-label col-lg-12 col-sm-12">
                                            Select Secondary Categories
                                        </label>
                                        <div class="col-lg-12 col-md-12 col-sm-12">
                                            <select class="form-control m-select2" id="m_select2_2" name="catagory_id">

                                                @foreach($scts as $sct)


                                                    <option value="{{$sct->id}}">
                                                        {{$sct->name}}
                                                    </option>

                                                @endforeach
                                            </select>
                                        </div>
                                    </div>




















                                    <div class="form-group m-input-group--air">
                                        <label for="exampleInputEmail1">
                                            Category
                                        </label>
                                        <div class="input-group m-input-group">
                                            <div class="input-group-prepend">
														<span class="input-group-text">
															<i class="flaticon-list"></i>
														</span>
                                            </div>
                                            <input type="text"
                                                   class="form-control m-input--air category-name"
                                                   placeholder="Category Name"
                                                   name="name"
                                                   autocomplete="off"
                                                   aria-describedby="basic-addon1">
                                        </div>
                                        <span class="m-form__help">
													Do not Create Same Category Twice
												</span>
                                    </div>
                                    <div class="form-group m-input-group--air">
                                        <label for="exampleInputEmail1">
                                            Slug
                                        </label>
                                        <div class="input-group m-input-group">
                                            <div class="input-group-prepend">
														<span class="input-group-text">
															<i class="flaticon-more-v6"></i>
														</span>
                                            </div>
                                            <input type="text"
                                                   readonly
                                                   class="form-control m-input--air category-slug"
                                                   placeholder="Slug"
                                                   name="slug"
                                                   autocomplete="off"
                                                   aria-describedby="basic-addon1">
                                        </div>
                                        <span class="m-form__help">
													This is auto generated.You cannot edit it.This is required to generate url
												</span>
                                    </div>
                                    <div class="form-group m-input-group--air">
                                        <label for="exampleInputEmail1">
                                            Description
                                        </label>
                                        <div class="input-group m-input-group">

                                            <textarea name="description" id="mytextarea">Category Description</textarea>
                                        </div>
                                        <span class="m-form__help">
													Description
												</span>
                                    </div>

                                </div>






                                <div class="m-portlet__foot m-portlet__foot--fit">
                                    <div class="m-form__actions">
                                        <button type="submit" class="btn btn-primary">
                                            Submit
                                        </button>
                                        <button type="reset" class="btn btn-secondary">
                                            Cancel
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <!--end::Section-->

                    </div>
                </div>
                <!--end::Portlet-->
                <!--begin::Portlet-->

            </div>
        </div>
    </div>

 @endsection



@section('admin_footer_script')
    <script>
        //== Class definition
        var Select2 = function() {
            //== Private functions
            var demos = function() {
                // basic
                $('#m_select2_1, #m_select2_1_validate').select2({
                    placeholder: "Select a state"
                });

                // nested
                $('#m_select2_2, #m_select2_2_validate').select2({
                    placeholder: "Select a state"
                });

                // multi select
                $('#m_select2_3, #m_select2_3_validate').select2({
                    placeholder: "Select a state",
                });

                // basic
                $('#m_select2_4').select2({
                    placeholder: "Select a state",
                    allowClear: true
                });

                // loading data from array
                var data = [{
                    id: 0,
                    text: 'Enhancement'
                }, {
                    id: 1,
                    text: 'Bug'
                }, {
                    id: 2,
                    text: 'Duplicate'
                }, {
                    id: 3,
                    text: 'Invalid'
                }, {
                    id: 4,
                    text: 'Wontfix'
                }];

                $('#m_select2_5').select2({
                    placeholder: "Select a value",
                    data: data
                });

                // loading remote data

                function formatRepo(repo) {
                    if (repo.loading) return repo.text;
                    var markup = "<div class='select2-result-repository clearfix'>" +
                        "<div class='select2-result-repository__meta'>" +
                        "<div class='select2-result-repository__title'>" + repo.full_name + "</div>";
                    if (repo.description) {
                        markup += "<div class='select2-result-repository__description'>" + repo.description + "</div>";
                    }
                    markup += "<div class='select2-result-repository__statistics'>" +
                        "<div class='select2-result-repository__forks'><i class='fa fa-flash'></i> " + repo.forks_count + " Forks</div>" +
                        "<div class='select2-result-repository__stargazers'><i class='fa fa-star'></i> " + repo.stargazers_count + " Stars</div>" +
                        "<div class='select2-result-repository__watchers'><i class='fa fa-eye'></i> " + repo.watchers_count + " Watchers</div>" +
                        "</div>" +
                        "</div></div>";
                    return markup;
                }

                function formatRepoSelection(repo) {
                    return repo.full_name || repo.text;
                }

                $("#m_select2_6").select2({
                    placeholder: "Search for git repositories",
                    allowClear: true,
                    ajax: {
                        url: "https://api.github.com/search/repositories",
                        dataType: 'json',
                        delay: 250,
                        data: function(params) {
                            return {
                                q: params.term, // search term
                                page: params.page
                            };
                        },
                        processResults: function(data, params) {
                            // parse the results into the format expected by Select2
                            // since we are using custom formatting functions we do not need to
                            // alter the remote JSON data, except to indicate that infinite
                            // scrolling can be used
                            params.page = params.page || 1;

                            return {
                                results: data.items,
                                pagination: {
                                    more: (params.page * 30) < data.total_count
                                }
                            };
                        },
                        cache: true
                    },
                    escapeMarkup: function(markup) {
                        return markup;
                    }, // let our custom formatter work
                    minimumInputLength: 1,
                    templateResult: formatRepo, // omitted for brevity, see the source of this page
                    templateSelection: formatRepoSelection // omitted for brevity, see the source of this page
                });

                // custom styles

                // tagging support
                $('#m_select2_12_1, #m_select2_12_2, #m_select2_12_3, #m_select2_12_4').select2({
                    placeholder: "Select an option",
                });

                // disabled mode
                $('#m_select2_7').select2({
                    placeholder: "Select an option"
                });

                // disabled results
                $('#m_select2_8').select2({
                    placeholder: "Select an option"
                });

                // limiting the number of selections
                $('#m_select2_9').select2({
                    placeholder: "Select an option",
                    maximumSelectionLength: 2
                });

                // hiding the search box
                $('#m_select2_10').select2({
                    placeholder: "Select an option",
                    minimumResultsForSearch: Infinity
                });

                // tagging support
                $('#m_select2_11').select2({
                    placeholder: "Add a tag",
                    tags: true
                });

                // disabled results
                $('.m-select2-general').select2({
                    placeholder: "Select an option"
                });
            }

            var modalDemos = function() {
                $('#m_select2_modal').on('shown.bs.modal', function () {
                    // basic
                    $('#m_select2_1_modal').select2({
                        placeholder: "Select a state"
                    });

                    // nested
                    $('#m_select2_2_modal').select2({
                        placeholder: "Select a state"
                    });

                    // multi select
                    $('#m_select2_3_modal').select2({
                        placeholder: "Select a state",
                    });

                    // basic
                    $('#m_select2_4_modal').select2({
                        placeholder: "Select a state",
                        allowClear: true
                    });
                });
            }

            //== Public functions
            return {
                init: function() {
                    demos();
                    modalDemos();
                }
            };
        }();

        //== Initialization
        jQuery(document).ready(function() {
            Select2.init();
        });
    </script>
    <script src='{{asset('/production/js/admin/tinymce/tinymce.min.js')}}'></script>
    <script>
        tinymce.init({
            selector: '#mytextarea'
        });
    </script>
    <script>



        $(document).ready(function(){
            $(".category-name").keyup(function(){

                var cat_name_val = $( this ).val();
                var actualSlug = cat_name_val.replace(/ /g,'-').toLowerCase()

              $(".category-slug").inputVal(actualSlug);



            });
        });


    </script>

    @endsection