<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInitialCreateOrderToProductBVsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('initial_create_order_to_product_b_vs', function (Blueprint $table) {
            $table->increments('id');
            $table->text('product_id');
            $table->text('quantity');
            $table->text('parent_product_id');
            $table->text('initial_create_orders_id');
            $table->text('customer_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('initial_create_order_to_product_b_vs');
    }
}
