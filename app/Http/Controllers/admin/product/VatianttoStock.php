<?php

namespace App\Http\Controllers\admin\product;

use App\Product;
use App\ProductVatienttoStock;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\DataTables;
use Carbon\Carbon;

class VatianttoStock extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth:admin,storeuser,cashier']);


    }


    public function addStock(Request $request){

        $a = new ProductVatienttoStock();

        $a->amount = $request->stock_number;
        $a->description = $request->description;

        $a->prim_product = $request->primary_product_id;
        $a->product_vatient_id = $request->product_id;

        $a->save();

        $b = \App\ProductVatient::findorfail($request->product_id);
         $getvariniStock = $b->stock;

         $b->stock = $a->amount + $getvariniStock;
             $b->save();

         $c = Product::findorfail($request->primary_product_id);

         $getProIniStock = $c->stock;

         $c->stock = $getProIniStock + $a->amount;

         $c->save();


        return back();

    }

    public function ProductVarianrstockdatatable($id){


        $users = \App\ProductVatienttoStock::where('product_vatient_id', $id)
            ->select(['id','amount','description','created_at']);



        return Datatables::of($users)
            ->editColumn('created_at', function($user) {
                return Carbon::createFromFormat('Y-m-d H:i:s', $user->created_at)->toDayDateTimeString();
            })
            ->addColumn('action', function ($user) {
                return
                    '<a href="/admin/product/varient/stock/delete/'.$user->id.'" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i> Delete</a>';
            })
            ->rawColumns(['action'])
            ->make();
    }

    public function delete($id){
        $a = \App\ProductVatienttoStock::findorfail($id);
       // dd($a);
       // $currentStock = $a->amount;





        $b = \App\ProductVatient::findorfail($a->product_vatient_id);
        $getvariniStock = $b->stock;

        $b->stock = $getvariniStock - $a->amount;
        $b->save();

        $c = Product::findorfail($a->prim_product);

        $getProIniStock = $c->stock;

        $c->stock = $getProIniStock - $a->amount;

        $c->save();







        $a->delete();
        return back();
    }
}
