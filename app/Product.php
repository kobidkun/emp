<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{

    protected $hidden = [
        'updated_at',
        'created_at',
        'varient_id',
        'arrtibute_id',
        'catagory_id',
        'catagory3_id',
        'catagory2_id',
        'stock_id',
        'rating_id',
        'l_desc',
        's_desc',
        'h_light',
        'base_price',
        'offer_price',
        'base_price',
        'dis_per',
        'active',
        'discount_price',
        'disc_price',
        'code',
    ];



    public function catagories()
    {
        return $this->belongsToMany('App\Catagory','catagories','product_id','id');
    }


    public function products_to_categories()
    {
        return $this->hasOne('App\ProductsToCategory','product_id','id');
    }



    public function products_to_categories2()
    {
        return $this->hasOne('App\ProductToCategory2','product_id','id');
    }

    public function products_to_categories3()
    {
        return $this->hasOne('App\ProductToCategory3','product_id','id');
    }



    public function product_primary_images()
    {
        return $this->hasOne('App\ProductPrimaryImage','product_id','id');
    }

    public function product_images()
    {
        return $this->hasMany('App\ProductImage','product_id','id');
    }

    public function images()
    {
        return $this->hasMany('App\ProductImage','product_id','id');
    }

    public function image()
    {
        return $this->hasOne('App\ProductPrimaryImage','product_id','id');
    }



    public function product_ratingss()
    {
        return $this->hasMany('App\Rating','product_id','id');
    }

    public function product_to_atributes()
    {
        return $this->hasMany('App\Product\ProductToAtribute','product_id','id');
    }

    public function product_to_varients()
    {
        return $this->hasMany('App\ProductVatient','product_id','id');
    }

    public function product_to_sale()
    {
        return $this->hasMany('App\model\Invoice\InvoiceToProduct','parent_product_id','id');
    }


}
