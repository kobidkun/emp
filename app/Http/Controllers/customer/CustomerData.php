<?php

namespace App\Http\Controllers\Customer;


use App\Customer;
use App\CustomerToMoreDetails;
use App\CustomerToNotification;
use App\Master\Types\BVTypes;
use App\Model\Misc\DBNotification;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class CustomerData extends Controller
{


    public function __construct()
    {
        $this->middleware('auth:customer-api');
    }


   public function Profile(Request $request){

       $getcustomer = $request->user();

       $getmore = CustomerToMoreDetails::where('customer_id',$getcustomer->id)->first();

     //  return response()->json($getmore);
       if ($getmore === null){
           $ab = new CustomerToMoreDetails();
           $ab->customer_id = $getcustomer->id;
           $ab->save();
       }

       $kus = Customer::with('customer_to_more_details')->find($getcustomer->id);

       return response()->json($kus, 200);

   }


   public function Notificationss(Request $request){

       $getcustomer = $request->user();

       $kusur = $getcustomer->custnotifications()->where('read', 0)->orderBy('id', 'DESC');
       $kusrd = $getcustomer->custnotifications()->where('read', 1)->orderBy('id', 'DESC');

       $kuunread = $kusur->get();
       $kuunreadcounrt = $kusur->count();
       $kuread = $kusrd->get();
     //  $kuread = $kus::where('read', 1)->get();



      foreach ($kuunread as $ku) {
       //   dd($ku->id);
           $a = DBNotification::find($ku->id);
           $a->read = true;
           $a->save();
       }

       return response()->json([
           'urc' => $kuunreadcounrt,
           'ur' => $kuunread,
           'r' => $kuread,
       ], 200);

   }


   public function NotificationssCount(Request $request){

       $getcustomer = $request->user();

       $kusur = $getcustomer->custnotifications()->where('read', 0);


       $kuunreadcounrt = $kusur->count();



       return response()->json([
           'urc' => $kuunreadcounrt
       ], 200);

   }

   public function GetOrder(Request $request){
       $getcustomer = $request->user();

       $customer_id = $getcustomer->id;




        $a = \App\Model\Order\CreateOrder::with([
            'order_to_products'
        ])->where('customer_id', $customer_id)->latest()->get();

        return response()->json($a,200);

   }

   public function GetBVOrder(Request $request){

       $getcustomer = $request->user();

       $customer_id = $getcustomer->id;




       $a = \App\Model\Order\BVOrder\CreateBVOrder::with([
           'order_to_products','create_b_v_order_to_payments'
       ])->where('customer_id', $customer_id)->latest()->paginate(12);

       return response()->json($a,200);

   }


   public function Getorderbvdetails($slug){
       $a = \App\Model\Order\BVOrder\CreateBVOrder::with([
           'order_to_products','create_b_v_order_to_payments','order_to_pickups'
       ])->where('order_secure_id', $slug)->first();

       return response()->json($a,200);
   }

   public function GetInvoice(Request $request){

       $getcustomer = $request->user();

       $customer_id = $getcustomer->id;




       $a = \App\Model\Invoice\CreateInvoice::with([
           'invoice_to_products'
       ])->where('customer_id', $customer_id)->latest()->get();

       return response()->json($a,200);

   }

   public function GetBVInvoice(Request $request){

       $getcustomer = $request->user();

       $customer_id = $getcustomer->id;




       $a = \App\Model\BVInvoice\CreateInvoiceBV::with([
           'invoice_to_products'
       ])->where('customer_id', $customer_id)->latest()->get();

       return response()->json($a,200);

   }

   public function GetReturnInvoice(Request $request){

       $getcustomer = $request->user();

       $customer_id = $getcustomer->id;




       $a = \App\Model\Invoice\ReturnInvoice::with([
           'invoice_to_products'
       ])->where('customer_id', $customer_id)->latest()->get();

       return response()->json($a,200);


   }

   public function GetReturnBVInvoice(Request $request){

       $getcustomer = $request->user();

       $customer_id = $getcustomer->id;

       $a = \App\Model\BVInvoice\ReturnInvoiceBV::with([
           'return_invoiceto_products'
       ])->where('customer_id', $customer_id)->latest()->get();

       return response()->json($a,200);

   }


    public function getForApiforBvPurchaseTypes(){

        $a = BVTypes::all();

        return response()->json($a,200);
    }



   public function  createMoredata(Request $request){
        $getcustomer = $request->user();
        $a = new CustomerToMoreDetails();
        $a->customer_id = $getcustomer->id;
        $a->save();

        return response()->json($a,200);
   }

   public function getMoredatasave(Request $request){
       $getcustomer = $request->user();

       $getmore = CustomerToMoreDetails::where('customer_id',$getcustomer->id)->first();


       $adharurl = null;
       $adharurl2 = null;
       $panurl = null;
       $panurl2 = null;
       $selfileurl = null;

       if ($request->addharimage !== null){

           $image = $request->imagef;  // your base64 encoded
           $image = str_replace('data:image/jpeg;base64,', '', $image);
           $image = str_replace(' ', '+', $image);
           $imageName = str_random(10).'.'.'jpeg';

           $imageb = $request->imageb;  // your base64 encoded
           $imageb = str_replace('data:image/jpeg;base64,', '', $imageb);
           $imageb = str_replace(' ', '+', $imageb);
           $imageNameb = str_random(10).'.'.'jpeg';

           //   \File::put(public_path(). '/secure/document/verify/' . $imageName, base64_decode($image));
         $imgup =  Storage::disk('s3')->put('secure/doccument/permament/verify/'.$imageName, base64_decode($image));
         $imgupb =  Storage::disk('s3')->put('secure/doccument/permament/verify/'.$imageNameb, base64_decode($imageb));

           $adharurl =   env('cdnurl').'/secure/doccument/permament/verify/'.$imageName;
           $adharurl2 =   env('cdnurl').'/secure/doccument/permament/verify/'.$imageNameb;
       //  return response()->json($url2);



       }

       if ($request->panimage !== null){

           $image = $request->imagef;  // your base64 encoded
           $image = str_replace('data:image/jpeg;base64,', '', $image);
           $image = str_replace(' ', '+', $image);
           $imageName = str_random(10).'.'.'jpeg';

           $imageb = $request->imageb;  // your base64 encoded
           $imageb = str_replace('data:image/jpeg;base64,', '', $imageb);
           $imageb = str_replace(' ', '+', $imageb);
           $imageNameb = str_random(10).'.'.'jpeg';

           //   \File::put(public_path(). '/secure/document/verify/' . $imageName, base64_decode($image));
           $imgup =  Storage::disk('s3')->put('secure/doccument/permament/verify/'.$imageName, base64_decode($image));
           $imgupb =  Storage::disk('s3')->put('secure/doccument/permament/verify/'.$imageNameb, base64_decode($imageb));

           $panurl =   env('cdnurl').'/secure/doccument/permament/verify/'.$imageName;
           $panurl2 =   env('cdnurl').'/secure/doccument/permament/verify/'.$imageNameb;
           //  return response()->json($url2);
       }

       if ($request->selfile !== null){

           $image = $request->imagef;  // your base64 encoded
           $image = str_replace('data:image/jpeg;base64,', '', $image);
           $image = str_replace(' ', '+', $image);
           $imageName = str_random(10).'.'.'jpeg';

           //   \File::put(public_path(). '/secure/document/verify/' . $imageName, base64_decode($image));
           $imgup =  Storage::disk('s3')->put('secure/doccument/permament/verify/'.$imageName, base64_decode($image));


           $selfileurl =   env('cdnurl').'/secure/doccument/permament/verify/'.$imageName;

           //  return response()->json($url2);
       }


       if ($getmore !== null){
           if ($request->panimage !== null){
               $getmore->panrno = 'na';
               $getmore->pandata = $panurl;
               $getmore->panimage = $panurl2;
           }

           if ($request->selfile !== null){
               $getmore->selfile = $selfileurl;
               $getmore->selfiledump = 'na';
           }


           if ($request->addharimage !== null){
               $getmore->aadharno = 'na';
               $getmore->addhardata = $adharurl;
               $getmore->addharimage = $adharurl2;
           }
           $getmore->save();
           return response()->json(true,200);

       } else {
           $a = new CustomerToMoreDetails();
           $a->customer_id = $getcustomer->id;
           $a->save();

           $getmore = CustomerToMoreDetails::where('customer_id',$getcustomer->id)->first();
           $getmore->aadharno = 'na';
           $getmore->addhardata = $adharurl;
           $getmore->addharimage = $adharurl2;
           $getmore->panrno = 'na';
           $getmore->pandata = $panurl;
           $getmore->panimage = $panurl2;
           $getmore->selfile = $selfileurl;
           $getmore->selfiledump = 'na';
           $getmore->ke1 = $request->ke1;
           $getmore->ke2 = $request->ke2;
           $getmore->ke3 = $request->ke3;
           $getmore->ke4 = $request->ke4;
           $getmore->ke5 = $request->ke5;
           $getmore->ke6 = $request->ke6;
           $getmore->ke7 = $request->ke7;
           $getmore->ke8 = $request->ke8;
           $getmore->ke9 = $request->ke9;
           $getmore->ke0 = $request->ke10;
           $getmore->save();
           return response()->json(true,200);

       }




   }

    public function createmoredatacreate ($user){
        $getmore = new CustomerToMoreDetails();
        $getmore->customer_id = $user->id;
        $getmore->aadharno = null;
        $getmore->addhardata = null;
        $getmore->addharimage = null;
        $getmore->panrno = null;
        $getmore->pandata = null;
        $getmore->panimage = null;
        $getmore->selfile = null;
        $getmore->selfiledump = null;
        $getmore->ke1 = null;
        $getmore->ke2 = null;
        $getmore->ke3 = null;
        $getmore->ke4 = null;
        $getmore->ke5 = null;
        $getmore->ke6 = null;
        $getmore->ke7 = null;
        $getmore->ke8 = null;
        $getmore->ke9 = null;
        $getmore->ke0 = null;
        $getmore->save();
    }


        public function updateNotifytoken (Request $request){
        $getcustomer = $request->user();

        $token = $getcustomer->customer_to_notifications;


     //   $collection = collect([1, 2, 3, 4, 5]);

        $multiplied = $token->map(function ($item, $key) {
            return $item->uuid;
        });

        $multiplied->all();

        $notifiable = 'kkkk';


        $collection = collect($token);

     $vvv =   $collection->contains('uuid', $request->uuid);




       $player_id = "631c2170-5709-4b2a-8109-24cf3645cbee";



       // $return["allresponses"] = $response;
      //  $return = json_encode( $return);



      //  return response()->json($return);

     if ($vvv === false){
         $a = new CustomerToNotification();
         $a->customer_id = $getcustomer->id;
         $a->uuid = $request->uuid;
         $a->devices = $request->devices;
         $a->token = $request->token;
         $a->save();
         $player_id = "631c2170-5709-4b2a-8109-24cf3645cbee";
       $response = $this->sendMessage($player_id);




         return response()->json($response,200);
     } else {
         return response()->json($vvv,200);
     }





    }


    function sendMessage($player_id){
        $content = array(
            "en" => 'Welcome to Emporium Marketing. You will receive important messages & updates from now'
        );

        $fields = array(
            'app_id' => env('one_app_id'),
            'include_player_ids' => array($player_id),
            'data' => array("foo" => "bar"),
            'contents' => $content
        );

        $fields = json_encode($fields);
        print("\nJSON sent:\n");
        print($fields);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        $response = curl_exec($ch);
        curl_close($ch);

        return $response;
    }




}
