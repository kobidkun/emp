<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDBNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('d_b_notifications', function (Blueprint $table) {
            $table->increments('id');
            $table->string('customer_id');
            $table->string('type')->nullable();
            $table->string('read')->default(false);
            $table->text('msg')->nullable();;
            $table->text('slug')->nullable();;

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('d_b_notifications');
    }
}
