<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductVatientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_vatients', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('code_name')->nullable();

            $table->string('hsn')->nullable();
            $table->string('type')->nullable();
            $table->string('unit')->nullable();
            $table->string('code_value')->nullable();
            $table->string('base_price')->nullable();
            $table->string('disc_price')->nullable();
            $table->string('offer_price')->nullable();
            $table->string('mrp_price')->nullable();
            $table->string('disc_amount')->nullable();
            $table->string('discount_price')->nullable();
            $table->string('discount_per')->nullable();
            $table->string('dis_per')->nullable();
            $table->string('taxable_value')->nullable();
            $table->string('cgst')->nullable();
            $table->string('cgst_percentage')->nullable();
            $table->string('sgst')->nullable();
            $table->string('sgst_percentage')->nullable();
            $table->string('igst')->nullable();
            $table->string('igst_percentage')->nullable();

            $table->string('taxable_rate')->nullable();
            $table->string('cess_percentage')->nullable();

            $table->string('total_tax')->nullable();
            $table->string('listing_price')->nullable();
            $table->string('slug')->nullable();
            $table->string('stock_id')->nullable();
            $table->string('stock')->nullable();

            $table->string('color')->nullable();
            $table->string('size')->nullable();
            $table->string('product_id')->nullable();
            $table->string('business_vol')->nullable();





            $table->boolean('active')->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_vatients');
    }
}
