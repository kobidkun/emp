@extends('admin.index');

@section('content')

    <div class="m-content">
        <div class="row">
            <div class="col-md-6">
                <!--begin::Portlet-->
                <div class="m-portlet m-portlet--tab">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
												<span class="m-portlet__head-icon m--hide">
													<i class="la la-gear"></i>
												</span>
                                <h3 class="m-portlet__head-text">
                                    Add Website Slider
                                </h3>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <!--begin::Section-->
                        <div class="m-section">



                                <form method="POST" action="{{ route('website.slider.frontend.uploadImage') }}"
                                      class="m-dropzone dropzone m-dropzone--primary"
                                      id="m-dropzone-two"
                                >

                                    {{ csrf_field() }}

                                    <input type="text" name="title" class="form-control" placeholder="Enter Title">
                                    <div class="m-dropzone__msg dz-message needsclick">
                                        <h3 class="m-dropzone__msg-title">
                                            Drop new Secondary Images here or click to upload and refresh the page
                                        </h3>
                                        <span class="m-dropzone__msg-desc">
														Upload up to 10 files
													</span>
                                    </div>
                                </form>





                        </div>
                        <!--end::Section-->

                    </div>
                </div>
                <!--end::Portlet-->
                <!--begin::Portlet-->

            </div>
            <div class="col-md-6">
                <!--begin::Portlet-->
                <div class="m-portlet m-portlet--tab">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
												<span class="m-portlet__head-icon m--hide">
													<i class="la la-gear"></i>
												</span>
                                <h3 class="m-portlet__head-text">
                                    All website Slider
                                </h3>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <!--begin::Section-->
                        <div class="m-section">



                            @foreach($images as $image)
                                <div class="m-demo" data-code-preview="true" data-code-html="true"
                                     data-code-js="false">
                                    <div class="m-demo__preview">
                                        <div class="m-stack m-stack--ver m-stack--general m-stack--demo">
                                            <div class="m-stack__item" style="width: 250px;">
                                                <img src="{{asset($image->cdn_url)}}"
                                                     style="display: block;
    margin-left: auto;
    margin-right: auto;
    width: 220px;"
                                                >
                                            </div>
                                            <div class="m-stack__item m-stack__item--center m-stack__item--middle">
                                                <div style="text-align: center">



                                                    <br>
                                                    <br>
                                                    <br>


                                                    <a href="{{route('website.slider.frontend.delete',[$image->id])}}"
                                                       class="btn m-btn--pill m-btn--air  btn-danger"
                                                    >

                                                        <i class="la la-trash"></i> Delete Image

                                                    </a>


                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                            @endforeach





                        </div>
                        <!--end::Section-->

                    </div>
                </div>
                <!--end::Portlet-->
                <!--begin::Portlet-->

            </div>
        </div>
    </div>

@endsection



@section('admin_footer_script')
    <script>
        //== Class definition

        var DropzoneDemo = function () {
            //== Private functions
            var demos = function () {
                // single file upload
                // multiple file upload
                Dropzone.options.mDropzoneTwoPrimaryImage = {
                    paramName: 'image',
                    method: 'POST',
                    maxFilesize: 1, // MB
                    maxFiles: 1,
                    acceptedFiles: ".jpeg,.jpg,.png,.gif"

                };
                Dropzone.options.mDropzoneTwoPrimary  = {
                    paramName: 'image',
                    method: 'POST',
                    maxFilesize: 1, // MB
                    maxFiles: 1,
                    acceptedFiles: ".jpeg,.jpg,.png,.gif",
                    init: function () {
                        this.on("maxfilesexceeded", function (file) {
                            this.removeAllFiles();
                            this.addFile(file);
                        })

                    }
                };
                Dropzone.options.mDropzoneTwo = {
                    paramName: 'image',
                    method: 'POST',
                    maxFilesize: 5, // MB
                    maxFiles: 20,
                    acceptedFiles: ".jpeg,.jpg,.png,.gif"

                };


            };

            return {
                // public functions
                init: function() {
                    demos();
                }
            };
        }();

        DropzoneDemo.init();
    </script>>


@endsection