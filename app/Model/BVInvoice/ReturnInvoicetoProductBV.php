<?php

namespace App\Model\BVInvoice;

use Illuminate\Database\Eloquent\Model;

class ReturnInvoicetoProductBV extends Model
{

    public $table = "return_invoiceto_products";

    protected $fillable = [

        'name',
        'product_id',
        'customer_id',
        'parent_product_id',
        'color',
        'size',
        'create_invoices_id',
        'invoice_id',
        'sno',
        'unit',
        'hsn',
        'type',
        'quantity',
        'rate',
        'discount_amount',
        'discount_percentage',
        'taxable_rate',
        'taxable_amount',
        'tax_value',
        'cgst',
        'sgst',
        'igst',
        'sgst_percentage',
        'cgst_percentage',
        'igst_percentage',
        'total',
        'product_total_disc',
    ];
}
