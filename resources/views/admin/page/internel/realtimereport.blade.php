@extends('admin.index');

@section('content')

    <div class="m-content">

        <div class="row">





            <div class="col-md-12">
                    <!--begin::Portlet-->


                <div id="container"></div>


                </div>



        </div>








    </div>



    <div class="m-content">


    </div>

 @endsection

@section('admin_footer_script')

    <style>
        .highcharts-credits{
            display: none;!important;
        }
    </style>



    <script src="{{asset('production/js/highcharts-with-moment.min.js')}}"></script>


    <script>


        function requestData_a() {
            $.ajax({
                url: '{{route('realtime.graph')}}',
                success: function(point) {
                    var series = chart.series[0],
                        shift = series.data.length > 20; // shift if the series is
                                                         // longer than 20

                    // add the point
                    chart.series[0].addPoint(point, true, shift);

                    // call it again after one second
                    setTimeout(requestData_a, 1000);
                },
                cache: false
            });
        }

        document.addEventListener('DOMContentLoaded', (function() {

                Highcharts.theme = {
                    colors: ['#e14052', '#95C471', '#35729E', '#251735'],

                    colorAxis: {
                        maxColor: '#05426E',
                        minColor: '#F3E796'
                    },

                    plotOptions: {
                        map: {
                            nullColor: '#fcfefe'
                        }
                    },

                    navigator: {
                        maskFill: 'rgba(170, 205, 170, 0.5)',
                        series: {
                            color: '#95C471',
                            lineColor: '#35729E'
                        }
                    }
                };

// Apply the theme
                Highcharts.setOptions(Highcharts.theme);



            Highcharts.setOptions({
                time: {
                    timezone: 'Asia/Calcutta'
                }
            });


                chart = Highcharts.chart('container', {
                    chart: {
                        type: 'area',
                        events: {
                            load: requestData_a
                        }
                    },
                    title: {
                        text: 'Live Invoice Status'
                    },
                    xAxis: {
                        type: 'datetime',
                        tickPixelInterval: 150,
                        maxZoom: 20 * 1000
                    },
                    yAxis: {
                        minPadding: 0.2,
                        maxPadding: 0.2,
                        title: {
                            text: 'Invoice Today',
                            margin: 80
                        }
                    },
                    series: [{
                        name: 'Invoice Today',
                        data: []
                    }]
                });
            })
        );


    </script>



    @endsection