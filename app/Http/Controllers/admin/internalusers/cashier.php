<?php

namespace App\Http\Controllers\admin\internalusers;

use App\CasherUser;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class cashier extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth:admin']);


    }


    public function View(){


        return view('admin.page.internaluser.cashier.create');

    }

    public function create(){

        $a = CasherUser::paginate(20);

        return view('admin.page.internaluser.cashier.create')->with(['cashier' => $a]);
    }

    public function SaveUsers(Request $request){

        $password = $request->password;

        $generatepwd = bcrypt($password);

        $a = new \App\CasherUser();

        $a->name = $request->name;
        $a->email = $request->email;
        $a->mobile = $request->mobile;
        $a->password = $generatepwd;
        $a->user_type = 'cashier';
        $a->save();

        return back();




    }

    public function Delete($id){

      $a = CasherUser::findorfail($id);
      $a->delete();
      return back();
    }

    public function Edit(){

        return view('admin.page.internaluser.storeuser.create');
    }
}
