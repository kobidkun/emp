<?php

namespace App\Http\Controllers\export;

use App\Model\BVInvoice\CreateInvoiceBV;
use App\Model\BVInvoice\InvoiceToProductBV;
use App\model\Invoice\CreateInvoice;

use App\model\Invoice\InvoiceToProduct;
use App\Model\Order\BVOrder\CreateBVOrder;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
class ExportGraph extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth:admin,storeuser,cashier']);


    }

    public function ViewInvoice(){
        return view('export.graph.saleinvoice');
    }


    public function  InvoiceGraph(Request $request){


        $from = $request->from;
        $To= $request->to;
        $customer = $request->customer_id;


        $dateFrom = \Carbon\Carbon::parse($from)->format('Y-m-d')." 00:00:00";
        $dateTo = \Carbon\Carbon::parse($To)->format('Y-m-d')." 23:59:59";



        $monthlysales = DB::table('create_invoice_b_vs')
            ->select(
                DB::raw('sum(invoice_total) as total'),
                DB::raw('date(created_at) as dates')


            )->whereBetween('created_at', [$dateFrom, $dateTo])

            ->groupBy('dates')
            ->orderBy('dates','asc')
            ->get();


        $dailysalesqty = DB::table('invoice_to_product_b_vs')
            ->select(
                DB::raw('sum(quantity) as total'),
                DB::raw('date(created_at) as dates')
            //   DB::raw('created_at as kkk')


            )->whereBetween('created_at', [$dateFrom, $dateTo])

            ->groupBy('dates')
            ->orderBy('dates','asc')
            ->get();


        $productSales = DB::table('invoice_to_product_b_vs')
            ->select(
              //  'created_at as dates',
                DB::raw('sum(quantity) as total'),
             //   DB::raw('date(created_at) as dates'),'date(created_at) as dates'

                DB::raw('name as product')

               // 'name'
             //   DB::raw('name')

            //   DB::raw('created_at as kkk')


            )->whereBetween('created_at', [$dateFrom, $dateTo])

            ->groupBy('product')
            ->orderBy('total','asc')
            ->get();


        return view('export.graph.inner.invoice')->with([
            'monthlysales' => $monthlysales,
            'dailysalesqty' => $productSales,
        ]);

    }

    public function test(){




        return view('export.graph.inner.test');



    }


    public function RealtimeReportView(){
        return view('admin.page.internel.realtimereport');
    }



    public function RealtimeReportViewqty(){
        return view('admin.page.internel.realtimereportquantuty');
    }

    public function OnlineorderView(){
        return view('admin.page.internel.onlineorder');
    }


    public function RealtimeOrders(){
        $a = CreateInvoiceBV::select('created_at','id')
            ->whereRaw('created_at >= CURRENT_DATE')
           // ->whereDate('created_at', DB::raw('curdate()'))
        ->get();
        $b = $a->count();
        $d =  time() * 1000;



        return response()->json([$d, $b]);
    }

    public function RealtimeOnlineOrders(){
        $a = CreateBVOrder::select('created_at','id')
            ->whereRaw('created_at >= CURRENT_DATE')
           // ->whereDate('created_at', DB::raw('curdate()'))
        ->get();
        $b = $a->count();
        $d =  time() * 1000;



        return response()->json([$d, $b]);
    }


    public function RealtimeQuantiy(){
        $a = InvoiceToProductBV::select('created_at','quantity')
           // ->whereDate('created_at', DB::raw('CURDATE()'))
            ->whereRaw('created_at >= CURRENT_DATE')
            ->get();
        $b = $a->sum('quantity');
        $d =  time() * 1000;

        return response()->json([$d, $b]);
    }




}
