<?php

namespace App\Model\Order\BVOrder;

use Illuminate\Database\Eloquent\Model;

class CreateBVOrderToProduct extends Model
{
    protected $fillable = [
        'id',
        'product_id',
        'size',
        'color',
        'order_id',
        'parent_product_id',
        'customer_id',
        'product_name',
        'quantity',
        'hsn_sac',
        'hsn',
        'type',
        'taxable_rate',
        'taxable_bv_rate',
        'taxable_value',
        'taxable_bv_value',
        'taxable_discount_amount',
        'taxable_tax_cgst',
        'taxable_tax_sgst',
        'taxable_tax_igst',
        'taxable_tax_cgst_percentage',
        'taxable_tax_sgst_percentage',
        'taxable_tax_igst_percentage',
        'taxable_tax_cess_percentage',
        'taxable_tax_cess',
        'taxable_tax_total',
        'total',
    ];
}
