<?php

namespace App\Http\Controllers\customer;

use App\Model\Order\BVOrder\OrderToPickup;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ManagePickup extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:customer-api');
    }

    public function RequestPickup(Request $request){
        $getcustomer = $request->user();
      //  $a = CreateOrderBV::where('id', $request->order_id)->first();
        // create request
        $getlastpickup = OrderToPickup::where('status', 'complete')
            ->orderBy('id', 'DESC')
            ->first();

        $a = new OrderToPickup();
        $a->customer_id = $getcustomer->id;
        $a->create_b_v_order_id = $request->order_id;
        $a->status = 'pending';
        $a->save();
        // create eta & time

        $eta = ($a->id - $getlastpickup->id)*6 ;
        $position = ($a->id - $getlastpickup->id);

        return response()->json([
            'eta' => $eta,
            'position' => $position,
        ]);



    }

    public function GetCustomerPickup(Request $request){
        $getcustomer = $request->user();
        $getuserlastpickup = OrderToPickup::whereDate('created_at', Carbon::today())
            ->where('status', 'pending')
            ->where('customer_id', $getcustomer->id)
            ->orderBy('id', 'DESC')
            ->first();

        if ($getuserlastpickup !== null){
            $getlastpickup = OrderToPickup::where('status', 'complete')
                ->orderBy('id', 'DESC')
                ->first();

            $eta = ($getuserlastpickup->id - $getlastpickup->id)*6 ;
            $position = ($getuserlastpickup->id - $getlastpickup->id);

            return response()->json([
                'eta' => $eta,
                'position' => $position,
            ]);

        } else {
            return response()->json(false);
        }
    }



    public function RequestPickupstatus($id){

      //  $a = CreateOrderBV::where('id', $request->order_id)->first();
        // create request
        $getlastpickup = OrderToPickup::where('status', 'complete')
            ->orderBy('id', 'DESC')
            ->first();

        $a = OrderToPickup::find($id);

        $eta = ($a->id - $getlastpickup->id)*6 ;
        $position = ($a->id - $getlastpickup->id);

        return response()->json([
            'eta' => $eta,
            'position' => $position,
        ]);



    }
}
