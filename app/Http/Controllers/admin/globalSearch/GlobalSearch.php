<?php

namespace App\Http\Controllers\admin\globalSearch;

use App\Customer;
use App\Model\BVInvoice\CreateInvoiceBV;
use App\model\Invoice\CreateInvoice;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GlobalSearch extends Controller
{
    public function Search(Request $request){


        $srarch = $request->keyword;

        $i = CreateInvoiceBV::where('invoice_number_generated', 'like', "$srarch%")->take(10)
            ->get();

        $j = CreateInvoice::where('invoice_number_generated', 'like', "$srarch%")->take(10)
            ->get();

        $k = Customer::where('fname', 'like', "$srarch%")
            ->orwhere('lname', 'like', "$srarch%")
            ->orwhere('ic_number', 'like', "$srarch%")->take(10)
            ->get();

        return response()->json([
            'b' => $i,
            'j' => $j,
            'k' => $k,
        ]);
    }
}
