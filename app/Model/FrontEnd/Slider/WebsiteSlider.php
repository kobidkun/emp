<?php

namespace App\Model\FrontEnd\Slider;

use Illuminate\Database\Eloquent\Model;

class WebsiteSlider extends Model
{
    protected $hidden = array(


        'updated_at',
        'created_at',

    );
}
