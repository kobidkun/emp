<?php
namespace App\Channels\Messages;

class PushNotifyMessages
{
    public $content;

    public function content($content)
    {
        $this->content = $content;

        return $this;
    }
}