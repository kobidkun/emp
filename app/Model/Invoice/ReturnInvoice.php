<?php

namespace App\Model\Invoice;

use Illuminate\Database\Eloquent\Model;

class ReturnInvoice extends Model
{
    public function invoice_to_products()
    {
        return $this->hasMany('App\Model\Invoice\ReturnInvoicetoProduct', 'create_orders_id', 'id');
    }


    public function invoice_to_stock_out()
    {
        return $this->hasMany('App\StockOut', 'id', 'invoice_id');
    }
}
