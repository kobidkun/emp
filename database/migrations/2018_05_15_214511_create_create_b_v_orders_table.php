<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCreateBVOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('create_b_v_orders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('customer_id');
            $table->string('payment_type');
            $table->string('bookedfrom');
            $table->string('customer_gstin')->nullable();
            $table->text('order_secure_id')->nullable();
            $table->text('order_total_taxable_amount')->nullable();
            $table->text('order_total_taxable_discount_amount')->nullable();
            $table->text('order_total_tax_cgst')->nullable();
            $table->text('order_total_tax_sgst')->nullable();
            $table->text('order_total_tax_igst')->nullable();
            $table->text('order_total_tax_cess')->nullable();
            $table->text('order_total_tax')->nullable();
            $table->text('order_total')->nullable();
            $table->text('order_total_bv')->nullable();
            $table->text('lead_purchase')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('create_b_v_orders');
    }
}
