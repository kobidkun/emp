@extends('admin.index');

@section('content')

    <div class="m-content">
        <div class="row">
            <div class="col-md-12">


                            <div class="m-portlet m-portlet--mobile">

                                <div class="m-portlet__body">
                                    <!--begin: Search Form -->


                                    <style>
                                        <style>
                                        .table > tbody > tr > td {
                                            vertical-align: middle;
                                        }


                                        .dataTables_wrapper .dataTables_paginate .paginate_button:hover {
                                            /* color: white !important; */
                                            /* border: 1px solid #111; */
                                            background: #ffffff !important;
                                            border-color: #5867dd !important;

                                        }


                                        .table .thead-dark th {
                                            color: #fff;
                                            background-color: #5867dd!important;
                                            border-color: #32383e;
                                        }





                                    </style>




                                    <table class="table table-striped table-bordered" id="product-table">
                                        <thead class="thead-dark">
                                        <tr>
                                            <th style="width: 140px">Invoice ID</th>
                                            <th style="width: 80px">Date</th>
                                            <th style="width: 120px">Customer Name</th>
                                            <th >Tax Amt</th>
                                            <th>CGST</th>
                                            <th>TAX</th>

                                            <th>INV TOTAL</th>
                                            <th>DETAILS</th>



                                        </tr>
                                        </thead>
                                    </table>







                        </div>
                        <!--end::Section-->

                    </div>


            </div>
        </div>
    </div>

 @endsection


@section('admin_footer_script')





    <link rel="stylesheet" type="text/css" href="{{asset('/datatable/datatables.css')}}"/>



    <script type="text/javascript" src="{{asset('/datatable/datatables.min.js')}}"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js"></script>
    <script>
        $(function() {
            $('#product-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{{route('dashboard.returninvoice.view.datatable')}}',
                order: [ [0, 'desc'] ],
                columns: [
                    { data: 'invoice_number_generated', name: 'invoice_number_generated' },
                    { data: 'invoice_date', name: 'invoice_date' },
                    { data: 'invoice_customer_name', name: 'invoice_customer_name' },
                    { data: 'invoice_total_taxable_amount', name: 'invoice_total_taxable_amount' },
                    { data: 'invoice_total_tax_cgst', name: 'invoice_total_tax_cgst' },
                    { data: 'invoice_total_tax_igst', name: 'invoice_total_tax_igst' },

                    { data: 'invoice_total', name: 'invoice_total' },
                    {data: 'action', name: 'action', orderable: false, searchable: false},

                ]
            });

        });
    </script>



    @endsection