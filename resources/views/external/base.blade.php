
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Bootswatch: Cerulean</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width">
    <link rel="stylesheet" href="{{asset('/external/bootstrap.min.css')}}" media="screen">
    <link rel="stylesheet" href="{{asset('public/external/custom.css')}}">

</head>
<body>
{{--<div class="navbar navbar-expand-lg fixed-top navbar-dark bg-primary">
    <div class="container">
        <a href="../" class="navbar-brand">Bootswatch</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" id="themes">Themes <span class="caret"></span></a>
                    <div class="dropdown-menu" aria-labelledby="themes">
                        <a class="dropdown-item" href="../default/">Default</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="../cerulean/">Cerulean</a>
                        <a class="dropdown-item" href="../cosmo/">Cosmo</a>
                        <a class="dropdown-item" href="../cyborg/">Cyborg</a>
                        <a class="dropdown-item" href="../darkly/">Darkly</a>
                        <a class="dropdown-item" href="../flatly/">Flatly</a>
                        <a class="dropdown-item" href="../journal/">Journal</a>
                        <a class="dropdown-item" href="../litera/">Litera</a>
                        <a class="dropdown-item" href="../lumen/">Lumen</a>
                        <a class="dropdown-item" href="../lux/">Lux</a>
                        <a class="dropdown-item" href="../materia/">Materia</a>
                        <a class="dropdown-item" href="../minty/">Minty</a>
                        <a class="dropdown-item" href="../pulse/">Pulse</a>
                        <a class="dropdown-item" href="../sandstone/">Sandstone</a>
                        <a class="dropdown-item" href="../simplex/">Simplex</a>
                        <a class="dropdown-item" href="../sketchy/">Sketchy</a>
                        <a class="dropdown-item" href="../slate/">Slate</a>
                        <a class="dropdown-item" href="../solar/">Solar</a>
                        <a class="dropdown-item" href="../spacelab/">Spacelab</a>
                        <a class="dropdown-item" href="../superhero/">Superhero</a>
                        <a class="dropdown-item" href="../united/">United</a>
                        <a class="dropdown-item" href="../yeti/">Yeti</a>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="../help/">Help</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="https://blog.bootswatch.com">Blog</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" id="download">Cerulean <span class="caret"></span></a>
                    <div class="dropdown-menu" aria-labelledby="download">
                        <a class="dropdown-item" target="_blank" href="https://jsfiddle.net/bootswatch/9y480qo5/">Open in JSFiddle</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="../4/cerulean/bootstrap.min.css" download>bootstrap.min.css</a>
                        <a class="dropdown-item" href="../4/cerulean/bootstrap.css" download>bootstrap.css</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="../4/cerulean/_variables.scss" download>_variables.scss</a>
                        <a class="dropdown-item" href="../4/cerulean/_bootswatch.scss" download>_bootswatch.scss</a>
                    </div>
                </li>
            </ul>

        </div>
    </div>
</div>--}}


<div class="container">



    <!-- Forms
    ================================================== -->
    <div class="bs-docs-section">
        <div class="row">
            <div class="col-lg-12">
                <div class="page-header">
                    <h1 id="forms">Payment</h1>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-6">
                <div class="bs-component">
                    <form action="{{route('front.customer.makemapment')}}" method="post">


                        @csrf

                        <fieldset>
                            <legend>Details</legend>


                            <div class="form-group row">
                                <label for="staticEmail" class="col-sm-2 col-form-label">Email</label>
                                <div class="col-sm-10">
                                    <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="{{$a->email}}">
                                </div>
                            </div>


                            <input type="hidden" name="id" value="{{$a->id}}">

                            <div class="form-group row">
                                <label for="staticEmail" class="col-sm-2 col-form-label">Amount</label>
                                <div class="col-sm-10">
                                    <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="{{$a->amount}}">
                                </div>
                            </div>


                            <div class="form-group row">
                                <label for="staticEmail" class="col-sm-2 col-form-label">Phone</label>
                                <div class="col-sm-10">
                                    <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="{{$a->phone}}">
                                </div>
                            </div>


                            <div class="form-group row">
                                <label for="staticEmail" class="col-sm-2 col-form-label">Name</label>
                                <div class="col-sm-10">
                                    <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="{{$a->buyer_name}}">
                                </div>
                            </div>







                            <button type="submit" class="btn btn-primary">Pay Now</button>
                        </fieldset>
                    </form>
                </div>
            </div>



        </div>
    </div>




    <div id="source-modal" class="modal fade" tabindex='-1'>
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Source Code</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
                    <pre contenteditable></pre>
                </div>
            </div>
        </div>
    </div>

    <footer id="footer">
        <div class="row">
            <div class="col-lg-12">

                <ul class="list-unstyled">
                    <li class="float-lg-right"><a href="#top">Back to top</a></li>

                </ul>

            </div>
        </div>

    </footer>


</div>


<script src="{{asset('/external/jquery.min.js')}}"></script>
<script src="{{asset('/external/umd/popper.min.js')}}"></script>
<script src="{{asset('/external/bootstrap.min.js')}}"></script>
<script src="{{asset('/external/custom.js')}}"></script>
</body>
</html>
