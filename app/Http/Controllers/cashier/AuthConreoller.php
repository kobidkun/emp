<?php

namespace App\Http\Controllers\cashier;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Validator;
class AuthConreoller extends Controller
{
    public function __construct()
    {
        $this->middleware('guest:cashier', ['except' => ['logout']]);
    }

    public function loginview(){

        return view('cashier.auth.login');

    }

    public function CashierLogin(Request $request){

        $validator = Validator::make($request->all(), [
            'email' => 'required',
            'password' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput($request->only('email', 'remember'));
        }


        // Attempt to log the user in
        if (Auth::guard('cashier')->attempt(['email' => $request->email, 'password' => $request->password], $request->remember)) {
            // if successful, then redirect to their intended location
            return redirect()->intended(route('customer.dashboard'));
        }
        // if unsuccessful, then redirect back to the login with the form data
        return redirect() ->back()
            ->withErrors($validator, 'password')
            ->withInput($request->only('email', 'remember'));

    }

    public function logout()
    {
        Auth::guard('cashier')->logout();
        return redirect(route('storeuser.login'));
    }
}
