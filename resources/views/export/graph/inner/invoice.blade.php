@extends('admin.index');

@section('content')

    <div class="m-content">
        <div class="row">
            <div class="col-md-6" style="height:720px;">
                <!--begin::Portlet-->
                <div class="m-portlet m-portlet--tab">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
												<span class="m-portlet__head-icon m--hide">
													<i class="la la-gear"></i>
												</span>
                                <h3 class="m-portlet__head-text">
                                    Orders Last 30 Days (updated Daily)
                                </h3>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <!--begin::Section-->
                        <div class="m-section">
                            <div class="m-widget4__chart m-portlet-fit--sides m--margin-top-10 m--margin-top-20" >
                                <canvas id="m_chart_sales_sales_daily"></canvas>
                            </div>
                        </div>

                        <!--end::Section-->
                    </div>
                </div>
                <!--end::Portlet-->
                <!--begin::Portlet-->

            </div>

            <div class="col-md-6" style="height:520px;">
                <!--begin::Portlet-->
                <div class="m-portlet m-portlet--tab">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
												<span class="m-portlet__head-icon m--hide">
													<i class="la la-gear"></i>
												</span>
                                <h3 class="m-portlet__head-text">
                                    Daily Sales Last 30 Days
                                </h3>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <!--begin::Section-->
                        <div class="m-section">

                            <div class="m-widget14__chart" >
                                <canvas  id="m_chart_daily_sales"></canvas>
                            </div>




                        </div>

                        <!--end::Section-->
                    </div>
                </div>
                <!--end::Portlet-->
                <!--begin::Portlet-->

            </div>
        </div>
    </div>

@endsection

@section('admin_footer_script')

    <script>//== Class definition
        var Dashboard = function() {

            //== Sparkline Chart helper function


            //== Daily Sales chart.
            //** Based on Chartjs plugin - http://www.chartjs.org/


            //== Profit Share Chart.
            //** Based on Chartist plugin - https://gionkunz.github.io/chartist-js/index.html


            //== Sales Stats.
            //** Based on Chartjs plugin - http://www.chartjs.org/
            var salesStats = function() {
                if ($('#m_chart_sales_sales_daily').length == 0) {
                    return;
                }

                var config = {
                    type: 'line',
                    data: {
                        labels: [

                            @foreach($monthlysales as $monthlysale)

                                ' {{$monthlysale->dates}}' ,

                            @endforeach
                        ],
                        datasets: [{
                            label: "Total Sales in INR ",
                            borderColor: mUtil.getColor('brand'),
                            borderWidth: 2,
                            pointBackgroundColor: mUtil.getColor('brand'),

                            backgroundColor: mUtil.getColor('accent'),

                            pointHoverBackgroundColor: mUtil.getColor('danger'),
                            pointHoverBorderColor: Chart.helpers.color(mUtil.getColor('danger')).alpha(0.2).rgbString(),
                            data: [
                                @foreach($monthlysales as $monthlysale)

                                {{$monthlysale->total}},

                                @endforeach
                            ]
                        }]
                    },
                    options: {
                        title: {
                            display: true,
                        },
                        tooltips: {
                            intersect: false,
                            mode: 'nearest',
                            xPadding: 10,
                            yPadding: 10,
                            caretPadding: 10
                        },
                        legend: {
                            display: false,
                            labels: {
                                usePointStyle: false
                            }
                        },
                        responsive: true,
                        maintainAspectRatio: false,
                        hover: {
                            mode: 'index'
                        },
                        scales: {
                            xAxes: [{
                                display: false,
                                gridLines: false,
                                scaleLabel: {
                                    display: true,
                                    labelString: 'Month'
                                }
                            }],
                            yAxes: [{
                                display: false,
                                gridLines: false,
                                scaleLabel: {
                                    display: true,
                                    labelString: 'Value'
                                }
                            }]
                        },

                        elements: {
                            point: {
                                radius: 3,
                                borderWidth: 0,

                                hoverRadius: 8,
                                hoverBorderWidth: 2
                            }
                        }
                    }
                };

                var chart = new Chart($('#m_chart_sales_sales_daily'), config);
            }

            //== Daily Sales chart.
            //** Based on Chartjs plugin - http://www.chartjs.org/
            var dailySales = function() {
                var chartContainer = $('#m_chart_daily_sales');

                if (chartContainer.length == 0) {
                    return;
                }

                var chartData = {
                    labels: [
                        @foreach($dailysalesqty as $monthlysale)

                            'name:  {{     $monthlysale->product }} ' ,
                        //

                        @endforeach

                    ],
                    datasets: [{
                        //label: 'Dataset 1',
                        backgroundColor: mUtil.getColor('success'),
                        label: "Sold Quantity ",
                        data: [
                            @foreach($dailysalesqty as $monthlysale)

                            {{$monthlysale->total}} ,

                            @endforeach
                        ]
                    }]
                };

                var chart = new Chart(chartContainer, {
                    type: 'bar',
                    data: chartData,
                    options: {
                        title: {
                            display: true,
                            text:'Quantity '

                        },
                        tooltips: {
                            intersect: false,
                            mode: 'nearest',
                            xPadding: 10,
                            yPadding: 10,
                            caretPadding: 10
                        },
                        legend: {
                            display: false
                        },
                        responsive: true,
                        maintainAspectRatio: false,
                        barRadius: 4,
                        scales: {
                            xAxes: [{
                                display: false,
                                gridLines: false,
                                stacked: true
                            }],
                            yAxes: [{
                                display: false,
                                stacked: true,
                                gridLines: false
                            }]
                        },
                        layout: {
                            padding: {
                                left: 0,
                                right: 0,
                                top: 0,
                                bottom: 0
                            }
                        }
                    }
                });
            }

            //== Profit Share Chart.
            //** Based on Chartist plugin - https://gionkunz.github.io/chartist-js/index.html


            //== Sales Stats.



            return {
                //== Init demos
                init: function() {
                    // init charts

                    salesStats();

                    dailySales();
                }
            };
        }();

        //== Class initialization on page load
        jQuery(document).ready(function() {
            Dashboard.init();
        });
    </script>



@endsection