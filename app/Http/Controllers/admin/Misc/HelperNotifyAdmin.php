<?php

namespace App\Http\Controllers\admin\Misc;

use App\Customer;
use App\Http\Requests\CustomerLogin;
use App\Http\Requests\CustomerRegister;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class HelperNotifyAdmin extends Controller
{
    function sendOneSignalMessage($user,$message,$url){


        $user = Customer::find($user);

        $player_id = $user->customer_to_notifications;

        foreach ($player_id as $id ) {
            

            $content = array(
                "en" => $message
            );

            $fields = array(
                'app_id' => env('one_app_id'),
                'include_player_ids' => array($id->uuid),
                'data' => array("foo" => "bar"),
                'contents' => $content,
                'url' => $url
            );

            $fields = json_encode($fields);
            print("\nJSON sent:\n");
            print($fields);

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8'));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_HEADER, FALSE);
            curl_setopt($ch, CURLOPT_POST, TRUE);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

            $response = curl_exec($ch);
            curl_close($ch);

            return $response;
        }
    }


}
