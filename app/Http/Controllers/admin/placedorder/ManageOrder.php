<?php

namespace App\Http\Controllers\admin\placedorder;

use App\Customer;
use App\Http\Controllers\Customer\CreateOrder;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Yajra\DataTables\DataTables;
use PDF;
use Carbon\Carbon;
class ManageOrder extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth:admin,storeuser,cashier']);


    }


    public function Allorders(){


        return view('admin.page.placedorder.all');



    }

    public function details($id){

        $a = \App\Model\Order\CreateOrder::findorfail($id);


        return view('admin.page.placedorder.viewinvoice')->with(['invoice' => $a]);



    }


    public function exportPdfOrder(Request $request, $id)
    {
        $invoice = \App\Model\Order\CreateOrder::findorfail($id);

        $invoicepdf = PDF::loadView('admin.page.placedorder.generatepdffrominvoice', ['invoice' => $invoice])->setPaper('a4');

        return $invoicepdf->download('invoice' . $invoice->order_secure_id . '-' . $invoice->customer->fname . '.pdf');


    }

    public function exportPrintOrder(Request $request, $id)
    {
        $invoice = \App\Model\Order\CreateOrder::findorfail($id);

        $invoicepdf = PDF::loadView('admin.page.placedorder.generatepdffrominvoice', ['invoice' => $invoice])->setPaper('a4');

        return $invoicepdf->stream('invoice' . $id . ' ' . $invoice->customer->fname . '.pdf', array("Attachment" => false));


    }


    public function Datatable(){


        $invoices = \App\Model\Order\CreateOrder::select(
            [
                'id',
                'order_secure_id',
                'customer_gstin',
                'customer_id',
                'order_total_taxable_amount',
                'order_total_tax_cgst',
                'order_total_tax_sgst',
                'order_total_tax',
                'order_total',
                'payment_type',
                'bookedfrom',
                'created_at',
            ])->orderBy('updated_at', 'DESC');



        return Datatables::of($invoices)
            ->editColumn('created_at', function($invoice) {
                return Carbon::createFromFormat('Y-m-d H:i:s', $invoice->created_at)->toDayDateTimeString();
            })

            ->addColumn('customer_id', function ($invoice) {

                $a = Customer::findorfail($invoice->customer_id);

                return
                    $a->fname;
            })




            ->addColumn('action', function ($invoice) {
                return '<a href="'.route('dashboard.placed.create.view',$invoice->id).'" class=" btn btn-xs btn-primary" title="View details"><i class="la la-edit"></i>View Details</a>';
            })


            ->rawColumns(['action','customer_id'])
            ->orderColumn('id', 'created_at $1')
            ->make();



    }
}
