@extends('admin.index');

@section('content')
    <style>
        .select2-options {


        }
    </style>
    <div class="m-content">
        <div class="row">
            <div class="col-md-12">



                <div class="m-portlet">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
												<span class="m-portlet__head-icon m--hide">
													<i class="la la-gear"></i>
												</span>
                                <h3 class="m-portlet__head-text">
                                    Add attributes to product Product
                                </h3>
                            </div>
                        </div>
                    </div>


                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                @endif



                    <!--begin::Form-->
                    <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed"
                          role="form" method="post"
                          action="{{ route('admin.attribute.product.store') }}"
                    >
                        {{ csrf_field() }}
                        <div class="m-portlet__body">

                            <div class="form-group m-form__group row">
                                <div class="col-lg-6">

                                   </div>



                                    <select name="" id=""class=" form-control attribute-select">


                                        @foreach($atts as $att)
                                            <option value="{{$att->default_value}}">{{$att->name}}</option>
                                            @endforeach

                                    </select>




                                </div>

                                <div class="col-lg-12">


                                    <button class="btn btn-block btn-primary add-attribute">Add Seceded Attribute</button>



                                </div>
                            </div>
                            <div class="form-group m-form__group row attribute-items ">




                            </div>





















                        <div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
                            <div class="m-form__actions m-form__actions--solid">
                                <div class="row">
                                    <div class="col-lg-4"></div>
                                    <div class="col-lg-8">
                                        <button type="submit" class="btn btn-primary">
                                            Next
                                        </button>
                                        <button type="reset" class="btn btn-secondary">
                                            Reset
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!--end::Form-->
                </div>
                <!--end::Portlet-->





            </div>
        </div>
    </div>

@endsection

@section('admin_footer_script')







    <script>
        $(document).ready(function() {

            $(".add-attribute").click(function (event) {
                event.preventDefault()



                var ATTributeproductname = $('.attribute-select :selected').text();
                var ATTributeproductvalue =  $('.attribute-select').val();



                $(".attribute-items").append('' +
                    '<div class="col-lg-4" style="padding-top: 5px">' +
                    '<h2 style="margin-top: 25px">'+ATTributeproductname+'</h2>'+
                    '<input type="hidden"' +
                    'value="'+ATTributeproductname+'"' +
                    'name="attribute_name[]">' +
                    '<input type="hidden"' +
                    'value="{{$id}}"' +
                    'name="product_id[]">' +
                    '</div>' +

                    '<div class="col-lg-8">' +
                    '<label for="">Attribute Value</label>'+
                    '<input type="text"' +
                    'class="form-control m-input--air"' +
                    'placeholder="Name"' +
                    'value="'+ATTributeproductvalue+'"' +
                    'name="attribute_value[]">' +
                    '</div>' +

                    '</div>' +
                    '')




            });



        });

    </script>



@endsection