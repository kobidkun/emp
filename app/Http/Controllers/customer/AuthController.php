<?php

namespace App\Http\Controllers\customer;

use App\Customer;
use App\CustomerToMoreDetails;
use App\Http\Requests\CustomerLogin;
use App\Http\Requests\CustomerRegister;
use App\Mail\Auth\ForgotPasswordOTP;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

use Validator;
use Webpatser\Uuid\Uuid;

class AuthController extends Controller
{
    public function loginPage(){

        return view('customer.page.customer.login');

    }


    public function CustomerRegisterShow(Request $request){

    }

    public function ResetPasswordRequestttVerify(Request $request){
        $phone = $request->mobile;
        $otp = $request->otp;

        $user = Customer::where('mobile', $phone)->first();

        if ($user->customer_to_more_details->ke1 === $otp){

            $kkk = Uuid::generate()->string;
            $user->customer_to_more_details->ke2 = $kkk;

            return response()->json([
                'status' => true,
                'uuid' => $kkk
            ]);
        } else {
            return response()->json([
                'status' => false,
                'uuid' => null
            ]);
        }
    }


    public function ResetPasswordRequestchgpwd(Request $request){
        $phone = $request->mobile;
        $password = $request->password;
        $uuid = $request->uuid;

        $user = Customer::where('mobile', $phone)->first();

        if ($user->customer_to_more_details->ke2 === $uuid){

            $user->password = $password;

            $user->save();

            return response()->json([
                'status' => true,
            ]);
        } else {
            return response()->json([
                'status' => false
            ]);
        }
    }

    public function ResetPasswordRequesttt(Request $request){
        $phone = $request->mobile;
        $email = $request->email;
        $strng = $request->strng;

        $otp = rand(1000,9999);

        $user = Customer::where('mobile', $phone)->first();


        if ($user->customer_to_more_details === null){
            $getmore = new CustomerToMoreDetails();
            $getmore->customer_id = $user->id;
            $getmore->aadharno = null;
            $getmore->addhardata = null;
            $getmore->addharimage = null;
            $getmore->panrno = null;
            $getmore->pandata = null;
            $getmore->panimage = null;
            $getmore->selfile = null;
            $getmore->selfiledump = null;
            $getmore->ke1 = $otp;
            $getmore->ke2 = null;
            $getmore->ke3 = null;
            $getmore->ke4 = null;
            $getmore->ke5 = null;
            $getmore->ke6 = null;
            $getmore->ke7 = null;
            $getmore->ke8 = null;
            $getmore->ke9 = null;
            $getmore->ke0 = null;
            $getmore->save();
        } else{
            $user->customer_to_more_details->ke1 = $otp;

            $user->save();
        }


        //

      $aa =  Mail::to($user->email)->send(new ForgotPasswordOTP($otp));



        /*

        // Authorisation details.
        $username = "info@tecions.com";
        $hash = "0f6e77a7b7899df610723a41f78ae94062618c070555be76f69e70084b1d2c85";


        // Account details
        $apiKey = urlencode('Jj1v1p31YQg-Fb8pLoo1vfohJGuwXiWQOiMg8CqvDM	');

        $msg = 'Thank you for contacting GB7. Your OTP is '.$otp.'. Do Not Share with anyone.';

        // Message details
        $numbers = $phone;
        $sender = urlencode('TSTCNS');
        $message = rawurlencode($msg);


        // Prepare data for POST request
        $data = array('apikey' => $apiKey, 'numbers' => $numbers, "sender" => $sender, "message" => $msg);

        // Send the POST request with cURL
        $ch = curl_init('https://api.textlocal.in/send/');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        curl_close($ch);

        // Process your response here

        */

        return ($user);

    }

    public function CustomerRegisterSave(CustomerRegister $request ){

        $s = new Customer();
        $s->fname = $request->fname;
        $s->lname = $request->lname;
        $s->email = $request->email;
        $s->ref = $request->ref;
        $s->mobile = $request->mobile;
        $s->password = bcrypt($request->password);
        $s->save();
        return response()->json('successful',200);

    }

    public function CustomerLoginShow(Request $request){

    }
    public function __construct()
    {
        $this->middleware('guest:customer', ['except' => ['logout']]);
    }

    public function CustomerLogin(Request $request){

        $validator = Validator::make($request->all(), [
            'email' => 'required|exists:customers,email',
            'password' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput($request->only('email', 'remember'));
        }


        // Attempt to log the user in
        if (Auth::guard('customer')->attempt(['email' => $request->email, 'password' => $request->password], $request->remember)) {
            // if successful, then redirect to their intended location
            return redirect()->intended(route('customer.dashboard'));
        }
        // if unsuccessful, then redirect back to the login with the form data
        return redirect() ->back()
            ->withErrors($validator, 'password')
            ->withInput($request->only('email', 'remember'));

    }


}
