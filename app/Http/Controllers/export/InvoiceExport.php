<?php

namespace App\Http\Controllers\export;

use App\model\Invoice\CreateInvoice;
use File;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class InvoiceExport extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth:admin,storeuser,cashier']);


    }


    public function InvoiceExportTojsonview(Request $request){



        return view('export.exportcustomjson');





    }


    public function InvoiceExportTojson(Request $request){

        $from = $request->from;
        $To= $request->to;

       $dateFrom = \Carbon\Carbon::parse($from)->format('Y-m-d')." 00:00:00";
        $dateTo = \Carbon\Carbon::parse($To)->format('Y-m-d')." 23:59:59";



       $a = CreateInvoice::with('invoice_to_products')
           ->whereBetween('created_at', [$dateFrom, $dateTo])->get();


        $fileName = $dateFrom.' '.$dateTo. time() . '_invoice.json';
        File::put(public_path('/exported/custom/invoice/json/'.$fileName),$a);

        return response()->download(public_path('/exported/custom/invoice/json/'.$fileName));
    }
}
