@component('mail::message')
# Password reset requested

Your OTP to reset password is

@component('mail::button', ['url' => '#'])
{{$otp}}
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
