<?php

namespace App\Http\Controllers\publiccontroller;

use App\Customer;
use App\Http\Controllers\customer\CreateOrderBV;
use App\Model\BVInvoice\CreateInvoiceBV;
use App\Model\BVinvoice\Payment\PaymentDetails;
use App\Model\BVInvoice\ReturnInvoiceBV;
use App\model\Invoice\CreateInvoice;
use App\Model\Invoice\ReturnInvoice;
use App\Model\Order\BVOrder\CreateBVOrder;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use PDF;
use Softon\Indipay\Facades\Indipay;

class PublicWebConreoller extends Controller
{
    public function exportPdfInvoice($slug){
        $invoice = CreateInvoice::where('invoice_secure_id', $slug)->first();

        $invoicepdf = PDF::loadView('admin.page.Invoice.generatepdffrominvoice',['invoice' => $invoice])->setPaper('a4');

        return $invoicepdf->download('invoice'.$invoice->invoice_number.'-'.$invoice->invoice_customer_name.'.pdf');


    }

    public function exportPdfInvoicebv($slug){
        $invoice = CreateInvoiceBV::where('invoice_secure_id', $slug)->first();

        $invoicepdf = PDF::loadView('admin.page.bvInvoice.generatepdffrominvoice',['invoice' => $invoice])->setPaper('a4');

        return $invoicepdf->download('invoice'.$invoice->invoice_number.'-'.$invoice->invoice_customer_name.'.pdf');


    }

    public function exportPdfReturnInvoice($slug){
        $invoice = ReturnInvoice::where('invoice_secure_id', $slug)->first();

        $invoicepdf = PDF::loadView('admin.page.returnInvoice.generatepdffrominvoice',['invoice' => $invoice])->setPaper('a4');

        return $invoicepdf->download('invoice'.$invoice->invoice_number.'-'.$invoice->invoice_customer_name.'.pdf');


    }

    public function exportPdfReturnInvoiceBV($slug){
        $invoice = ReturnInvoiceBV::where('invoice_secure_id', $slug)->first();

        $invoicepdf = PDF::loadView('admin.page.returnInvoicebv.generatepdffrominvoice',['invoice' => $invoice])->setPaper('a4');

        return $invoicepdf->download('invoice'.$invoice->invoice_number.'-'.$invoice->invoice_customer_name.'.pdf');


    }
    public function PaymentProcess($slug){
        $order = PaymentDetails::where('slug',$slug)->first();
        $customer = Customer::find($order->customer_id);

        $gatewaydataini = $order->inidump;
        $gatewaydata = json_decode($gatewaydataini);

      //  return Indipay::process($order);

      //  return Indipay::process($gatewaydata);

       // return response()->json($gatewaydata);

        return ($gatewaydataini);

      //  return redirect(url($gatewaydata->paymentLink));

       /* return view('external.base')->with([
            'a' => $order
        ]);*/

    }

    public function PaymentProcessWeb($slug){
        $order = PaymentDetails::where('slug',$slug)->first();
        $customer = Customer::find($order->customer_id);

        $gatewaydataini = $order->inidump;
        $gatewaydata = json_decode($gatewaydataini);

      //  return Indipay::process($order);

      //  return Indipay::process($gatewaydata);

       // return response()->json($gatewaydata);

        return redirect(url($gatewaydata->paymentLink));

      //  return redirect(url($gatewaydata->paymentLink));

       /* return view('external.base')->with([
            'a' => $order
        ]);*/

    }

    public function PaymentProcessComplete(Request $request){
      //  dd($request);

       // return $request;
       $reqid = $request->orderId;

        $order = PaymentDetails::where('gateway_id',$reqid)->first();
        $order->status = $request->txStatus;
        $order->save();


        $getorder = CreateBVOrder::where('id',$order->create_order_id)->first();

        if ($request->txStatus === "SUCCESS"){
            return response()->json([
                'status' => $order,
                'order' => $getorder
            ],200);
        } else{
            return response()->json([
                'status' => $order,
                'order' => $getorder
            ],401);
        }


       // return response()->json($request);

       // $order = PaymentDetails::where('slug',$slug)->first();


       // return view('external.base');

    }
}
