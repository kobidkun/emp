<?php

namespace App\Http\Controllers\Customer;

use App\Customer;
use App\Customer\CustomerCartItems;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ShopController extends Controller
{


    public function __construct()
    {
        $this->middleware('auth:customer-api');
    }





    public function GetWallet(Request $request){
        $getcustomer = $request->user();
        $getid = $getcustomer->id;

        $getuser = Customer::findorfail($getid);

        $walletbalance = $getuser->waller_balance;

        return response()->json([
            'wallet_balance' => $walletbalance,
            'currency' => 'Rs',
            'customer' => $getuser
        ],200);





    }



    public function GetWalletBV(Request $request){
        $getcustomer = $request->user();
        $getid = $getcustomer->id;

        $getuser = Customer::findorfail($getid);

        $walletbalance = $getuser->bvwaller_balance;

        return response()->json([
            'bv_wallet_balance' => $walletbalance,
            'currency' => 'BV',
            'customer' => $getuser
        ],200);

    }






}
