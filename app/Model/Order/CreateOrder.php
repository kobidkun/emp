<?php

namespace App\Model\Order;

use Illuminate\Database\Eloquent\Model;

class CreateOrder extends Model
{
    protected $fillable = [
        'id'
    ];

    public function order_to_products()
    {
        return $this->hasMany('App\Model\Order\OrderToProduct', 'create_orders_id', 'id');
    }

    public function customer()
    {
        return $this->hasOne('App\Customer', 'id', 'customer_id');
    }

    public function order_to_invoices()
    {
        return $this->hasOne('App\Model\Order\OrderToInvoice', 'create_order_id', 'id');
    }
}
