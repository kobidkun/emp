<?php

namespace App\Mail\Customer;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Activate extends Mailable
{
    use Queueable, SerializesModels;
    protected $a;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($a)
    {
        $this->a = $a;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('email.customer.activate');
    }
}
