<?php

namespace App\Http\Controllers\admin;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\Invoice\CreateInvoice;
use Illuminate\Support\Facades\DB;



class AdminStats extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth:admin,storeuser,cashier']);


    }

    public function DailyOrderStatus(){





        $monthlysales = DB::table('invoice_to_product_b_vs')
            ->select(
                DB::raw('sum(quantity) as total'),
                DB::raw('day(created_at) as dates')
              //  DB::raw('day(created_at) as dates')->date()


            )

            ->groupBy('dates')
            ->orderBy('dates','asc')
            ->get();





        return $monthlysales;



    }
}
