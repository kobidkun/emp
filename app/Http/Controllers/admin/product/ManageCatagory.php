<?php

namespace App\Http\Controllers\admin\product;

use App\Catagory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\DataTables;

class ManageCatagory extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth:admin,storeuser,cashier']);


    }

   public function CreateCatagory(){
       return view('admin.page.catagory.create');
   }

    public function CreateCatagorySave(Request $request){
        $s = new Catagory();
        $s->name = $request->name;
        $s->slug = $request->slug;
        $s->description = $request->description;
        $s->product_id = $request->product_id;
        $s->save();
        return back();
    }

    public function AllCatagory(){
        return view('admin.page.catagory.all');
    }

    public function DeleteCategoryPrimary($id){
        $a = Catagory::findorfail($id);
        $a->delete();
        return back();
    }

    public function getCatagoryDatatables()
    {
        $cats = Catagory::select(['id','name','slug','description','slug']);

        return DataTables::of($cats)
            ->addColumn('details', function ($cat) {
                return '<a href="#edit-'.$cat->id.'" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a>';
            })
            ->make(true);
    }
}
