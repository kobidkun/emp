@extends('admin.index');

@section('content')

    <div class="m-content">
        <div class="row">




            <div class="col-md-6">
                <!--begin::Portlet-->
                <div class="m-portlet m-portlet--tab">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
												<span class="m-portlet__head-icon m--hide">
													<i class="la la-gear"></i>
												</span>
                                <h3 class="m-portlet__head-text">
                                    Create BV Purchase
                                </h3>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <!--begin::Section-->
                        <div class="m-section">


                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif




                            <form class="m-form m-form--fit m-form--label-align-right"
                                  role="form" method="post"
                                  action="{{ route('master.manage.bv.master.save') }}"
                            >
                                {{ csrf_field() }}
                                <div class="m-portlet__body">

                                    <div class="form-group m-input-group--air">
                                        <label for="exampleInputEmail1">
                                            Description
                                        </label>
                                        <div class="input-group m-input-group">
                                            <div class="input-group-prepend">
														<span class="input-group-text">
															<i class="flaticon-list"></i>
														</span>
                                            </div>
                                            <input type="text"
                                                   class="form-control m-input--air category-name"
                                                   placeholder="Description"
                                                   name="description"
                                                   required
                                                   autocomplete="off"
                                                   aria-describedby="basic-addon1">
                                        </div>

                                    </div>




                                    <div class="form-group m-input-group--air">
                                        <label for="exampleInputEmail1">
                                            Value in %
                                        </label>
                                        <div class="input-group m-input-group">
                                            <div class="input-group-prepend">
														<span class="input-group-text">
															<i class="flaticon-more-v6"></i>
														</span>
                                            </div>
                                            <input type="number"

                                                   class="form-control m-input--air category-slug"
                                                   placeholder="Value"
                                                   name="value"
                                                   required
                                                   autocomplete="off"
                                                   aria-describedby="basic-addon1">
                                        </div>

                                    </div>


                                    <div class="form-group m-input-group--air">
                                        <label for="exampleInputEmail1">
                                            BV Amount
                                        </label>
                                        <div class="input-group m-input-group">
                                            <div class="input-group-prepend">
														<span class="input-group-text">
															<i class="flaticon-more-v6"></i>
														</span>
                                            </div>
                                            <input type="number"

                                                   class="form-control m-input--air category-slug"
                                                   placeholder="BV Amount"
                                                   name="bv"
                                                   required
                                                   autocomplete="off"
                                                   aria-describedby="basic-addon1">
                                        </div>

                                    </div>


                                    <div class="form-group m-input-group--air">
                                        <label for="exampleInputEmail1">
                                            Price in Rs
                                        </label>
                                        <div class="input-group m-input-group">
                                            <div class="input-group-prepend">
														<span class="input-group-text">
															<i class="flaticon-more-v6"></i>
														</span>
                                            </div>
                                            <input type="number"

                                                   class="form-control m-input--air category-slug"
                                                   placeholder="Price"
                                                   name="price"
                                                   required
                                                   autocomplete="off"
                                                   aria-describedby="basic-addon1">
                                        </div>

                                    </div>


                                    <div class="form-group m-input-group--air">
                                        <label for="exampleInputEmail1">
                                            Type
                                        </label>
                                        <div class="input-group m-input-group">
                                            <div class="input-group-prepend">
														<span class="input-group-text">
															<i class="flaticon-more-v6"></i>
														</span>
                                            </div>
                                            <input type="text"

                                                   class="form-control m-input--air category-slug"
                                                   placeholder="Type"
                                                   name="type"
                                                   required
                                                   autocomplete="off"
                                                   aria-describedby="basic-addon1">
                                        </div>

                                    </div>







                                </div>






                                <div class="m-portlet__foot m-portlet__foot--fit">
                                    <div class="m-form__actions">
                                        <button type="submit" class="btn btn-primary">
                                            Submit
                                        </button>
                                        <button type="reset" class="btn btn-secondary">
                                            Cancel
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <!--end::Section-->

                    </div>
                </div>
                <!--end::Portlet-->
                <!--begin::Portlet-->

            </div>
            <div class="col-md-6">
                <!--begin::Portlet-->
                <div class="m-portlet m-portlet--tab">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
												<span class="m-portlet__head-icon m--hide">
													<i class="la la-gear"></i>
												</span>
                                <h3 class="m-portlet__head-text">
                                    All Purchase Types
                                </h3>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <!--begin::Section-->
                        <div class="m-section">



                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">description</th>
                                    <th scope="col">value</th>
                                    <th scope="col">bv</th>
                                    <th scope="col">price</th>
                                    <th scope="col">type</th>


                                </tr>
                                </thead>
                                <tbody>


                                @foreach($a as $s)

                                    <tr>
                                        <th scope="row">{{$s->id}}</th>
                                        <td>{{$s->description}}</td>
                                        <td>{{$s->value}}</td>
                                        <td>{{$s->bv}}</td>
                                        <td>{{$s->price}}</td>
                                        <td>{{$s->type}}</td>
                                        <td>
                                            <a href="{{route('master.manage.bv.master.delete',$s->id)}}" class=" btn btn-danger">Delete</a>
                                        </td>


                                    </tr>


                                @endforeach





                                </tbody>
                            </table>








                        </div>
                        <!--end::Section-->

                    </div>
                </div>
                <!--end::Portlet-->
                <!--begin::Portlet-->

            </div>








        </div>
    </div>

@endsection



@section('admin_footer_script')


@endsection