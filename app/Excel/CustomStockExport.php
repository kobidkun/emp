<?php
/**
 * Created by PhpStorm.
 * User: kobid
 * Date: 13/04/18
 * Time: 5:44 AM
 */




namespace App\Excel;



use App\ProductVatient;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;


class CustomStockExport implements FromQuery, WithHeadings
{
    use Exportable;

    public function headings(): array
    {
        return [
            'ID',
            'NAME',
            'CODE NAME',
            'COLOR',
            'HSN Code',
            'MRP',
            'CGST',
            'CGST %',
            'SGST',
            'SGST %',
            'IGST',
            'IGST %',
            'Stock ',
            'TOTAL TAX ',
            'LISTING PRICE ',
            'STOCK',
            'TYPE',
            'Unit',
            'SIZE'


        ];
    }

    public function forYear($from, $to)
    {
        $this->from = $from;
        $this->to = $to;

        return $this;
    }

    public function query()
    {

        $dateFrom = $this->from->format('Y-m-d')." 00:00:00";
        $dateTo = $this->to->format('Y-m-d')." 23:59:59";
        $a = ProductVatient::select(

            'id',
            'name',
            'code_name',
            'color',
            'hsn',
            'mrp_price',
           'cgst',
           'cgst_percentage',
         'sgst',
         'sgst_percentage',
         'igst',
           'igst_percentage',
           'taxable_rate',
           'total_tax',
        'listing_price',
         'stock',
          'type',
          'unit',
           'size'




        )->whereBetween('created_at', [$dateFrom, $dateTo]);
       return $a;

      //  return CreateInvoice::query()->whereYear('created_at', $this->year);
    }
}