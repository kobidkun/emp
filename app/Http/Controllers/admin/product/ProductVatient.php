<?php

namespace App\Http\Controllers\admin\product;

use App\Product;
use App\ProductVatienttoImages;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;
use QrCode;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\DataTables;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Image;
class ProductVatient extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth:admin,storeuser,cashier']);


    }


    public function SaveProductVarient(Request $request){

        $s = new \App\ProductVatient();
        $s->code_name = $request->code_name;
        $s->color = $request->color;
        $s->size = $request->size;

        $s->stock = $request->stock;

        $s->name = $request->name;
        $s->product_id = $request->product_id;
        $s->hsn = $request->hsn;
        $s->type = $request->type;
        $s->code_name = $request->code_name;
        $s->unit = $request->unit;
        $s->mrp_price = $request->mrp_price;
        $s->listing_price = $request->listing_price;
        $s->igst_percentage = $request->igst_percentage;
        $s->igst = $request->igst;
        $s->cgst_percentage = $request->cgst_percentage;
        $s->sgst_percentage = $request->cgst_percentage;
        $s->cgst = $request->cgst;
        $s->sgst = $request->cgst;
        $s->cess_percentage = $request->cess_percentage;
        $s->taxable_value = $request->taxable_value;
        $s->taxable_rate = $request->taxable_rate;
        $s->total_tax = $request->total_tax;
        $s->unit = $request->unit;
        $s->business_vol = $request->business_vol;






        $s->slug = str_replace(' ', '_', $request->name.'-'.time());;




        //


        $s->save();




        return back();





    }

    public function details($varid){
        $a = \App\ProductVatient::findorfail($varid);
        $priProduct = Product::findorfail($a->product_id);

        $fromDate = Carbon::now()->subMonth()->startOfMonth()->toDateString();
        $tillDate = Carbon::now()->subMonth()->endOfMonth()->toDateString();

        $productSale = $a->productvarient_to_sale()
            ->whereBetween(DB::raw('date(created_at)'), [$fromDate, $tillDate])
            //->whereBetween('created_at',[$fromDate,$tillDate])
            ->get();

        $sumsale = $productSale->sum('quantity');


        return view('admin.page.product.varient.details')->with(
            [
                'product' => $a,
                'prim_product' => $priProduct,
                'sumsale' => $sumsale

            ]
        );
    }

    public function UpdateProductdata(Request $request, $id){


        $s =  \App\ProductVatient::findorfail($id);
        //  return 'lll';
        $s->name = $request->name;
        $s->hsn = $request->hsn;
        $s->type = $request->type;
        $s->code_name = $request->code_name;
        $s->unit = $request->unit;
        $s->mrp_price = $request->mrp_price;
        $s->listing_price = $request->listing_price;
        $s->igst_percentage = $request->igst_percentage;
        $s->igst = $request->igst;
        $s->cgst_percentage = $request->cgst_percentage;
        $s->sgst_percentage = $request->cgst_percentage;
        $s->cgst = $request->cgst;
        $s->sgst = $request->cgst;
        $s->cess_percentage = $request->cess_percentage;
        $s->taxable_value = $request->taxable_value;
        $s->taxable_rate = $request->taxable_rate;
        $s->total_tax = $request->total_tax;
        $s->unit = $request->unit;
        $s->business_vol = $request->business_vol;

        $s->save();

        return back();

    }

    public function ProductVarianrdatatable($id){


        $users = \App\ProductVatient::where('product_id', $id)->select(['id','name','color','size','stock','created_at']);



        return Datatables::of($users)
            ->editColumn('created_at', function($user) {
                return Carbon::createFromFormat('Y-m-d H:i:s', $user->created_at)->toDayDateTimeString();
            })
            ->addColumn('action', function ($user) {
                return '<a href="/admin/product/varient/details/'.$user->id.'" class="btn btn-xs btn-primary"><i class="fa fa-book"></i> Details</a>'.' '.
                    '<a href="/admin/product/varient/delete/'.$user->id.'" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i> Delete</a>';
            })
            ->rawColumns(['action'])
            ->make();
    }

    public function delete($id){
        $a = \App\ProductVatient::findorfail($id);
        $a->delete();
        return back();
    }


    public function ImageUpload(Request $request){
        $id = $request->id;
        $image      = $request->file('image');
        $fileName   = time() . '.' . $image->getClientOriginalExtension();

        $image_normal = Image::make($image);
        $image_thumb = Image::make($image);
        $image_normal = $image_normal->stream();
        $image_thumb = $image_thumb->stream();

        $finalImage = $image_thumb->__toString();

        // Storage::disk('s3')->put($path.$fileName, $image_normal->__toString());
        Storage::disk('s3')->put('product_variant_image/secondary/'.$fileName, $finalImage);

        $s = new ProductVatienttoImages();
        $s->images_url = $fileName;
        $s->extinsion = $fileName;
        $s->path = 'product_variant_image/secondary/';
        $s->local_path = 'null';
        $s->cdn_url = env('cdnurl').'/product_variant_image/secondary/'.$fileName;
        $s->product_vatient_id = $request->product_vatient_id;
        $s->product_id = $request->product_id;
        $s->name = $fileName;
        $s->save();
    }

    public function DeleteProductSecondaryImages(Request $request, $id){
        $img = ProductVatienttoImages::findorfail($id);

        Storage::disk('s3')->delete('product_variant_image/secondary/'.$img->extinsion);
        $img->delete();
        return redirect()->back();

        //  return 'lll';


    }


}
