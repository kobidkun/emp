<?php

namespace App\Http\Controllers\admin\Config;

use App\Model\Master\AppConfig;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GlobalConfig extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth:admin,storeuser,cashier']);


    }


   public function GetIndex(){
        $a = AppConfig::first();

      //  dd($a);
       return view('admin.page.config.config')->with([
           'code' => $a
       ]);
   }

   public function SaveConfig($id, Request $request){

      $a = AppConfig::find($id);


      $a->config = $request->config;
      $a->save;
     //  return ($a->config);
      return back();

   }
}
