
<!DOCTYPE html>
<html lang="en" >
<!-- begin::Head -->
<head>
    <meta charset="utf-8" />
    <title>
        Admin | Emporium
    </title>
    <meta name="description" content="Latest updates and statistic charts">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!--begin::Web font -->
    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
    <script>
        WebFont.load({
            google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},
            active: function() {
                sessionStorage.fonts = true;
            }
        });
    </script>
    <!--end::Web font -->
    <!--begin::Base Styles -->
    <link href="{{asset('/production/css/production.min.css')}}" rel="stylesheet" type="text/css" />
    <!--end::Base Styles -->
    <link rel="shortcut icon" href="{{asset('images/icon.png')}}" />
</head>
<!-- end::Head -->
<!-- end::Body -->
<body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-light m-aside-left--fixed m-aside-left--offcanvas m-aside-left--minimize m-brand--minimize m-footer--push m-aside--offcanvas-default"  >
<!-- begin:: Page -->
<div class="m-grid m-grid--hor m-grid--root m-page">
    <!-- BEGIN: Header -->
    @include('admin.component.header')
    <!-- END: Header -->
    <!-- begin::Body -->
    <div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
        <!-- BEGIN: Left Aside -->
        @include('admin.component.sidemenu')
        <!-- END: Left Aside -->
        <div class="m-grid__item m-grid__item--fluid m-wrapper">



            @yield('content')


        </div>
    </div>
    <!-- end:: Body -->
    <!-- begin::Footer -->
    @include('admin.component.footer')
    <!-- end::Footer -->
</div>


<script src="{{asset('/production/js/production.min.js')}}" type="text/javascript"></script>
<!--end::Base Scripts -->
<!--begin::Page Snippets -->
<script src="{{asset('production/js/dashboard.js')}}" type="text/javascript"></script>
<!--end::Page Snippets -->

@yield('admin_footer_script')
<link href="{{asset('/easyautocomplete/easy-autocomplete.min.css')}}" rel="stylesheet" type="text/css" />
<script src="{{asset('/easyautocomplete/jquery.easy-autocomplete.min.js')}}" type="text/javascript"></script>
{{--<script>
    $(document).ready(function(){
        $("#search").keyup(function(){
            var str=  $("#search").val();
            if(str == "") {
                $( "#txtHint" ).html("<b>Blogs information will be listed here...</b>");
            }else {
                $.get( "{{ url('/api/search/global?keyword=') }}"+str, function( data ) {
                  //  $( "#txtHint" ).html( data );
                    console.log(data);
                });
            }
        });
    });
</script>--}}


<script>
    var options = {
        url: function(phrase) {
            return "/api/search/global?keyword=" + phrase ;
        },

        categories: [
            {   //Category fruits
                listLocation: "b",
                header: "-- Invoice BV --",
                getValue: "invoice_number_generated",

            },
            {   //Category vegetables
                listLocation: "j",
                header: "-- Invoice --",
                getValue: "invoice_number_generated",

            },

            {   //Category vegetables
                listLocation: "k",
                header: "-- Customers --",
                getValue: "fname",


            }
        ]


    };

    $("#search").easyAutocomplete(options);
</script>




</body>
<!-- end::Body -->
</html>
