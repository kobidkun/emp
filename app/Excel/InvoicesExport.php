<?php
/**
 * Created by PhpStorm.
 * User: kobid
 * Date: 13/04/18
 * Time: 5:44 AM
 */




namespace App\Excel;

use App\model\Invoice\CreateInvoice;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;



class InvoicesExport implements FromCollection
{
    use Exportable;

    public function collection()
    {
        return CreateInvoice::all();
    }
}