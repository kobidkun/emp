<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rating extends Model
{
    protected $hidden = [
        'updated_at',
        'created_at',
    ];

    public function products()
    {
        return $this->belongsTo('App\Product');
    }

}
