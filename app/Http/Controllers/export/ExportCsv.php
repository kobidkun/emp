<?php

namespace App\Http\Controllers\export;

use App\Http\Controllers\admin\product\ProductVatient;
use App\Model\BVInvoice\CreateInvoiceBV;
use App\Model\BVInvoice\ReturnInvoiceBV;
use App\model\Invoice\CreateInvoice;
use App\model\Invoice\InvoiceToProduct;
use App\Model\Invoice\ReturnInvoice;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ExportCsv extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth:admin,storeuser,cashier']);


    }


    public function ViewInvoice(){
        return view('export.csv.saleinvoice');
    }


    public function exportInvoice(Request $request)
    {

        $from = $request->from;
        $To= $request->to;


        $dateFrom = \Carbon\Carbon::parse($from)->format('Y-m-d')." 00:00:00";
        $dateTo = \Carbon\Carbon::parse($To)->format('Y-m-d')." 23:59:59";


        $headers = array(
            "Content-type" => "text/csv",
            "Content-Disposition" => "attachment; filename=".$dateFrom.$dateTo."-invoice-export.csv",
            "Pragma" => "no-cache",
            "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
            "Expires" => "0"
        );






        $reviews = CreateInvoice::whereBetween('created_at', [$dateFrom, $dateTo])->get();
        $columns = array(
            'Sno',
            'Invoice Number',
            'Invoice Serial No',
            'Invoice Date',
            'Return Month ',
            'Customer Name',
            'Invoice GSTIN',
            'Place of Supply',
            'Invoice Return Quarter',
            'Billing Address Street',
            'Billing Address Locality',
            'Billing Address City',
            'Billing Address State',
            'Billing Address Country',
            'Billing Address Pin',
            'Shipping Address Street',
            'Shipping Address Locality',
            'Shipping Address City',
            'Shipping Address State',
            'Shipping Address Country',
            'Shipping Address Pincode',
            'Taxable Amount',
            'Discount Amount',
            'Cgst',
            'Sgst',
            'Igst',
            'Tax',
            'Taxable Value',
            'Taxable Amount',
            'Disount Amount',
            'Total'

        );

        $callback = function() use ($reviews, $columns)
        {
            $file = fopen('php://output', 'w');
            fputcsv($file, $columns);

            foreach($reviews as $review) {
                fputcsv($file, array(
                    $review->id,
                    $review->invoice_number,
                    $review->invoice_number_generated,
                    $review->invoice_date,
                    $review->invoice_return_month,
                    $review->invoice_customer_name,
                    $review->invoice_customer_gstin,
                    $review->invoice_place_of_supply,
                    $review->invoice_return_quater,
                    $review->invoice_billing_address_street,
                    $review->invoice_billing_address_locality,
                    $review->invoice_billing_address_city,
                    $review->invoice_billing_address_state,
                    $review->invoice_billing_address_country,
                    $review->invoice_billing_address_pin,
                    $review->invoice_shipping_address_street,
                    $review->invoice_shipping_address_locality,
                    $review->invoice_shipping_address_city,
                    $review->invoice_shipping_address_state,
                    $review->invoice_shipping_address_country,
                    $review->invoice_shipping_address_pin,
                    $review->invoice_total_taxable_amount,
                    $review->invoice_total_taxable_discount_amount,
                    $review->invoice_total_tax_cgst,
                    $review->invoice_total_tax_sgst,
                    $review->invoice_total_tax_igst,
                    $review->invoice_total_tax,
                    $review->invoice_total_tax_value,
                    $review->invoice_total_taxable_amt,
                    $review->invoice_total_discount_amt,
                    $review->invoice_total

                ));
            }
            fclose($file);
        };
        return response()->stream($callback, 200, $headers);
    }





    public function ViewProduct(){
        return view('export.csv.saleproduct');
    }


    public function exportProduct(Request $request)
    {

        $from = $request->from;
        $To= $request->to;


        $dateFrom = \Carbon\Carbon::parse($from)->format('Y-m-d')." 00:00:00";
        $dateTo = \Carbon\Carbon::parse($To)->format('Y-m-d')." 23:59:59";


        $headers = array(
            "Content-type" => "text/csv",
            "Content-Disposition" => "attachment; filename=".$dateFrom.$dateTo."-product-export.csv",
            "Pragma" => "no-cache",
            "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
            "Expires" => "0"
        );






        $reviews = InvoiceToProduct::whereBetween('created_at', [$dateFrom, $dateTo])->get();
        $columns = array(
            'Sno',
            'Invoice Number',
            'Name',
            'Unit ',
            'HSN Code',
            'Type',
            'Quantity',
            'Rate',
            'Discount Amount',
            'Discount Percentage',
            'Total Discount Percentage',
            'Taxable Rate',
            'Taxable Amount',
            'Tax Value',
            'CGST',
            'SGST',
            'IGST',
            'CGST %',
            'SGST %',
            'IGST %',
            /*'Taxable Amount',
            'Taxable Amount',
            'Taxable Amount',
            'Discount Amount',*/
            'Time & Date',
            'Color',
            'Size',
            'Total'

        );

        $callback = function() use ($reviews, $columns)
        {
            $file = fopen('php://output', 'w');
            fputcsv($file, $columns);

            foreach($reviews as $review) {
                fputcsv($file, array(
                    $review->id,
                    $review->invoice_id,
                    $review->name,
                    $review->unit,
                    $review->hsn,
                    $review->type,
                    $review->quantity,
                    $review->rate,
                    $review->discount_amount,
                    $review->discount_percentage,
                    $review->product_total_disc,
                    $review->taxable_rate,
                    $review->taxable_amount,
                    $review->tax_value,
                    $review->cgst,
                    $review->sgst,
                    $review->igst,
                    $review->sgst_percentage,
                    $review->cgst_percentage,
                    $review->igst_percentage,
                    $review->product_total_disc,
                    $review->taxable_amount,
                    $review->created_at,
                    $review->color,
                    $review->size,
                    $review->total

                ));
            }
            fclose($file);
        };
        return response()->stream($callback, 200, $headers);
    }






    public function ViewCustomer(){
        return view('export.csv.salecustomer');
    }


    public function exportCustomer(Request $request)
    {

        $from = $request->from;
        $To= $request->to;
        $customer = $request->customer_id;


        $dateFrom = \Carbon\Carbon::parse($from)->format('Y-m-d')." 00:00:00";
        $dateTo = \Carbon\Carbon::parse($To)->format('Y-m-d')." 23:59:59";


        $headers = array(
            "Content-type" => "text/csv",
            "Content-Disposition" => "attachment; filename=".$customer.$dateFrom.$dateTo."-customer-export.csv",
            "Pragma" => "no-cache",
            "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
            "Expires" => "0"
        );






        $reviews = CreateInvoice::where('customer_id', $customer)->whereBetween('created_at', [$dateFrom, $dateTo])->get();
        $columns = array(
            'Sno',
            'Invoice Number',
            'Invoice Date',
            'Return Month ',
            'Customer Name',
            'Invoice GSTIN',
            'Place of Supply',
            'Invoice Return Quarter',
            'Billing Address Street',
            'Billing Address Locality',
            'Billing Address City',
            'Billing Address State',
            'Billing Address Country',
            'Billing Address Pin',
            'Shipping Address Street',
            'Shipping Address Locality',
            'Shipping Address City',
            'Shipping Address State',
            'Shipping Address Country',
            'Shipping Address Pincode',
            'Taxable Amount',
            'Discount Amount',
            'Cgst',
            'Sgst',
            'Igst',
            'Tax',
            'Taxable Value',
            'Taxable Amount',
            'Disount Amount',
            'Total'

        );

        $callback = function() use ($reviews, $columns)
        {
            $file = fopen('php://output', 'w');
            fputcsv($file, $columns);

            foreach($reviews as $review) {
                fputcsv($file, array(
                    $review->id,
                    $review->invoice_number,
                    $review->invoice_date,
                    $review->invoice_return_month,
                    $review->invoice_customer_name,
                    $review->invoice_customer_gstin,
                    $review->invoice_place_of_supply,
                    $review->invoice_return_quater,
                    $review->invoice_billing_address_street,
                    $review->invoice_billing_address_locality,
                    $review->invoice_billing_address_city,
                    $review->invoice_billing_address_state,
                    $review->invoice_billing_address_country,
                    $review->invoice_billing_address_pin,
                    $review->invoice_shipping_address_street,
                    $review->invoice_shipping_address_locality,
                    $review->invoice_shipping_address_city,
                    $review->invoice_shipping_address_state,
                    $review->invoice_shipping_address_country,
                    $review->invoice_shipping_address_pin,
                    $review->invoice_total_taxable_amount,
                    $review->invoice_total_taxable_discount_amount,
                    $review->invoice_total_tax_cgst,
                    $review->invoice_total_tax_sgst,
                    $review->invoice_total_tax_igst,
                    $review->invoice_total_tax,
                    $review->invoice_total_tax_value,
                    $review->invoice_total_taxable_amt,
                    $review->invoice_total_discount_amt,
                    $review->invoice_total

                ));
            }
            fclose($file);
        };
        return response()->stream($callback, 200, $headers);
    }




    public function ExportProductsVariendsWithStockview(Request $request){

        return view('export.csv.productvarient');
    }

    public function ExportProductsVariendsWithStock(Request $request){

        $from = $request->from;
        $To= $request->to;


        $dateFrom = \Carbon\Carbon::parse($from)->format('Y-m-d')." 00:00:00";
        $dateTo = \Carbon\Carbon::parse($To)->format('Y-m-d')." 23:59:59";


        $headers = array(
            "Content-type" => "text/csv",
            "Content-Disposition" => "attachment; filename=".$dateFrom.$dateTo."-stock-export.csv",
            "Pragma" => "no-cache",
            "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
            "Expires" => "0"
        );






        $reviews = \App\ProductVatient::whereBetween('created_at', [$dateFrom, $dateTo])->get();
        $columns = array(


            'ID',
            'CODE NAME',
            'COLOR',
            'HSN Code',
            'MRP',
            'CGST',
            'CGST %',
            'SGST',
            'SGST %',
            'IGST',
            'IGST %',
            'Stock ',
            'TOTAL TAX ',
            'LISTING PRICE ',
            'STOCK',
            'TYPE',
            'Unit',
            'SIZE'

        );

        $callback = function() use ($reviews, $columns)
        {
            $file = fopen('php://output', 'w');
            fputcsv($file, $columns);

            foreach($reviews as $review) {
                fputcsv($file, array(
                    $review->id,
                    $review->code_name,
                    $review->color,
                    $review->hsn,
                    $review->mrp_price,
                    $review->cgst,
                    $review->cgst_percentage,
                    $review->sgst,
                    $review->sgst_percentage,
                    $review->igst,
                    $review->igst_percentage,
                    $review->taxable_rate,
                    $review->total_tax,
                    $review->listing_price,
                    $review->stock,
                    $review->type,
                    $review->unit,
                    $review->size

                ));
            }
            fclose($file);
        };
        return response()->stream($callback, 200, $headers);

    }


    public function Stockinout(Request $request){



        $from = $request->from;
        $To= $request->to;


        $dateFrom = \Carbon\Carbon::parse($from)->format('Y-m-d')." 00:00:00";
        $dateTo = \Carbon\Carbon::parse($To)->format('Y-m-d')." 23:59:59";




        $products = \App\ProductVatient::with('Productsale','stock')->get();




        $csvExporter = new \Laracsv\Export();


        $csvExporter->beforeEach(function ($products) {







            // Now notes field will have this value
            $products->output = $products->Productsale()->sum('quantity');
            $products->input = $products->stock()->sum('amount');
        });

        $csvExporter->build($products, ['name' => 'Product Name',
            'code_name' => 'Code Name',
            'size'  => 'Product Name',
            'color' => 'Product Color',
            'output'  => 'Product Input Stock',
            'input'  => 'Product Output'
        ]);


        $csvExporter->download(time().'-input-output-stocks.csv');





    }
    public function StockinoutView(Request $request){


        return view('export.csv.stockinout-csv');


    }

    public function InvoiceBV(Request $request){


        return view('export.csv.saleinvoicebv');


    }

    public function InvoiceBVDownload(Request $request){


        $from = $request->from;
        $To= $request->to;


        $dateFrom = \Carbon\Carbon::parse($from)->format('Y-m-d')." 00:00:00";
        $dateTo = \Carbon\Carbon::parse($To)->format('Y-m-d')." 23:59:59";




        $products = CreateInvoiceBV::whereBetween('created_at', [$dateFrom, $dateTo])->get();




        $csvExporter = new \Laracsv\Export();


        $csvExporter->build($products, [
            'invoice_number_generated' => 'Inv no',
            'invoice_date' => 'Date',
            'invoice_return_month'  => 'Return Month',
            'invoice_customer_name' => 'Customer name',
            'invoice_customer_gstin'  => 'Customer GSTIN',
            'invoice_place_of_supply'  => 'Plase of supply',
            'invoice_return_quater'  => 'Return Quarter',
            'invoice_billing_address_street'  => 'Street',
            'invoice_billing_address_locality'  => 'Locality',
            'invoice_billing_address_city'  => 'city',
            'invoice_billing_address_state'  => 'state',
            'invoice_billing_address_country'  => 'Country',
            'invoice_billing_address_pin'  => 'pin',
            'invoice_shipping_address_street'  => 'Street',
            'invoice_shipping_address_locality'  => 'Locality',
            'invoice_shipping_address_city'  => 'city',
            'invoice_shipping_address_state'  => 'state',
            'invoice_shipping_address_country'  => 'Country',
            'invoice_shipping_address_pin'  => 'pin',
            'invoice_total_taxable_amount'  => 'Taxable Amount',
            'invoice_total_taxable_discount_amount'  => 'Discount Amount',
            'invoice_total_tax_cgst'  => 'Cgst',
            'invoice_total_tax_sgst'  => 'Sgst',
            'invoice_total_tax_igst'  => 'Igst',
            'invoice_total_tax'  => 'Tax',
            'invoice_total_tax_value'  => 'Taxable Value',
            'invoice_total_taxable_amt'  => 'Taxable Amount',
            'invoice_total_discount_amt'  => 'Disount Amount',
            'invoice_total'  => 'Total',
            'total_bv'  => 'Total BV',
        ]);






        $csvExporter->download(time().'-Bv-invoice-export.csv');


    }


    public function ReturnInvoiceBV(Request $request){


        return view('export.csv.returninvoicebv');


    }

    public function ReturnInvoiceBVDownload(Request $request){


        $from = $request->from;
        $To= $request->to;


        $dateFrom = \Carbon\Carbon::parse($from)->format('Y-m-d')." 00:00:00";
        $dateTo = \Carbon\Carbon::parse($To)->format('Y-m-d')." 23:59:59";




        $products = ReturnInvoiceBV::whereBetween('created_at', [$dateFrom, $dateTo])->get();




        $csvExporter = new \Laracsv\Export();


        $csvExporter->build($products, [
            'invoice_number_generated' => 'Inv no',
            'invoice_date' => 'Date',
            'invoice_return_month'  => 'Return Month',
            'invoice_customer_name' => 'Customer name',
            'invoice_customer_gstin'  => 'Customer GSTIN',
            'invoice_place_of_supply'  => 'Plase of supply',
            'invoice_return_quater'  => 'Return Quarter',
            'invoice_billing_address_street'  => 'Street',
            'invoice_billing_address_locality'  => 'Locality',
            'invoice_billing_address_city'  => 'city',
            'invoice_billing_address_state'  => 'state',
            'invoice_billing_address_country'  => 'Country',
            'invoice_billing_address_pin'  => 'pin',
            'invoice_shipping_address_street'  => 'Street',
            'invoice_shipping_address_locality'  => 'Locality',
            'invoice_shipping_address_city'  => 'city',
            'invoice_shipping_address_state'  => 'state',
            'invoice_shipping_address_country'  => 'Country',
            'invoice_shipping_address_pin'  => 'pin',
            'invoice_total_taxable_amount'  => 'Taxable Amount',
            'invoice_total_taxable_discount_amount'  => 'Discount Amount',
            'invoice_total_tax_cgst'  => 'Cgst',
            'invoice_total_tax_sgst'  => 'Sgst',
            'invoice_total_tax_igst'  => 'Igst',
            'invoice_total_tax'  => 'Tax',
            'invoice_total_tax_value'  => 'Taxable Value',
            'invoice_total_taxable_amt'  => 'Taxable Amount',
            'invoice_total_discount_amt'  => 'Disount Amount',
            'invoice_total'  => 'Total',
            'total_bv'  => 'Total BV',
        ]);






        $csvExporter->download(time().'-return-bv-invoice-export.csv');


    }

    public function ReturnInvoice(Request $request){


        return view('export.csv.returninvoice');


    }

    public function ReturnInvoiceDownload(Request $request){


        $from = $request->from;
        $To= $request->to;


        $dateFrom = \Carbon\Carbon::parse($from)->format('Y-m-d')." 00:00:00";
        $dateTo = \Carbon\Carbon::parse($To)->format('Y-m-d')." 23:59:59";




        $products = ReturnInvoice::whereBetween('created_at', [$dateFrom, $dateTo])->get();




        $csvExporter = new \Laracsv\Export();


        $csvExporter->build($products, [
            'invoice_number_generated' => 'Inv no',
            'invoice_date' => 'Date',
            'invoice_return_month'  => 'Return Month',
            'invoice_customer_name' => 'Customer name',
            'invoice_customer_gstin'  => 'Customer GSTIN',
            'invoice_place_of_supply'  => 'Plase of supply',
            'invoice_return_quater'  => 'Return Quarter',
            'invoice_billing_address_street'  => 'Street',
            'invoice_billing_address_locality'  => 'Locality',
            'invoice_billing_address_city'  => 'city',
            'invoice_billing_address_state'  => 'state',
            'invoice_billing_address_country'  => 'Country',
            'invoice_billing_address_pin'  => 'pin',
            'invoice_shipping_address_street'  => 'Street',
            'invoice_shipping_address_locality'  => 'Locality',
            'invoice_shipping_address_city'  => 'city',
            'invoice_shipping_address_state'  => 'state',
            'invoice_shipping_address_country'  => 'Country',
            'invoice_shipping_address_pin'  => 'pin',
            'invoice_total_taxable_amount'  => 'Taxable Amount',
            'invoice_total_taxable_discount_amount'  => 'Discount Amount',
            'invoice_total_tax_cgst'  => 'Cgst',
            'invoice_total_tax_sgst'  => 'Sgst',
            'invoice_total_tax_igst'  => 'Igst',
            'invoice_total_tax'  => 'Tax',
            'invoice_total_tax_value'  => 'Taxable Value',
            'invoice_total_taxable_amt'  => 'Taxable Amount',
            'invoice_total_discount_amt'  => 'Disount Amount',
            'invoice_total'  => 'Total',
        ]);






        $csvExporter->download(time().'-Bv-invoice-export.csv');


    }



}
