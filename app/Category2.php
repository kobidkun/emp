<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category2 extends Model
{

    protected $hidden = array(
        'description',
        'product_id',
        'updated_at',
        'created_at',
        'catagory_id',
    );


    public function products()
    {
        return $this->belongsToMany('App\Product');
    }

    public function category()
    {
        return $this->belongsTo('App\Catagory','catagory_id','id');
    }

    public function category3()
    {
        return $this->hasMany('App\Catagory3');
    }
}
