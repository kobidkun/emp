<?php

namespace App\Http\Controllers\admin\FrontEnd;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class ManageGallery extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth:admin,storeuser,cashier']);


    }


    public function View(){
        $a = \App\Model\FrontEnd\ManageGallery::all();
        return view('admin.page.frontend.gallery')->with(['images' => $a]);
    }
    public function Upload(Request $request){

        $this->validate($request, [

            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:85',
        ]);

        $imageName1 = time().'.'.$request->image->getClientOriginalExtension();
        $imageName2 = str_replace(' ', '_', $imageName1);
        $imageName = str_replace(' ', '_', $imageName1);
        $image = $request->file('image');
        $t = Storage::disk('s3')->put('slider/gallery/'.$imageName, file_get_contents($image), 'slider/mobile/');
        $imageName = Storage::disk('s3')->url('slider/gallery/'.$imageName);
        // $imageName3 = Storage::disk('s3')->url('product_image/primary/'.$imageName);

        //  $replacesextraspaces1 = str_replace(' ', '_', $finalimageurl1);

        $s = new \App\Model\FrontEnd\ManageGallery();
        $s->url = $imageName;
        $s->title = $imageName2;

        $s->cdn_url = 'https://cdn.emp.tecions.xyz/slider/gallery/'.$imageName2;

        $s->name = $imageName2;
        $s->type = $request->type;
        $s->save();
        return $imageName2;
    }


    public function Delete($id){
        $img = \App\Model\FrontEnd\ManageGallery::findorfail($id);

        Storage::disk('s3')->delete('slider/gallery/'.$img->name);
        $img->delete();
        return redirect()->back();
    }
}
